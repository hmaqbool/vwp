<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class DocuSignPage extends DashboardAppPage
{
    public function admin()
    {
        //session_destroy();
        //px($_SESSION);
        
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $logic->loginCheck();
        $logic->redirectToSendDocument();
    }
    
    public function admin_login()
    {
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        if (isset($_SESSION["LoggedIn"]) && $_SESSION["LoggedIn"] == true) 
        {
            $logic->redirectToSendDocument();
            return;
        }
	
        $logic->initCreds();
        $logic->initSession();
        
        $this->regDisplay('login');
    }
    
    public function admin_loginSubmit()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        if (isset($_POST["submit"])) 
        {
            $_SESSION["UserID"] = $_POST["DevCenterEmail"];
            $_SESSION["Password"] = $_POST["DevCenterPassword"];
            $_SESSION["IntegratorsKey"] = $_POST["DevCenterIKey"];
            
            if ($logic->login())
            {
                $_SESSION["LoggedIn"] = true;
                $logic->redirectToSendDocument();
            }
            else 
            {
                $_SESSION["LoggedIn"] = false;
                $logic->redirectToError();
            }
        }

        // if the "reset" button has been pressed, set all values back to original
        if (isset($_POST["reset"])) {
            $logic->initCreds();
        }
    }
    
    public function admin_sendDocument()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $this->regAjax('sendDocumentSubmit');
        $this->doAjax();
        
        $sc = SiteConfig::getObj();
        $sc->template("docusign.php");
        
        $logic->loginCheck();
        $this->regDisplay('sendDocument');
    }
        
    public function admin_sendTemplate()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $logic->loginCheck();
        
        $this->regAjax("sendTemplateSubmit");
        $this->doAjax();
        
        $loadTemplates = $logic->loadTemplates();
        
        $sc = SiteConfig::getObj();
        $sc->template("docusign.php");
        
        $this->regDisplay('sendTemplate', $loadTemplates);
    }
    
    public function admin_getStatusAndDocs()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $logic->loginCheck();
        
        /*
        if ($this->getVar('envelopid') == '')
        {
            $this->regDisplay("showMessage", "Missing parameter 'Envelop Id'", 'Missing Parameter');
            return;
        }
        
        if ($this->getVar('accountID') == '')
        {
            $this->regDisplay("showMessage", "Missing parameter 'Account Id'", 'Missing Parameter');
            return;
        }
        
        if ($this->getVar('source') == '')
        {
            $this->regDisplay("showMessage", "Missing parameter 'Source'", 'Missing Parameter');
            return;
        }
        */
        
        $sc = SiteConfig::getObj();
        $sc->template("docusign.php");
        
        $statuses = $logic->getStatusAndDocs();
        $this->regDisplay("getStatusAndDocs", $statuses);
    }
    
    public function admin_embedDocuSign()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $logic->loginCheck();
        
        $this->regAjax("embedDocuSignSubmit");
        $this->doAjax();
        
        $sc = SiteConfig::getObj();
        $sc->template("docusign.php");
        
        $this->regDisplay("embedDocuSign");
    }
    
    public function admin_embedSending()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
     
        $logic->loginCheck();

        if (!isset($_SESSION["embedToken"])) 
        {
            $this->regDisplay("showMessage", "You shouldn't be on this page", "Missing Parameter");
            return;
        }
        
        $sc = SiteConfig::getObj();
        $sc->template("docusign.php");
        
        $this->regDisplay("embedSending");
    }
    
    public function admin_connect()
    {
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $logic->loginCheck();
        $posts = $logic->connect();
        
        $sc = SiteConfig::getObj();
        $sc->template("docusign.php");
        
        $this->regDisplay("connect", $posts);
    }
    
    public function admin_pop()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
     
        $logic->loginCheck();
        $this->regDisplay('pop');
    }
    
    public function admin_getEnvelopeStatus()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
     
        $logic->loginCheck();

        if(empty($_GET['envelopeid']))
        {
            $this->regDisplay("showMessage", "Missing parameter 'Envelop Id'", 'Missing Parameter');
            return;
        }
        
        $envelope_id = $_GET['envelopeid'];

        // Call function that returns Envelope Status
        $envelopeStatus = $logic->getEnvelopeStatus($envelope_id);

        $this->regDisplay('getEnvelopeStatus', $envelopeStatus);
        
        //pr($envelopeStatus);
        //exit;
    }
    
    public function admin_getPdf()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
     
        $logic->loginCheck();
        
        if(empty($_GET['envelopeid']))
        {
            $this->regDisplay("showMessage", "Missing parameter 'Envelop Id'", 'Missing Parameter');
            return;
        }
        
        $envelope_id = $_GET['envelopeid'];

        // Call function that returns Envelope Status
        $status = $logic->getEnvelopeStatus($envelope_id);

        // Call function that returns PDF bytes (in Object)
        $pdf = $logic->getRequestPDF($envelope_id);

        // Output PDF as Attachment
        // - Title it as the Subject of the Envelope
        $pdfTitle = $status->RequestStatusResult->Subject;

        $output = $pdf->RequestPDFResult->PDFBytes;
        
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="'.$pdfTitle.'.pdf"');
        
        echo $output;
        exit;
    }
            
    public function admin_error()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
     
        $logic->loginCheck();
        $this->regDisplay('error');
    }
    
    public function admin_logout()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $logic->loginCheck();
        $logic->logout();
        $logic->redirectToLogin();
    }
    
    
    
    
    
    
    // Depricated.
    public function admin_sendDocumentSubmit()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $envelope = $logic->buildEnvelope();
        
        if (isset($_POST["SendNow"])) 
        {
            $logic->sendDocumentNow($envelope);
            exit;
        }
        else {
            $logic->embedDocumentSending($envelope);
        }
    }
    
    // Depricated.
    public function admin_embedDocuSignSubmit()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $logic->loginCheck();
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
            $logic->_oneSigner = isset($_POST["OneSigner"]);
            $logic->createAndSend($logic->_oneSigner);
            
            if(!$logic->_oneSigner){
                $logic->_showTwoSignerMessage = true;
            }
        } 
        else if ($_SERVER["REQUEST_METHOD"] == "GET") 
        {
            if (isset($_GET["envelopeID"])) 
            {
                // Display a message that we are moving on to Signer Number 2
                // - unless the message is suppressed (by signing from the GetStatusAndDocs page)
                if(isset($_GET['from_gsad'])){
                    $logic->getToken($logic->getStatus($_GET['envelopeID']),$_GET['clientID']);
                } 
                else 
                {
                    $logic->_showTransitionMessage = true;
                    $logic->getToken($logic->getStatus($_GET['envelopeID']), 2);
                }
            } 
            else {
                $_SESSION["embedToken"] = "";
            }
        }
        
        $sc = SiteConfig::getObj();
        $sc->template("docusign.php");
        
        $this->regDisplay("embedDocuSign", $logic->_showTwoSignerMessage, $logic->_showTransitionMessage);
    }
    
    // Depricated.
    public function admin_sendTemplateSubmit()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $logic->loginCheck();
     
        $displayForms = true;
        if(isset($_POST['createSampleEnvelope']))
        {
            $logic->createSampleEnvelope();
        } 
        else 
        {
            $api = $logic->getAPI();
            $template = new RequestTemplate();
            $template->TemplateID = $_POST["TemplateTable"];
            $template->IncludeDocumentBytes = false;

            try {
                $templateDetails = $api->RequestTemplate($template);
            } 
            catch (SoapFault $e) 
            {
                $_SESSION["errorMessage"] = array($e, $api->__getLastRequest(), $csParams);
                $logic->redirectToError();
            }
        }  
        
        $sc = SiteConfig::getObj();
        $sc->template("docusign.php");
        
        $this->regDisplay('sendTemplate', $templateDetails, $loadTemplates);
    }
}

?>