<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 * 
 * 
 * Todo: Common class should be a singalton class.
 * 
 */

class DocuSignLogic extends DashboardAppLogic
{    
    // Do we want One Signer (=true) or Two (=false)
    public $_oneSigner = true; 
    
    // Display (or not display) a message before Signer One has signed (only for Two Signer mode)
    public $_showTwoSignerMessage = false; 

    public $_showTransitionMessage = false;
        
    
    public function redirectToLogin()
    {
        header('location: http://hashimadmin.sporefanz.com/docusign/admin/login');
    }
    
    public function redirectToSendDocument()
    {
        header('location: http://hashimadmin.sporefanz.com/docusign/admin/sendDocument');
    }
    
    public function redirectToError()
    {
        header('location: http://hashimadmin.sporefanz.com/docusign/admin/error');
    }
    
    public function redirectToGetStatusAndDocs($envelopId, $accountId, $source='Document')
    {
        header("Location: http://hashimadmin.sporefanz.com/docusign/admin/getStatusAndDocs/envelopid_$envelopId/accountID_$accountId/source_$source");
    }
    
    public function redirectToEmbedSending($envelopId, $accountId, $source='Document')
    {
        header("Location: http://hashimadmin.sporefanz.com/docusign/admin/embedSending/envelopid_$envelopId/accountID_$accountId/source_$source");
    }
    
    
    // ======================================================================================= 
    // ================================= UTILITY =============================================
    // ======================================================================================= 
    
    /**
    * Prints variable dump pretty for debug and browser
    * @param unknown_type $val
    */
    public function print_r2($val) {
        echo '<pre>';
        print_r($val);
        echo  '</pre>';
    }

    /**
    * Prints variable dump pretty for debug and browser
    * @param unknown_type $val
    */
    public function pr($val) {
        echo '<pre>';
        print_r($val);
        echo  '</pre>';
    }
    
    /**
    * Returns xsd format datetime for start of today
    * @return string
    */
    public function todayXsdDate() 
    {
        if ($this->getConfig('timeZone') != null) {
            date_default_timezone_set($this->getConfig('timeZone'));
        }
        return (date("Y") . "-" . date("m") . "-" . date("d") . "T00:00:00.00");
    }

    /**
    * Returns xsd format datetime for now
    * @return string
    */
    public function nowXsdDate() 
    {
        if ($this->getConfig('timeZone') != null) {
            date_default_timezone_set($this->getConfig('timeZone'));
        }
        return (date("Y") . "-" . date("m") . "-" . date("d") . "T" . date("H") . ":" . date("i") . ":" . date("s"));
    }

    /**
    * A guid maker for all seasons (note that com_create_guid() only works on windows
    * @return string
    */
    public function guid(){
        if (function_exists('com_create_guid')){
            // somehow the function com_create_guid includes {}, while our webservice
            // doesn't.  Here we are changint the format by taking those curly braces out.
            $uuid = com_create_guid();
            $uuid = str_ireplace("{", "", $uuid );
            $uuid = str_ireplace("}", "", $uuid );
            return $uuid;
        }else{
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
            return $uuid;
        }
    }

    public function curPageURL() {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    public function getCallbackURL($callbackPage){
        $urlbase =  $this->curPageURL();
        $urlbase = substr($urlbase, 0, strrpos($urlbase, '/'));
        $urlbase = $urlbase . "/" . $callbackPage;
        
        //$urlbase = "http://hashimadmin.sporefanz.com/docusign/admin/";
        
        return $urlbase;
    }

    /**
    * Gets APIService SOAP client proxy that uses SOAP header authentication
    * @return APIService
    */
    public function getAPI() {
        
        include_once ("/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/api/APIService.php");
        
        $this->loginCheck();
        //$api_wsdl = "api/APIService.wsdl";
        $api_wsdl = 'http://hashim.sporefanz.com/includes/APIService.wsdl';
        $api_options =  array('location'=>$this->getConfig('apiEndpoint'),'trace'=>true,'features' => SOAP_SINGLE_ELEMENT_ARRAYS);
        $api = new APIService($api_wsdl, $api_options);
        $api->setCredentials("[" . $_SESSION["IntegratorsKey"] . "]" . $_SESSION["UserID"], $_SESSION["Password"]);

        return $api;
    }

    public function addEnvelopeID($envelopeID) {
        if (isset($_SESSION["EnvelopeIDs"])) {
            array_push($_SESSION["EnvelopeIDs"], $envelopeID);
        }
        else {
            $_SESSION["EnvelopeIDs"] = array($envelopeID);
        }
    }

    public function getLastEnvelopeID() {
        $index = count($_SESSION["EnvelopeIDs"]) - 1;
        return $_SESSION["EnvelopeIDs"][$index];
    }
    
        
    // ======================================================================================= 
    // ================================= SESSION =============================================
    // ======================================================================================= 
    
    public function loginCheck()
    {
        if (!isset($_SESSION["LoggedIn"]) || ($_SESSION["LoggedIn"] <> true )){
            $this->redirectToLogin();
        }
        
        return true;
    }
    
    public function initSession()
    {
        $_SESSION["LoggedIn"] = false;
	$_SESSION["errorMessage"] = null;
	$_SESSION["EnvelopeIDs"] = null;
        
        //session_name("DocuSignApp");
        session_name("DocuSign2011Q1Sample");
        
        session_start();
    }
    
    public function isLoggedIn()
    {
        $retval = false;
        if (isset($_SESSION["LoggedIn"]) && ($_SESSION["LoggedIn"] === true )){
            $retval = false;
        }
        return $retval;
    }

    public function checkSessionValue($key, $val)
    {
        $retval = false;
        if((isset($_SESSION[$key])) && ($_SESSION[$key]==$val)){
            $retval = true;
        }
        return $retval;
    }

    public function xmlpp($xml, $html_output=false) 
    {
        $level = 4;
        $indent = 0; // current indentation level
        $pretty = array();
        $retval = "";
        try {
            $xml_obj = new SimpleXMLElement($xml);
            // get an array containing each XML element
            $xml = explode("\n", preg_replace('/>\s*</', ">\n<", $xml_obj->asXML()));

            // shift off opening XML tag if present
            if (count($xml) && preg_match('/^<\?\s*xml/', $xml[0])) {
                $pretty[] = array_shift($xml);
            }

            foreach ($xml as $el) {
                if (preg_match('/^<([\w])+[^>\/]*>$/U', $el)) {
                    // opening tag, increase indent
                    $pretty[] = str_repeat(' ', $indent) . $el;
                    $indent += $level;
                } else {
                    if (preg_match('/^<\/.+>$/', $el)) {
                        $indent -= $level;  // closing tag, decrease indent
                    }
                    if ($indent < 0) {
                        $indent += $level;
                    }
                    $pretty[] = str_repeat(' ', $indent) . $el;
                }
            }
            $xml = implode("\n", $pretty);
            $retval = ($html_output) ? htmlentities($xml) : $xml;
        } catch(Exception $excp){

        }
        return $retval;
    }

    public function addToLog($desc, $msg)
    {
        $d =  date('d/m/Y H:i:s');
        if(!isset($_SESSION["traceLog"])){
            $_SESSION["traceLog"] = array();
            $_SESSION["traceLog"][] = "Log Started - " . $d . "<br/>";
        }
        if(count($_SESSION["traceLog"]) > 10 ){
            array_shift($_SESSION["traceLog"]);
        }
        $_SESSION["traceLog"][] = "[".$_SERVER['REMOTE_ADDR']."] - ".$d.": ".$desc ."<br/>" . $msg . "<br/>";
    }
    
    
    // ======================================================================================= 
    // ============================= LOGIN ===================================================
    // ======================================================================================= 
    
    public function getCredentialApi() 
    {
        include_once("/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/api/Credential.php");
        
        //echo $this->getPathX();
        //exit;
        
        $credapi_wsdl = "/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/api/Credential.wsdl";
        //$credapi_wsdl = "http://hashim.sporefanz.com/includes/Credential.wsdl";
        //$credapi_wsdl = $this->getPathX() . "Credential.wsdl";
        
        // Temporary written the code, have to delete it later on.
        /*
        if (file_exists($credapi_wsdl)) {
            echo "The file $credapi_wsdl exists";
        } else {
            echo "The file $credapi_wsdl does not exist";
        }        
        die;
        */

        $credapi_options = array
        (
            'location'=> $this->getConfig('credApiEndpoint'), 
            'trace'=>true,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS
        );
        
        try 
        {
            //$credapi = new Credential("https://demo.docusign.net/api/3.0/credential.asmx?WSDL", $credapi_options);
            $credapi = new Credential($credapi_wsdl, $credapi_options);
        } 
        catch (SoapFault $fault) 
        {
            $_SESSION["errorMessage"] = $fault;
            $credapi = null;
        }
        
        return $credapi;
    }
    
    public function initCreds() 
    {
        $_SESSION["UserID"] = $this->getConfig('userId');
        $_SESSION["Password"] = $this->getConfig('password');
        $_SESSION["IntegratorsKey"] = $this->getConfig('integratorKey');
        $_SESSION["AccountID"] = $this->getConfig('accountId');
    }
    
    public function login() 
    {
        $retval = false;
        $_SESSION["errorMessage"] = null;

        // TODO: add error handling
        $credApi = $this->getCredentialApi();

        $login = new Login();
        
        $login->Email="[" . $_SESSION["IntegratorsKey"] . "]" . $_SESSION["UserID"];
        $login->Password=$_SESSION["Password"];

        try 
        {
            $response = $credApi->Login($login);
        } 
        catch( SoapFault $fault) 
        {
            $_SESSION["errorMessage"] = $fault;
            $retval = false;
            return $retval;
        }
        

        if ($response->LoginResult->Success == true) 
        {
            $_SESSION["AccountID"] = $response->LoginResult->Accounts->Account[0]->AccountID;
            $retval = true;
        }
        else 
        {
            $_SESSION["errorMessage"] = $response->LoginResult->ErrorCode;
            $retval = false;
        }
            
        return $retval;
    }
    
    public function logout() 
    {
	session_destroy();
	return true;
    }
        
    
    // ======================================================================================= 
    // ============================= SEND DOCUMENT ===========================================
    // ======================================================================================= 
    
    public function buildEnvelope($arrData) 
    {
        include_once("/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/api/APIService.php");
        
        $envelope = new Envelope();
        
        if (isset($arrData["subject"])) {
            $envelope->Subject = $arrData["subject"];
        } 
        else 
        {
            $_SESSION["errorMessage"] = "You must have a subject";
            $this->redirectToError();
        }

        if ($arrData["emailBlurb"]) {
            $envelope->EmailBlurb = $arrData["emailBlurb"];
        } 
        else 
        {
            $_SESSION["errorMessage"] = "You must have an email blurb";
            $this->redirectToError();
        }

        $envelope->AccountId = $_SESSION["AccountID"];
        $envelope->Recipients = $this->constructDocumentRecipients($arrData);
        $envelope->Tabs = $this->addDocumentTabs(count($envelope->Recipients), $arrData);
        $envelope = $this->processOptions($envelope, $arrData);
        $envelope->Documents = $this->getDocuments($arrData);
        return $envelope;
    }

    /**
    * creates a recipient object for each row in the recipientList table and returns
    * an array of them
    * 
    * @return multitype:Recipient 
    */
    public function constructDocumentRecipients($arrData) 
    {
        $recipients = array();

        $count = count($arrData["RecipientName"]);
        for ($i = 1; $i <= $count; $i++) 
        {
            // Must contain all POST parameters
            if(empty($arrData["RecipientName"][$i]) || empty($arrData["RecipientEmail"][$i])){
                continue;
            }

            $r = new Recipient();

            $r->UserName = $arrData["RecipientName"][$i];
            $r->Email = $arrData["RecipientEmail"][$i];
            $r->RequireIDLookup = false;

            switch ($arrData["RecipientSecurity"][$i])
            {
                case "IDCheck":
                    $r->RequireIDLookup = true;
                    break;

                case "AccessCode":
                    $r->AccessCode = $arrData["RecipientSecuritySetting"][$i];
                    break;

                case "PhoneAuthentication":
                    $r->RequireIDLookup = true;
                    $r->IDCheckConfigurationName = "Phone Auth $";
                    $pa = new RecipientPhoneAuthentication();
                    $pa->RecipMayProvideNumber = true;
                    $r->PhoneAuthentication = $pa;
                    break;

                default:
                    ;break;
            }

            $r->ID = $i;
            $r->Type = RecipientTypeCode::Signer;
            $r->RoutingOrder = $i;

            if(!isset($arrData['RecipientInviteToggle'][$i]))
            {
                $r->CaptiveInfo = new RecipientCaptiveInfo();
                $r->CaptiveInfo->ClientUserId = $i;
            }

            array_push($recipients, $r);
        }

        if(empty($recipients))
        {
            $_SESSION["errorMessage"] = "You must include at least 1 Recipient";
            $this->redirectToError();
            exit;
        }

        return $recipients;
    }

    /**
    * Upload documents to send with the envelope
    * 
    * TODO: currently just uses stock documents only.
    * 
    * @return multitype:Document 
    */
    public function getDocuments($arrData) 
    {
        $documents = array();
        $id = 1;

        if (isset($arrData["stockdoc"])) 
        {
            $d = new Document();
            //$d->PDFBytes = file_get_contents("resources/Docusign_Demo_11.pdf");
            $d->PDFBytes = file_get_contents("/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/resources/Docusign_Demo_11.pdf");
                    
            $d->Name = "Demo Document";
            $d->ID = $id++;;
            $d->FileExtension = "pdf";
            array_push($documents, $d);
        } 
        else 
        {
            foreach ($_FILES as $f) 
            {
                if (isset($f["name"]) && !empty($f['tmp_name'])) 
                {
                    $d = new Document();
                    $d->PDFBytes = file_get_contents($f["tmp_name"]);
                    $d->Name = $f["name"];
                    $d->ID = $id++;
                    $temp = explode('.',$f['name']);
                    $ext = array_pop($temp);
                    $d->FileExtension = $ext;
                    array_push($documents, $d);
                };
            }
        }

        if (isset($arrData["signerattachment"])) {
            $d = new Document();
            
            $d->PDFBytes = file_get_contents("/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/resources/BlankPDF.pdf");
            $d->Name = "Signer Attachment";
            $d->ID = $id++;
            $d->FileExtension = "pdf";
            $d->AttachmentDescription = "Please attach your document here";
            array_push($documents, $d);
        }

        return $documents;
    }

    public function addDocumentTabs($count, $arrData) 
    {
        $tabs[] = new Tab();

        $pageTwo = isset($arrData["stockdoc"]) ? "2" : "1";
        $pageThree = isset($arrData["stockdoc"]) ? "3" : "1";

        if (isset($arrData["addsigs"])) {
            $company = new Tab();
            $company->Type = TabTypeCode::Company;
            $company->DocumentID = "1";
            $company->PageNumber = $pageTwo;
            $company->RecipientID = "1";
            $company->XPosition = "342";
            $company->YPosition = "303";
            array_push($tabs, $company);

            $init1 = new Tab();
            $init1->Type = TabTypeCode::InitialHere;
            $init1->DocumentID = "1";
            $init1->PageNumber = $pageThree;
            $init1->RecipientID = "1";
            $init1->XPosition = "454";
            $init1->YPosition = "281";
            array_push($tabs, $init1);

            $sign1 = new Tab();
            $sign1->Type = TabTypeCode::SignHere;
            $sign1->DocumentID = "1";
            $sign1->PageNumber = $pageTwo;
            $sign1->RecipientID = "1";
            $sign1->XPosition = "338";
            $sign1->YPosition = "330";
            array_push($tabs, $sign1);

            $fullAnchor = new Tab();
            $fullAnchor->Type = TabTypeCode::FullName;
            $anchor = new AnchorTab();
            $anchor->AnchorTabString = "Client Name (printed)";
            $anchor->XOffset = -123;
            $anchor->YOffset = 31;
            $anchor->Unit = UnitTypeCode::Pixels;
            $anchor->IgnoreIfNotPresent = true;
            $fullAnchor->AnchorTabItem = $anchor;
            $fullAnchor->DocumentID = "1";
            $fullAnchor->PageNumber = $pageTwo;
            $fullAnchor->RecipientID = "1";
            array_push($tabs, $fullAnchor);

            $date1 = new Tab();
            $date1->Type = TabTypeCode::DateSigned;
            $date1->DocumentID = "1";
            $date1->PageNumber = $pageTwo;
            $date1->RecipientID = "1";
            $date1->XPosition = "343";
            $date1->YPosition = "492";
            array_push($tabs, $date1);

            $init2 = new Tab();
            $init2->Type = TabTypeCode::InitialHere;
            $init2->DocumentID = "1";
            $init2->PageNumber = $pageThree;
            $init2->RecipientID = "1";
            $init2->XPosition = "179";
            $init2->YPosition = "583";
            $init2->ScaleValue = "0.6";
            array_push($tabs, $init2);

            if ($count > 1) {
                $sign2 = new Tab();
                $sign2->Type = TabTypeCode::SignHere;
                $sign2->DocumentID = "1";
                $sign2->PageNumber = $pageTwo;
                $sign2->RecipientID = "1";
                $sign2->XPosition = "338";
                $sign2->YPosition = "330";
                array_push($tabs, $sign2);

                $date2 = new Tab();
                $date2->Type = TabTypeCode::DateSigned;
                $date2->DocumentID = "1";
                $date2->PageNumber = $pageTwo;
                $date2->RecipientID = "1";
                $date2->XPosition = "343";
                $date2->YPosition = "492";
                array_push($tabs, $date2);
            }

            if (isset($arrData["formfields"])) {
                $favColor = new Tab();
                $favColor->Type = TabTypeCode::Custom;
                $favColor->CustomTabType = CustomTabType::Text;
                $favColor->DocumentID = "1";
                $favColor->PageNumber = $pageThree;
                $favColor->RecipientID = "1";
                $favColor->XPosition = "301";
                $favColor->YPosition = "416";

                if (isset($arrData["collabfields"])) {
                    $favColor->SharedTab = true;
                    $favColor->RequireInitialOnSharedTabChange = true;
                }

                array_push($tabs, $favColor);
            }

            if (isset($arrData["conditionalfields"])) {
                $fruitNo = new Tab();
                $fruitNo->Type = TabTypeCode::Custom;
                $fruitNo->CustomTabType = CustomTabType::Radio;
                $fruitNo->CustomTabRadioGroupName= "fruit";
                $fruitNo->TabLabel = "No";
                $fruitNo->Name = "No";
                $fruitNo->DocumentID = "1";
                $fruitNo->PageNumber = $pageThree;
                $fruitNo->RecipientID = "1";
                $fruitNo->XPosition = "296";
                $fruitNo->YPosition = "508";
                array_push($tabs, $fruitNo);

                $fruitYes = new Tab();
                $fruitYes->Type = TabTypeCode::Custom;
                $fruitYes->CustomTabType = CustomTabType::Radio;
                $fruitYes->CustomTabRadioGroupName= "fruit";
                $fruitYes->TabLabel = "Yes";
                $fruitYes->Name = "Yes";
                $fruitYes->Value = "Yes";
                $fruitYes->DocumentID = "1";
                $fruitYes->PageNumber = $pageThree;
                $fruitYes->RecipientID = "1";
                $fruitYes->XPosition = "202";
                $fruitYes->YPosition = "509";
                array_push($tabs, $fruitYes);

                $data1 = new Tab();
                $data1->Type = TabTypeCode::Custom;
                $data1->CustomTabType = CustomTabType::Text;
                $data1->ConditionalParentLabel = "fruit";
                $data1->ConditionalParentValue = "Yes";
                $data1->TabLabel = "Preferred Fruit";
                $data1->Name = "Fruit";
                $data1->DocumentID = "1";
                $data1->PageNumber = $pageThree;
                $data1->RecipientID = "1";
                $data1->XPosition = "265";
                $data1->YPosition = "547";
                array_push($tabs, $data1);
            }

            if (isset($arrData["signerattachment"])) {
                $attach = new Tab();
                $attach->Type = TabTypeCode::SignerAttachment;
                $attach->TabLabel = "Signer Attachment";
                $attach->Name = "Signer Attachment";
                $attach->DocumentID = "2";
                $attach->PageNumber = "1";
                $attach->RecipientID = "1";
                $attach->XPosition = "20";
                $attach->YPosition = "20";
                array_push($tabs, $attach);
            }
        }

        // eliminate 0th element
        array_shift($tabs);

        return $tabs;
    }
    
    public function processOptions($envelope, $arrData) 
    {
        if (isset($arrData["markup"])) {
            $envelope->AllowMarkup = true;
        }
        if (isset($arrData["enablepaper"])) {
            $envelope->EnableWetSign = true;
        }
        if ($arrData["reminders"] != null) {
            $remind = new DateTime($arrData["reminders"]);
            $now = new DateTime("now");
            $days = $now->diff($remind)->d;

            if ($envelope->Notification == null) {
                $envelope->Notification = new Notification();
            }
            $envelope->Notification->Reminders = new Reminders();
            $envelope->Notification->Reminders->ReminderEnabled = true;
            $envelope->Notification->Reminders->ReminderDelay = $days;
            $envelope->Notification->Reminders->ReminderFrequency = "2";
        }
        if ($arrData["expiration"] !=null) {
            $expire = new DateTime($arrData["expiration"]);
            $now = new DateTime("now");
            $days = $now->diff($expire)->d;

            if ($envelope->Notification == null) {
                $envelope->Notification = new Notification();
            }
            $envelope->Notification->Expirations = new Expirations();
            $envelope->Notification->Expirations->ExpireEnabled = true;
            $envelope->Notification->Expirations->ExpireAfter = $days;
            $envelope->Notification->Expirations->ExpireWarn = $days - 2;
        }

        return $envelope;
    }
    
    public function sendDocumentNow($envelope) 
    {
        $api = $this->getAPI();

        /*
        $header='<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" SOAP-ENV:mustUnderstand="1">
        <wsse:UsernameToken xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
        <wsse:Username>'.$UserID.'</wsse:Username>
        <wsse:Password type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'.$Password.'</wsse:Password>
        </wsse:UsernameToken>
        </wsse:Security>';

        // Set the SOAP Header variables
        $soap_var_header = new SoapVar( $header, XSD_ANYXML, null, null, null );
        $soap_header = new SoapHeader( 'https://demo.docusign.net/api/3.0/api.asmx', 'wsse', $soap_var_header );
        $soapClient->__setSoapHeaders(array($soap_header));
        */

        $result = array();
        $csParams = new CreateAndSendEnvelope();
        $csParams->Envelope = $envelope;

        try 
        {
            $status = $api->CreateAndSendEnvelope($csParams)->CreateAndSendEnvelopeResult;
            if ($status->Status == EnvelopeStatusCode::Sent) 
            {
                $this->addEnvelopeID($status->EnvelopeID);
                
                $result['status'] = true;
                $result['title'] = 'Success';
                $result['message'] = 'Document successfuly sent';
                $result['url'] = "http://hashimadmin.sporefanz.com/docusign/admin/getStatusAndDocs/envelopid_$status->EnvelopeID/accountID_$envelope->AccountId/source_Document";
            }
        } 
        catch (SoapFault $e) 
        {
            $result['status'] = false;
            $result['title'] = 'Success';
            $result['message'] = "There are some errors in processing your request, please try again later of contact your system's administrator";
            $result['url'] = "http://hashimadmin.sporefanz.com/docusign/admin/error";
        }
        
        return $result;
    }
   
    public function embedDocumentSending($envelope) 
    {
        $api = $this->getAPI();

        $ceParams = new CreateEnvelope();
        $ceParams->Envelope = $envelope;
        
        try 
        {
            $status = $api->CreateEnvelope($ceParams)->CreateEnvelopeResult;
            
            if ($status->Status == EnvelopeStatusCode::Created) 
            {
                $rstParam = new RequestSenderToken();
                $rstParam->AccountID = $envelope->AccountId;
                $rstParam->EnvelopeID = $status->EnvelopeID;
                $rstParam->ReturnURL = $this->getCallbackURL("getstatusanddocs.php");
                $this->addEnvelopeID($status->EnvelopeID);
                $_SESSION["embedToken"] = $api->RequestSenderToken($rstParam)->RequestSenderTokenResult;
                
                $result['status'] = true;
                $result['url'] = "http://hashimadmin.sporefanz.com/docusign/admin/embedSending/envelopid_$status->EnvelopeID/accountID_$envelope->AccountId/source_Document";
                $result['message'] = "Press the close button to redirect you to the document edit page.";
                $result['title'] = "Redirect";
                
                //$this->redirectToEmbedSending($status->EnvelopeID, $envelope->AccountId);
            }
        } 
        catch (SoapFault $e) 
        {
            $result['status'] = false;
            $result['url'] = "";
            $result['message'] = "There are some errors in processing your request, please try again later of request your system's administrator";
            $result['title'] = "Error";
            
            //$_SESSION["errorMessage"] = $e;
            //$this->redirectToError();
        }
        
        return $result;
    }
    
    
    // ======================================================================================= 
    // ================================= SEND TEMPLATE =======================================
    // ======================================================================================= 
    
    public function voidSampleEnvelope($envelopeID) 
    {
        $dsapi = $this->getAPI();
        $veParams = new VoidEnvelope();
        $veParams->EnvelopeID = $envelopeID;
        $veParams->Reason = "Envelope voided by sender";
        try 
        {
            $status = $dsapi->VoidEnvelope($veParams)->VoidEnvelopeResult;
            if ($status->VoidSuccess) {
                header("Location: sendatemplate.php");
            }
        } 
        catch (SoapFault $e) 
        {
            $_SESSION["errorMessage"] = $e;
            //header("Location: error.php");
            $this->redirectToError();
        }
    }

    public function createSampleEnvelope($arrData) 
    {
        
        include_once("/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/api/APIService.php");

        // Create envelope information
        $envinfo = new EnvelopeInformation();
        $envinfo->Subject = $_POST["subject"];
        $envinfo->EmailBlurb = $_POST["emailblurb"];
        $envinfo->AccountId = $_SESSION["AccountID"];

        if ($_POST["reminders"] != null) 
        {
            $remind = new DateTime($_POST["reminders"]);
            $new = new DateTime($_SERVER['REQUEST_TIME']);
            $days = $now->diff($remind)->d;
            if ($envinfo->Notification == null) 
            {
                $envinfo->Notification = new Notification();
            }
            $envinfo->Notification->Reminders = new Reminders();
            $envinfo->Notification->Reminders->ReminderEnabled = true;
            $envinfo->Notification->Reminders->ReminderDelay = $days;
            $envinfo->Notification->Reminders->ReminderFrequency = "2";
        }

        if ($_POST["expiration"] != null) 
        {
            $expire = new DateTime($_POST["reminders"]);
            $new = new DateTime($_SERVER['REQUEST_TIME']);
            $days = $now->diff($expire)->d;
            if ($envinfo->Notification == null) 
            {
                $envinfo->Notification = new Notification();
            }
            $envinfo->Notification->Expirations = new Expirations();
            $envinfo->Notification->Expirations->ExpireEnabled = true;
            $envinfo->Notification->Expirations->ExpireDelay = $days;
            $envinfo->Notification->Expirations->ExpireFrequency = "2";
        }

        // Get all recipients
        $recipients = $this->constructRecipients();

        // Construct the template reference
        $tref = new TemplateReference();
        $tref->TemplateLocation = TemplateLocationCode::Server;
        $tref->Template = $_POST["TemplateID"];
        $tref->RoleAssignments = $this->createFinalRoleAssignments($recipients);
        $trefs = array($tref);

        if (isset($_POST["SendNow"])) {
            $this->sendNow($trefs, $envinfo, $recipients);
        }
        else  {
            $this->embedSending($trefs, $envinfo, $recipients);
        }
    }

    /**
    *	TODO: get list of recipients from table
    */
    public function constructRecipients() 
    {
        $recipients[] = new Recipient();

        $count = count($_POST["RoleName"]);
        for ($i = 1; $i <= $count; $i++) 
        {
            if ($_POST["RoleName"] != null) 
            {
                $r = new Recipient();
                $r->UserName = $_POST["Name"][$i];
                $r->Email = $_POST["RoleEmail"][$i];
                $r->ID = $i;
                $r->RoleName = $_POST["RoleName"][$i];
                $r->Type = RecipientTypeCode::Signer;
                array_push($recipients, $r);
            }
        }

        // eliminate 0th element
        array_shift($recipients);

        return $recipients;
    }

    /**
    * Create an array of
    * 
    * @return multitype:TemplateReferenceRoleAssignment
    */
    public function createFinalRoleAssignments($recipients) 
    {
        $roleAssignments[] = new TemplateReferenceRoleAssignment();

        foreach ($recipients as $r) 
        {
            $assign = new TemplateReferenceRoleAssignment();
            $assign->RecipientID = $r->ID;
            $assign->RoleName = $r->RoleName;
            array_push($roleAssignments, $assign);
        }

        // eliminate 0th element
        array_shift($roleAssignments);

        return $roleAssignments;
    }

    public function loadTemplates() 
    {
        $dsapi = $this->getAPI();

        $rtParams = new RequestTemplates();
        $rtParams->AccountID = $_SESSION["AccountID"];
        $rtParams->IncludeAdvancedTemplates = true;

        $templates = array();
        try {
            $templates = $dsapi->RequestTemplates($rtParams)->RequestTemplatesResult->EnvelopeTemplateDefinition;
        } 
        catch (SoapFault $e) 
        {
            $_SESSION["errorMessage"] = $e;
            $this->redirectToError();

            //echo "<option value='0'>No Templates Available</option>";
        }

        foreach ($templates as $template) 
        {
            echo '<option value="' . $template->TemplateID . '">' .
            $template->Name . ' - ' . $template->TemplateID . "</option>\n";
                            // echo $template->TemplateID . " " . $template->Name . "<br />" . "\n";
        }
    }

    public function sendNow($templateReferences, $envelopeInfo, $recipients) 
    {
        $api = $this->getAPI();

        $csParams = new CreateEnvelopeFromTemplates();
        $csParams->EnvelopeInformation = $envelopeInfo;
        $csParams->Recipients = $recipients;
        $csParams->TemplateReferences = $templateReferences;
        $csParams->ActivateEnvelope = true;
        try 
        {
            $status = $api->CreateEnvelopeFromTemplates($csParams)->CreateEnvelopeFromTemplatesResult;
            if ($status->Status == EnvelopeStatusCode::Sent) 
            {
                $this->addEnvelopeID($status->EnvelopeID);
                $this->redirectToGetStatusAndDocs($status->EnvelopeID, $envelope->AccountId);
            }
        } 
        catch (SoapFault $e) 
        {
            $_SESSION["errorMessage"] = array($e, $api->__getLastRequest(), $csParams);
            //header("Location: error.php");
            $this->redirectToError();
        }
    }

    public function embedSending($templateReferences, $envelopeInfo, $recipients) 
    {
        $api = $this->getAPI();

        $ceParams = new CreateEnvelopeFromTemplates();
        $ceParams->EnvelopeInformation = $envelopeInfo;
        $ceParams->Recipients = $recipients;
        $ceParams->TemplateReferences = $templateReferences;
        $ceParams->ActivateEnvelope = false;
        try 
        {
            $status = $api->CreateEnvelopeFromTemplates($ceParams)->CreateEnvelopeFromTemplatesResult;
            if ($status->Status == EnvelopeStatusCode::Created) 
            {
                $rstParam = new RequestSenderToken();
                $rstParam->AccountID = $envelopeInfo->AccountId;
                $rstParam->EnvelopeID = $status->EnvelopeID;
                $rstParam->ReturnURL = $this->getCallbackURL("getstatusanddocs.php");
                $this->addEnvelopeID($status->EnvelopeID);
                $_SESSION["embedToken"] = $api->RequestSenderToken($rstParam)->RequestSenderTokenResult;
                header("Location: embedsending.php?envelopid=" . $status->EnvelopeID . 
                    "&accountID=" . $envelope->AccountId . "&source=Document");
            }
        } 
        catch (SoapFault $e) 
        {
            $_SESSION["errorMessage"] = $e;
            //header("Location: error.php");
            $this->redirectToError();
        }
    }


    // ======================================================================================= 
    // ================================= EMBED DOCUSIGN ======================================
    // ======================================================================================= 

    public function createAndSend($oneSigner) 
    {
        include_once("/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/api/APIService.php");
        
        $status = "";
        $env = new Envelope();
        $env->Subject = "DocuSign API SDK Sample";
        $env->EmailBlurb = "This envelope demonstrates embedded signing";
        $env->AccountId = $_SESSION["AccountID"];
        $env->Recipients = $this->constructRecipientsExd($oneSigner);
        $doc = new Document();
        
        $doc->PDFBytes = file_get_contents("/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/resources/Docusign_Demo_11.pdf");
        $doc->Name = "Demo Document";
        $doc->ID = "1";
        $doc->FileExtension = "pdf";
        $env->Documents = array($doc);
        $env->Tabs = $this->addTabs(count($env->Recipients));
        $api = $this->getAPI();
        try 
        {
            $csParams = new CreateAndSendEnvelope();
            $csParams->Envelope = $env;
            $status = $api->CreateAndSendEnvelope($csParams)->CreateAndSendEnvelopeResult;
            
            $this->addEnvelopeID($status->EnvelopeID);
            $this->getToken($status, 1);
        } 
        catch (SoapFault $e) 
        {
            $_SESSION["errorMessage"] = $e;
            $this->redirectToError();
        }
    }

    public function constructRecipientsExd($oneSigner) 
    {
        $recipients[] = new Recipient();
        $r1 = new Recipient();
        $r1->UserName = $_SESSION["UserID"];
        $r1->Email = $_SESSION["UserID"];
        $r1->ID = 1;
        $r1->Type = RecipientTypeCode::Signer;
        $r1->CaptiveInfo = new RecipientCaptiveInfo();
        $r1->CaptiveInfo->ClientUserId = 1;
        array_push($recipients, $r1);
        
        if ($oneSigner != true) 
        {
            $r2 = new Recipient();
            $r2->UserName = "DocuSign Recipient2";
            $r2->Email = "DocuSignRecipient2@mailinator.com";
            $r2->ID = 2;
            $r2->Type = RecipientTypeCode::Signer;
            $r2->CaptiveInfo = new RecipientCaptiveInfo();
            $r2->CaptiveInfo->ClientUserId = 2;
            array_push($recipients, $r2);
        }    

        array_shift($recipients);
        return $recipients;
    }

    public function addTabs($count) 
    {
        $tabs[] = new Tab();
        
        $company = new Tab();
        $company->Type = TabTypeCode::Company;
        $company->DocumentID = "1";
        $company->PageNumber = "2";
        $company->RecipientID = "1";
        $company->XPosition = "342";
        $company->YPosition = "303";
        array_push($tabs, $company);
            
        $init1 = new Tab();
        $init1->Type = TabTypeCode::InitialHere;
        $init1->DocumentID = "1";
        $init1->PageNumber = "3";
        $init1->RecipientID = "1";
        $init1->XPosition = "454";
        $init1->YPosition = "281";
        array_push($tabs, $init1);
            
        $sign1 = new Tab();
        $sign1->Type = TabTypeCode::SignHere;
        $sign1->DocumentID = "1";
        $sign1->PageNumber = "2";
        $sign1->RecipientID = "1";
        $sign1->XPosition = "338";
        $sign1->YPosition = "330";
        array_push($tabs, $sign1);
            
        $fullAnchor = new Tab();
        $fullAnchor->Type = TabTypeCode::FullName;
        $anchor = new AnchorTab();
        $anchor->AnchorTabString = "Client Name (printed)";
        $anchor->XOffset = -123;
        $anchor->YOffset = 31;
        $anchor->Unit = UnitTypeCode::Pixels;
        $anchor->IgnoreIfNotPresent = true;
        $fullAnchor->AnchorTabItem = $anchor;
        $fullAnchor->DocumentID = "1";
        $fullAnchor->PageNumber = "2";
        $fullAnchor->RecipientID = "1";
        array_push($tabs, $fullAnchor);
            
        $date1 = new Tab();
        $date1->Type = TabTypeCode::DateSigned;
        $date1->DocumentID = "1";
        $date1->PageNumber = "2";
        $date1->RecipientID = "1";
        $date1->XPosition = "343";
        $date1->YPosition = "492";
        array_push($tabs, $date1);
            
        $init2 = new Tab();
        $init2->Type = TabTypeCode::InitialHere;
        $init2->DocumentID = "1";
        $init2->PageNumber = "3";
        $init2->RecipientID = "1";
        $init2->XPosition = "179";
        $init2->YPosition = "583";
        $init2->ScaleValue = "0.6";
        array_push($tabs, $init2);
            
        if ($count > 1) 
        {
            $sign2 = new Tab();
            $sign2->Type = TabTypeCode::SignHere;
            $sign2->DocumentID = "1";
            $sign2->PageNumber = "3";
            $sign2->RecipientID = "2";
            $sign2->XPosition = "339";
            $sign2->YPosition = "97";
            array_push($tabs, $sign2);

            $date2 = new Tab();
            $date2->Type = TabTypeCode::DateSigned;
            $date2->DocumentID = "1";
            $date2->PageNumber = "3";
            $date2->RecipientID = "2";
            $date2->XPosition = "339";
            $date2->YPosition = "97";
            array_push($tabs, $date2);
        }
            
        $favColor = new Tab();
        $favColor->Type = TabTypeCode::Custom;
        $favColor->CustomTabType = CustomTabType::Text;
        $favColor->DocumentID = "1";
        $favColor->PageNumber = "3";
        $favColor->RecipientID = "1";
        $favColor->XPosition = "301";
        $favColor->YPosition = "416";
        array_push($tabs, $favColor);
        
        $fruitNo = new Tab();
        $fruitNo->Type = TabTypeCode::Custom;
        $fruitNo->CustomTabType = CustomTabType::Radio;
        $fruitNo->CustomTabRadioGroupName= "fruit";
        $fruitNo->TabLabel = "No";
        $fruitNo->Name = "No";
        $fruitNo->DocumentID = "1";
        $fruitNo->PageNumber = "3";
        $fruitNo->RecipientID = "1";
        $fruitNo->XPosition = "296";
        $fruitNo->YPosition = "508";
        array_push($tabs, $fruitNo);
        
        $fruitYes = new Tab();
        $fruitYes->Type = TabTypeCode::Custom;
        $fruitYes->CustomTabType = CustomTabType::Radio;
        $fruitYes->CustomTabRadioGroupName = "fruit";
        $fruitYes->TabLabel = "Yes";
        $fruitYes->Name = "Yes";
        $fruitYes->Value = "Yes";
        $fruitYes->DocumentID = "1";
        $fruitYes->PageNumber = "3";
        $fruitYes->RecipientID = "1";
        $fruitYes->XPosition = "202";
        $fruitYes->YPosition = "509";
        array_push($tabs, $fruitYes);
            
        $data1 = new Tab();
        $data1->Type = TabTypeCode::Custom;
        $data1->CustomTabType = CustomTabType::Text;
        $data1->ConditionalParentLabel = "fruit";
        $data1->ConditionalParentValue = "Yes";
        $data1->TabLabel = "Preferred Fruit";
        $data1->Name = "Fruit";
        $data1->DocumentID = "1";
        $data1->PageNumber = "3";
        $data1->RecipientID = "1";
        $data1->XPosition = "265";
        $data1->YPosition = "547";
        array_push($tabs, $data1);
        
        /* eliminate 0th element */
        array_shift($tabs);
        
        return $tabs;
    }

    public function getToken($status, $clientID) 
    {
        $token = null;
        $_SESSION["embedToken"];
           
        // get recipient token
        $assertion = new RequestRecipientTokenAuthenticationAssertion();
        $assertion->AssertionID = $this->guid();
        $assertion->AuthenticationInstant = $this->todayXsdDate();
        $assertion->AuthenticationMethod = RequestRecipientTokenAuthenticationAssertionAuthenticationMethod::Password;
        $assertion->SecurityDomain = "DocuSign2011Q1Sample";
        
        // Get Recipient fro ClientID
        $recipient = false;
        foreach($status->RecipientStatuses->RecipientStatus as $rs)
        {
            if($rs->ClientUserId == $clientID){
                $recipient = $rs;
            }
        }
        
        if($recipient == false)
        {
            $_SESSION["errorMessage"] = "Unable to find Recipient";
            //header("Location: error.php");
            $this->redirectToError();
        }
        
        $urls = new RequestRecipientTokenClientURLs();
        //$urlbase = $this->getCallbackURL("pop.html") . "?source=Embedded";
        
        $urlbase = "http://hashimadmin.sporefanz.com/docusign/admin/pop/q_?source=Embedded";
        
        $urls->OnAccessCodeFailed = $urlbase . "&event=AccessCodeFailed&uname=" . $recipient->UserName;
        $urls->OnCancel = $urlbase . "&event=Cancel&uname=" . $recipient->UserName;
        $urls->OnDecline = $urlbase . "&event=Decline&uname=" . $recipient->UserName;
        $urls->OnException = $urlbase . "&event=Exception&uname=" . $recipient->UserName;
        $urls->OnFaxPending = $urlbase . "&event=FaxPending&uname=" . $recipient->UserName;
        $urls->OnIdCheckFailed = $urlbase . "&event=IdCheckFailed&uname=" . $recipient->UserName;
        $urls->OnSessionTimeout = $urlbase . "&event=SessionTimeout&uname=" . $recipient->UserName;
        $urls->OnTTLExpired = $urlbase . "&event=TTLExpired&uname=" . $recipient->UserName;
        $urls->OnViewingComplete = $urlbase . "&event=ViewingComplete&uname=" . $recipient->UserName;

        if ($this->_oneSigner) {
            $urls->OnSigningComplete = $urlbase . "&event=SigningComplete&uname=" . $recipient->UserName;
        }
        else {
            
            
            $urlbasePop2 = "http://hashimadmin.sporefanz.com/docusign/admin/pop2/q_?envelopeID=". $status->EnvelopeID;
            $urls->OnSigningComplete = $urlbasePop2;
            
            //$urls->OnSigningComplete = $this->getCallbackURL("pop2.html") . "?envelopeID=" . $status->EnvelopeID;
        }
        
        //vx($urls);
        
        $api = $this->getAPI();
        $rrtParams = new RequestRecipientToken();
        $rrtParams->AuthenticationAssertion = $assertion;
        $rrtParams->ClientURLs = $urls;
        $rrtParams->ClientUserID = $recipient->ClientUserId;
        $rrtParams->Email = $recipient->Email;
        $rrtParams->EnvelopeID = $status->EnvelopeID;
        $rrtParams->Username = $recipient->UserName;
        
        //vx($rrtParams);
        
        try {
            $token = $api->RequestRecipientToken($rrtParams)->RequestRecipientTokenResult;
        } 
        catch (SoapFault $e) 
        {
            $_SESSION["errorMessage"] = $e;
            //header("Location: error.php");
            $this->redirectToError();
        }
        
        $_SESSION["embedToken"] = $token;
    }

    public function getStatus($envelopeID) 
    {
        $status = null;
        $api = $this->getAPI();
        $rsParams = new RequestStatus();
        $rsParams->EnvelopeID = $envelopeID;
        
        try {
            $status = $api->RequestStatus($rsParams)->RequestStatusResult;
        } 
        catch (SoapFault $e) 
        {
            $_SESSION["errorMessage"] = $e;
            //header("Location: error.php");
            $this->redirectToError();
        }
        
        return $status;
    }
    
    
    // ======================================================================================= 
    // ================================= GET STATUS AND DOCS =================================
    // ======================================================================================= 
    
    public function getStatusAndDocs()
    {
        include_once("/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/api/APIService.php");
        
        $statuses = '';
        
        if (isset($_SESSION["EnvelopeIDs"]) && count($_SESSION["EnvelopeIDs"]) > 0)
        {
            $api = $this->getAPI();

            $filter = new EnvelopeStatusFilter();
            $filter->AccountId = $_SESSION["AccountID"];
            $filter->EnvelopeIds = $_SESSION["EnvelopeIDs"];
            
            try 
            {
                $rsexParams = new RequestStatusesEx();
                $rsexParams->EnvelopeStatusFilter = $filter;
                $statuses = $api->RequestStatusesEx($rsexParams)->RequestStatusesExResult;
            } 
            catch (SoapFault $e) 
            {
                $_SESSION["errorMessage"] = $e;
                $logic->redirectToError();
            }
        }
        
        return $statuses;
    }
    
    
    // ======================================================================================= 
    // ================================= ENVELOP STATUS ======================================
    // ======================================================================================= 
    
    function getEnvelopeStatus($envelope_id)
    {
	// Start API
	$api = $this->getAPI();
	
	// Create parameters for RequestStatus
	$requestStatusparams = new RequestStatus();
	$requestStatusparams->EnvelopeID = $envelope_id;
	$result = $api->RequestStatus($requestStatusparams); // Removed the array() around the $api call
	
	return $result;
    }
    
    
    // ======================================================================================= 
    // ================================= GET PDF =============================================
    // ======================================================================================= 
    
    function getRequestPDF($envelope_id)
    {
        // Start API
        $api = $this->getAPI();

        // Create parameters for RequestStatus
        $requestPDFparams = new RequestPDF();
        $requestPDFparams->EnvelopeID = $envelope_id;

        $result = $api->RequestPDF($requestPDFparams); // Removed the array() around the $api call

        return $result;
    }
    
        
    // ======================================================================================= 
    // ================================= CONNECT =============================================
    // ======================================================================================= 
    
    function guidConnect()
    {
        $uuid = '';
        if (function_exists('com_create_guid'))
        {
            $uuid = com_create_guid();
            // somehow the function com_create_guid includes {}, while our webservice
            // doesn't.  Here we are changint the format by taking those curly braces out.
            $uuid = str_ireplace("{", "", $uuid );
            $uuid = str_ireplace("}", "", $uuid );
        }
        else
        {
            mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid =  substr($charid, 0, 8).$hyphen
                    .substr($charid, 8, 4).$hyphen
                    .substr($charid,12, 4).$hyphen
                    .substr($charid,16, 4).$hyphen
                    .substr($charid,20,12);
        }
        return $uuid;
    }

    function connect()
    {
        $postBackPath = empty($_SERVER['HTTPS']) ? 'http://' : 'https://';
        $postBackPath .= ($_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'] );
        
        //ex($postBackPath);
        
        //ostBackPath = http://hashim.sporefanz.com/includes

        $postedXml = @file_get_contents('php://input');

        if ( !empty($_POST['post'])) 
        {
            // if this is a sample load
            $xml = simplexml_load_file("post.sample") or die("Unable to load sample XML file!");
            $xml->EnvelopeStatus->EnvelopeID = $this->guidConnect(); // here we replace the GUID so we have unique files
            //
            // using the curl library to get the post
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $postBackPath);
            curl_setopt($curl, CURLOPT_HEADER, array("Content-Type: application/xml"));
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $xml->asXML());
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_exec ( $curl );
            curl_close ($curl); 
            
        } 
        else if( ! empty( $postedXml ) ) 
        {
            // see if this is a post to this page
            // if it is then we have to save it.
            $xml = simplexml_load_string($postedXml);
            print 'Got Envelope ID: ' . $xml->EnvelopeStatus->EnvelopeID . '<br/>';
            file_put_contents($xml->EnvelopeStatus->EnvelopeID . '.xml', $xml->asXML()); 
        }

        // now load the posts we already have into an array
        $posts = array();
        
        
        if ($dh = opendir('.')) 
        //if ($dh = opendir('/mnt/stor1-wc2-dfw1/444787/dev.imranmedia.com/web/devs/hashim/docusign/v1/connect')) 
        {
            // iterate over file list
            while (($filename = readdir($dh)) !== false) {
                // if filename matches search pattern, print it
                if (ereg('.*\.xml', $filename)) 
                {
                    $xml = simplexml_load_file($filename);
                    $posts[] = $xml;
                }
            }
            // close directory
            closedir($dh);
        }
        return $posts;
    }

    
}
?>