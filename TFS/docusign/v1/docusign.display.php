<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class DocuSignDisplay extends DashboardAppDisplay
{
    public static $urlVars = array();

    public function login()
    {
        // Include js, css resources in page.
        $this->regResources();
        
        $html = new htmlBlock;
        
        $html 
            ->push(tag::div())
                ->class("dropshadow-bottomleft")
                ->push(tag::p()) 
                    ->class("leftalign")
                    ->push(tag::b())
                        ->append("Log In")
                    ->ascend()
                ->ascend()
                
                ->push(tag::hr()) 
                ->ascend()
                
                ->push(tag::form()) 
                    ->action($this->getPathX() . "admin/loginSubmit") 
                    ->id("logInForm") 
                    ->method("post")
                
                
                    ->push(tag::div()) 
                        ->id("container") 
                        ->class("centeralign") 
                        ->style("width: 300px; margin-top: 30px;")
                        ->push(tag::table()) 
                            ->style("width: 300px;") 
                            ->align("center")
                            ->push(tag::tr())
                                ->push(tag::td())
                                    ->append("E-mail")
                                ->ascend()
                                ->push(tag::td())
                                    ->push(tag::input()) 
                                        ->id("DevCenterEmail") 
                                        ->type("text") 
                                        ->name("DevCenterEmail") 
                                        ->value("hmaqbool@folio3.com") 
                                    ->ascend()
                                ->ascend()
                            ->ascend()
                
                            ->push(tag::tr())
                                ->push(tag::td())
                                    ->append("Password")
                                ->ascend()
                                ->push(tag::td())
                                    ->push(tag::input()) 
                                        ->id("DevCenterPassword") 
                                        ->type("password") 
                                        ->name("DevCenterPassword") 
                                        ->value("test123") 
                                    ->ascend()
                                ->ascend()
                            ->ascend()
                            ->push(tag::tr())
                                ->push(tag::td())
                                    ->append("Integrator Key")
                                ->ascend()
                                ->push(tag::td())
                                    ->push(tag::input()) 
                                        ->id("DevCenterIKey") 
                                        ->type("password") 
                                        ->name("DevCenterIKey") 
                                        ->value("TEST-6932f29e-997f-46bb-adb4-1d90973b9a74") 
                                    ->ascend()
                                ->ascend()
                            ->ascend()
                        ->ascend()
                        
                        
                        /*
                        ->push(tag::div())>
                            ->push(tag::table()) class("triangle-isosceles left") align("center")
                                    ->push(tag::tr())>
                                            ->push(tag::td())><img alt("") src("images/follow-us_reasonably_small.png") style("height: 25px; width: 25px") ->ascend()->ascend()
                                            ->push(tag::td())>->push(tag::b())Need a DevCenter Account?->ascend()->push(tag::br())
                                                    Get it
                                                    ->push(tag::a()) href("http://www.docusign.com/developers-center/get-free-developer-account")
                                                            here
                                                    </a>.->ascend()
                                    ->ascend()
                            ->ascend()
                        ->ascend()
                        */
                
                        ->push(tag::div()) 
                            ->id("action") 
                            ->class("centeralign") 
                            ->style("width: 200px; margin-top: 20px;")
                            ->push(tag::input()) 
                                ->id("Submit1") 
                                ->name("submit") 
                                ->type("submit") 
                                ->value("Login") 
                                ->style("width: 70px; margin-right: 5px;") 
                            ->ascend() 
                            ->push(tag::input()) 
                                ->id("Reset1") 
                                ->name("reset") 
                                ->type("submit") 
                                ->value("Reset") 
                                ->style("width: 70px; margin-left: 5px;") 
                            ->ascend()
                        ->ascend()
                    ->ascend()
                ->ascend()
            ->ascend()
                ;
        
        return $html;
    }
    
    public function regResources()
    {
        // Access to the Template controller object
        global $template;
        
        // Loaded jQuery colorbox.js file in the header
        $template->pushHeader('<link rel="stylesheet" href="http://hashim.sporefanz.com/includes/css/docusign/default.css" />');
        $template->pushHeader('<link rel="stylesheet" href="http://hashim.sporefanz.com/includes/css/docusign/jquery.ui.all.css" />');
        $template->pushHeader('<link rel="stylesheet" href="http://hashim.sporefanz.com/includes/css/docusign/SendDocument.css" />');
        
        // $template->pushHeader('<script src="http://hashim.sporefanz.com/includes/js/docusign/jquery-1.4.4.js"></script>');
        $template->pushHeader('<script src="http://hashim.sporefanz.com/includes/js/docusign/jquery.ui.core.js"></script>');
        $template->pushHeader('<script src="http://hashim.sporefanz.com/includes/js/docusign/jquery.ui.widget.js"></script>');
        $template->pushHeader('<script src="http://hashim.sporefanz.com/includes/js/docusign/Utils.js"></script>');
        $template->pushHeader('<script src="http://hashim.sporefanz.com/includes/js/docusign/jquery.ui.datepicker.js"></script>');
    }
    
    public function regClientEventsForSendDocument()
    {
        // Include js, css resources in page.
        $this->regResources();
        
        // Access to the Template controller object
        global $template;
        
        $template->pushReadyFunc('getTabSelected(Tab.SEND_DOCUMENT);');
        
        // jQuery event bindings and plugin calls here
        $template->pushHeader('

            <script type="text/javascript">
                function EnableDisableDiv() {
                    if ($("#sendoption").attr("checked")) {
                        $("#files").hide();
                        $("#files").disableSelection();
                    } else {
                        $("#files").show();
                        $("#files").enableSelection();
                    }
                    }
            </script>
            
            <script type="text/javascript">
                $(document).ready(function () {
                    var today = new Date().getDate();
                    $("#reminders").datepicker({
                        showOn: "button",
                        buttonImage: "http://hashim.sporefanz.com/includes/images/docusign/calendar-blue.gif",
                        buttonImageOnly: true,
                        minDate: today
                    });
                    $("#expiration").datepicker({
                        showOn: "button",
                        buttonImage: "http://hashim.sporefanz.com/includes/images/docusign/calendar-blue.gif",
                        buttonImageOnly: true,
                        minDate: today + 3
                    });
                    $(".switcher li").bind("click", function (){
                        //var $act = $(this);
                        $(this).parent().children("li").removeClass("active").end();
                        $(this).addClass("active");
                        if($(this).find("a").attr("title") == "On"){
                            $(this).parent().find("input").attr("checked","checked");
                        } else {
                            $(this).parent().find("input").removeAttr("checked");
                        }
                    });
                });
            </script>
        ');
    }
    
    public function regClientEventsForSendTemplates()
    {
        $this->regResources();
        
        global $template;
        
        $template->pushReadyFunc('getTabSelected(Tab.SEND_TEMPLATE);');
        
        $template->pushHeader('
            <script type="text/javascript" charset="utf-8">
                $(function () {
                    var today = new Date().getDate();
                    $("#reminders").datepicker({
                        showOn: "button",
                        buttonImage: "http://hashim.sporefanz.com/includes/images/docusign/calendar-blue.gif",
                        buttonImageOnly: true,
                        minDate: today
                    });

                    $("#expiration").datepicker({
                        showOn: "button",
                        buttonImage: "http://hashim.sporefanz.com/includes/images/docusign/calendar-blue.gif",
                        buttonImageOnly: true,
                        minDate: today
                    });

                    $(".switcher li").bind("click", function () {
                        var act = $(this);
                        $(act).parent().children("li").removeClass("active").end();
                        $(act).addClass("active");
                    });
                });
            </script>
        ');
    }
    
    public function sendDocument()
    {
        $this->regClientEventsForSendDocument();
        
        ob_start();
        ?>

        <form method="post" onsubmit='return false;' id="sendDocumentSubmit">
            <input id="subject" name="subject" type="text" value="Test Subject" placeholder="<enter the subject>" autocomplete="off"/>
            <br /><br />
            <textarea id="emailblurb" cols="140" name="emailBlurb" placeholder="<enter the e-mail blurb>" rows="4" class="email">Test Body</textarea>
            <br /><br />
            <table id="recipientList" class="recipientList">
                <tr class="recipientListHeader">
                    <th>Recipient</th>
                    <th>E-mail</th>
                    <th>Security</th>
                    <th>Send E-mail Invite</th>
                </tr>
                <tr id="Recipient1">
                    <td><input type="text" name="RecipientName[1]" id="txtRow1" value="" ></td>
                    <td><input type="email" name="RecipientEmail[1]" id="txtRow1" value=""></td>
                    <td>
                        <select id="RecipientSecurity1" name="RecipientSecurity[1]" onchange="EnableDisableInput(1);">
                            <option value="None">None</option>
                            <option value="IDCheck">ID Check</option>
                            <option value="AccessCode">Access Code:</option>
                            <option value="PhoneAuthentication">Phone Authentication</option>
                        </select>
                        <input type="text" name="RecipientSecuritySetting[1]" id="RecipientSecuritySetting1" value="12345" style="display: none;">
                    </td>
                    <td>
                        <ul class="switcher">
                            <li class="active"><a href="#" title="On">ON</a></li>
                            <li><a href="#" title="OFF">OFF</a></li>
                            <input title="RecipientInviteToggle1" id="RecipientInviteToggle1" name="RecipientInviteToggle[1]" class="RecipientInviteToggle" type="checkbox" checked style="display: none; ">
                        </ul>
                    </td>
                </tr>
            </table>
            
            <input type="button" onclick="addRecipientRowToTable()" value="Add Recipient"/>
            
            <br />
            <br />
            
            <div id="files" style="display:none;">
                <p> Document #1: <input class="upload" id="file1" type="file" name="file1" /> </p>
                <p> Document #2: <input class="upload" id="file2" type="file" name="file2"/> </p>
            </div>
            
            <table class="optionlist">
                <tr>
                    <td> 
                        <input id="sendoption" class="options" type="checkbox" value="stockdoc" name="stockdoc"  onclick="EnableDisableDiv()" checked /> 
                        Use a stock doc 
                    </td>
                    <td rowspan="3" style="display: none;">
                        <input type="text" id="reminders" class="datepickers" name="reminders" />
                        <br /> 
                        Add Daily Reminders
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="options" type="checkbox" value="addsig" name="addsigs" checked />
                        Add Signatures
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="options" type="checkbox" value="addformfield" name="formfields" disabled />
                        Add Form Fields
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="options" type="checkbox" value="addcondfield" name="conditionalfields" disabled />
                        Add Conditional Fields
                    </td>
                    <td rowspan="3" style="display: none;">
                        <input type="text" id="expiration" class="datepickers" name="expiration" disabled />
                        <br />
                        Add Expiration
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="options" type="checkbox" name="collabfields" value="addcollfield" disabled />
                        Add Collaborative Fields
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="options" type="checkbox" name="enablepaper" value="enablepaper" disabled />
                        Enable Signing on Paper
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input class="options" type="checkbox" name="signerattachment" value="reqattachment" disabled />
                        Request a Signer to Add an Attachment
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input class="options" type="checkbox" name="markup" value="enablemarkup" disabled />
                        Enable Signers to Mark Up the Documents
                    </td>
                </tr>
            </table>
            
            <br />
            <br />
            
            <table class="submit">
                <tr>
                    <td><input class="docusignbutton orange" type="submit" onclick='xajax_sendDocumentSubmit(xajax.getFormValues("sendDocumentSubmit"))' block='#sendDocumentSubmit' value="Send Now" name="SendNow"/></td>
                    <td><input class="docusignbutton brown" type="submit" onclick='xajax_sendDocumentSubmit(xajax.getFormValues("sendDocumentSubmit"))' block='#sendDocumentSubmit' value="Edit Before Sending" name="EditFirst"/></td>
                </tr>
            </table>
        </form>
                
        <?
        echo ob_get_clean();
    }
    
    public function sendTemplate($loadTemplates)
    {
        $this->regClientEventsForSendTemplates();
        
        ob_start();
        ?>
        
                <form onsubmit='return false;' id="sendTemplateSubmit" enctype="multipart/form_data" method="post">

                    <!-- Choose the Template before showing other form fields (autopopulate Roles) -->
                    <?
                            if(!$displayForms){
                    ?>

                            <div style="margin:0pt auto; width:400px; text-align:center;">
                                        <br />
                                        <div>
                                            Select a Template
                                            <br />
                                            <select id="TemplateTable" name="TemplateTable" >
                                                    <?php $loadTemplates; ?>
                                            </select>
                                        </div>
                                        <br />
                                        <div class="submitForm">
                                            <input class="docusignbutton orange" type="submit" onclick="xajax_sendTemplateSubmit(xajax.getFormValues('sendTemplateSubmit'))" block="#sendTemplateSubmit"  value="Continue with Template"/>
                                        </div>
                                    </div>

                    <? } else { ?>

                            <br />

                            <div>
                                    Template Chosen: 
                                    <strong><? echo $templateDetails->RequestTemplateResult->EnvelopeTemplateDefinition->Name; ?></strong>
                            </div>

                            <br />

                                <div>
                                <input id="subject" name="subject" type="text" value="Test Subject" placeholder="<enter the subject>" autocomplete="off"/>
                    <br />
                    <br />
                                <textarea id="emailblurb" cols="20" name="emailBlurb" placeholder="<enter the e-mail blurb>" rows="4" class="email">Test Body</textarea>
                    <br />
                    <br />

                                </div>
                                <br />
                                <div>
                                        <table width="100%" id="RecipientTable" name="RecipientTable" >
                            <tr class="recipientListHeader">
                                <th>
                                    Role Name
                                </th>
                                <th>
                                    Recipient Name
                                </th>
                                <th>
                                    E-mail
                                </th>
                                <th>
                                    Security
                                </th>
                                <th>
                                    Send E-mail Invite
                                </th>
                            </tr>

                            <? foreach($templateDetails->RequestTemplateResult->Envelope->Recipients->Recipient as $recipient): ?>
                                    <tr id="Role1">
                                            <td>
                                                    <input type="text" name="RoleName[1]" id="txtRow1" value="<? echo $recipient->RoleName ?>">
                                            </td>
                                            <td>
                                                    <input type="text" name="Name[1]" id="txtRow1">
                                            </td>
                                            <td>
                                                    <input type="email" name="RoleEmail[1]" id="txtRow1">
                                            </td>
                                            <td>
                                                    <select id="RoleSecurity1" name="RoleSecurity[1]" onchange="EnableDisableInput(1,'RoleSecurity');">
                                                            <option value="None">None</option>
                                                            <option value="IDCheck" <? echo (!empty($recipient->requireIDLookup) ? '"selected"' : ''); ?>>ID Check</option>
                                                            <option value="AccessCode" <? echo (!empty($recipient->AccessCode) ? '"selected"' : ''); ?>>Access Code:</option>
                                                            <option value="PhoneAuthentication" <? echo (!empty($recipient->PhoneAuthentication) ? '"selected"' : ''); ?>>Phone Authentication</option>
                                                    </select>
                                                    <input type="text" name="RoleSecuritySetting[1]" id="RoleSecuritySetting1" value="<? echo (!empty($recipient->AccessCode) ? $recipient->AccessCode : ''); ?>" style="display:none;">
                                            </td>
                                            <td>
                                                    <ul class="switcher">
                                                            <li class="active">
                                                                    <a href="#" title="On">ON</a>
                                                            </li>
                                                            <li>
                                                                    <a href="#" title="OFF">OFF</a>
                                                            </li>
                                                            <input title="RoleInviteToggle1" id="RoleInviteToggle1" name="RoleInviteToggle[1]" type="checkbox" style="display: none; ">
                                                    </ul>
                                            </td>
                                    </tr>
                                    <? endforeach; ?>
                                        </table>
                                </div>
                                <div>
                                    <br />
                                <table width="100%">
                                    <tr>
                                        <td class="fourcolumn">
                                            <input type="text" id="reminders" name="reminders" class="datepickers" />
                                            <br />
                                            Add Daily Reminders
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="fourcolumn">
                                            <input type="text" id="expiration" name="expiration" class="datepickers" />
                                            <br />
                                            Add Expiration
                                        </td>
                                    </tr>
                                </table>
                                </div>

                                <!-- Submitting the Envelope Data -->
                                <input type="hidden" name="TemplateID" value="<? echo $templateDetails->RequestTemplateResult->EnvelopeTemplateDefinition->TemplateID; ?>" />
                                <input type="hidden" name="createSampleEnvelope" value="1" />

                    <table class="submit">
                        <tr>
                            <td>
                                <input class="docusignbutton orange" type="submit" onclick="xajax_sendTemplateSubmit(xajax.getFormValues('sendTemplateSubmit'))" block="#sendTemplateSubmit" value="Send Now" name="SendNow"/>
                            </td>
                            <td>
                                <input class="docusignbutton brown" type="submit" onclick="xajax_sendTemplateSubmit(xajax.getFormValues('sendTemplateSubmit'))" block="#sendTemplateSubmit" value="Edit Before Sending" name="EditFirst"/>
                            </td>
                        </tr>
                    </table>

                <? 
                            } // End of if...else regarding $displayForms
                ?>

                </form>
        <?
        echo ob_get_clean();
    }
    
    public function showMessage($message, $title='Alert')
    {
        // Access to the Template controller object
        global $template;
        $template->pushReadyFunc("alert('$message', '$title');");
    }

    public function regResourcesForStatusAndDocs()
    {
        // Access to the Template controller object
        global $template;
        
        // Loaded jQuery colorbox.js file in the header
        $template->pushHeader('<link rel="stylesheet" href="http://hashim.sporefanz.com/includes/css/docusign/default.css" />');
        $template->pushHeader('<link rel="stylesheet" href="http://hashim.sporefanz.com/includes/css/docusign/GetStatusAndDocs.css" />');
        $template->pushHeader('<script src="http://hashim.sporefanz.com/includes/js/docusign/Utils.js"></script>');
    }
    
    public function regClientEventsForGetStatusAndDocs()
    {
        // Include js, css resources in page.
        $this->regResourcesForStatusAndDocs();
        
        // Access to the Template controller object
        global $template;
        
        $template->pushReadyFunc('getTabSelected(Tab.GET_STATUS_AND_DOCS);');
        
        // jQuery event bindings and plugin calls here
        $template->pushHeader('
            <script type="text/javascript">
                // Invert rows when clicking (not implemented, simple enough to view without deep-clicking)
                function invert(ident) {
                    var state = document.getElementById(ident).style.display;
                    if (state == "block") {
                        document.getElementById(ident).style.display = "none";
                    } else {
                        document.getElementById(ident).style.display = "block";
                    }
                }
            </script>
        ');
    }
    
    public function getStatusAndDocs($statuses)
    {
        $this->regClientEventsForGetStatusAndDocs();
        
        //vx($statuses);
        
        ob_start();
        
        if (isset($statuses)) 
        {?>
            <ul class=""> 
                <?foreach ($statuses->EnvelopeStatuses->EnvelopeStatus as $status) {?>
                    <li>
                        <span>
                            <u><?= $status->Subject ?></u> 
                            [<?= $status->Status ?>] - 
                            <?= $status->EnvelopeID; ?> 
                            <a href="<?= $this->getPathX()?>admin/getEnvelopeStatus/q_?envelopeid=<?= $status->EnvelopeID; ?>" target="_blank" title="Click to see a RequestStatus SOAP return for this Envelope">View RequestStatus</a>
                            &nbsp;&nbsp;<a href="<?= $this->getPathX()?>admin/getPdf/q_?envelopeid=<?= $status->EnvelopeID; ?>" target="_blank" title="Click to download PDF for this Envelope">Download PDF</a>
                        </span>
                        <ul>
                            <!-- Recipients -->
                            <li>
                                <span>Recipients (<?= count($status->RecipientStatuses->RecipientStatus); ?>)</span>
                                <ul id="<?= $status->EnvelopeID; ?>">
                                    <? foreach($status->RecipientStatuses->RecipientStatus as $rcpStatus){ ?>
                                        <li>
                                            <!-- Recipient Name and Start Signing -->
                                            <? echo $rcpStatus->UserName; ?> 
                                            <? /* <a href="<?= $this->getPathX()?>admin/embedDocuSignSubmit/q_?from_gsad=1&envelopeID=<?= $status->EnvelopeID; ?>&clientID=<?= $rcpStatus->ClientUserId ?>">Start Signing</a> */ ?>
                                        </li>
                                    <? } ?>
                                </ul>
                            </li>

                            <!-- Documents -->
                            <li>
                                <span>Documents (<?= count($status->DocumentStatuses->DocumentStatus); ?>)</span>
                                <ul>
                                    <? foreach($status->DocumentStatuses->DocumentStatus as $docStatus){ ?>
                                        <li><?= $docStatus->Name; ?></li>
                                    <? } ?>
                                </ul>
                            </li>

                        </ul>
                    </li>
                    <? };
            ?> </ul> 
        <?}
        else 
        {
            // No Envelopes created yet
            echo '<tr><td><div class="sampleMessage">';
            echo '	No envelopes created, yet. Use the tabs to create an Envelope.';
            echo '</div></td></tr>';
        }

        echo ob_get_clean();
    }
       
    public function embedDocuSign($_showTwoSignerMessage=false, $_showTransitionMessage=false)
    {
        $this->regResources();
        
        global $template;
        $template->pushReadyFunc('getTabSelected(Tab.EMBED_DOCUSIGN);');
        
        ob_start();
        ?>
        
        <form method="post" onsubmit='return false;' action="<?= $this->getPathX() ?>admin/embedDocuSignSubmit" id="embedDocuSignSubmit">
            <table width="100%">
                <tr>
                    <td class="rightalign">
                        <input class="docusignbutton orange" name="OneSigner" id="OneSigner" type="submit" onclick="xajax_embedDocuSignSubmit(xajax.getFormValues('embedDocuSignSubmit'))" block="#embedDocuSignSubmit" value="Create an Envelope with 1 Signer" />
                    </td>
                    <td class="leftalign">
                        <input class="docusignbutton brown" name="TwoSigners" id="TwoSigners" type="submit" onclick="xajax_embedDocuSignSubmit(xajax.getFormValues('embedDocuSignSubmit'))" block="#embedDocuSignSubmit" value="Create an Envelope with 2 Signers" />
                    </td>
                </tr>
            </table>
            
            <div id="embedAction"></div>
        </form>
        
        <?
        echo ob_get_clean();
    }
        
    public function embedSending()
    {
        $this->regResources();
        
        global $template;
        $template->pushReadyFunc('getTabSelected(Tab.EMBED_DOCUSIGN);');
        
        echo '<iframe src="' . $_SESSION['embedToken'] . '" width="100%" height="720px"></iframe>';
    }
    
    public function connect($posts)
    {
        global $template;
        
        $template->pushHeader(
        "
            <style>
                .comment {
                  font: Verdana;
                  font-weight: bold;
                }
                
                table.contentTable {
                  font: 11px/24px Verdana;
                  border-collapse: collapse;
                  width:100%;
                }
                
                table.contentTable thead {
                  padding: 0 0.5em;
                  text-align: left;
                  border-top: 1px solid #0087dd;
                  border-bottom: 1px solid #0087dd;
                  background: #f0f0f0;
                }
                
                table.contentTable td {
                  border-bottom: 1px solid #cacaca;
                  padding: 0 0.5em;
                }
                
                table.contentTable td:first-child {
                    text-align: left;
                }
                
                table.contentTable td+td {
                  border-left: 1px solid #cacaca;
                  text-align: center;
                }

                .header {
                  margin-bottom: 5px;
                }
                .footer {
                  margin-top: 5px;
                }
                .comment {
                  font: Verdana;
                  font-weight: bold;
                }
                table {
                  font: 11px/24px Verdana;
                  border-collapse: collapse;
                }

                thead {
                  padding: 0 0.5em;
                  text-align: left;
                  border-top: 1px solid #0087dd;
                  border-bottom: 1px solid #0087dd;
                  background: #f0f0f0;
                }

                td {
                  border-bottom: 1px solid #cacaca;
                  padding: 0 0.5em;
                }

                td:first-child {
                  text-align: left;
                }

                td+td {
                  border-left: 1px solid #cacaca;
                  text-align: center;
                }
            </style>
`       "        
        );
        
        $this->regResources();
        ob_start();
        ?>
        <div class="header"><span class="comment">Envelops status:</span></div> 
        <table class="contentTable">
            <thead>
                <tr>
                    <td class="headCell">Sender:</td>
                    <td class="headCell">Envelope ID:</td>
                    <td class="headCell">Subject:</td>
                    <td class="headCell">Status:</td>
                    <td class="headCell">Raw XML</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach( $posts as $post) {?>
                <tr>
                    <td class="contentCell"><?php echo $post->EnvelopeStatus->UserName ?></td>
                    <td class="contentCell"><?php echo $post->EnvelopeStatus->EnvelopeID ?></td>
                    <td class="contentCell"><?php echo $post->EnvelopeStatus->Subject ?></td>
                    <td class="contentCell"><?php echo $post->EnvelopeStatus->Status ?></td>
                    <td class="contentCell"><a href="<?php echo ($post->EnvelopeStatus->EnvelopeID . '.xml')?>">Download</a></td>
                </tr><?php } ?>
            </tbody>
        </table>
        <?
        echo ob_get_clean();
    }
        
    public function getEnvelopeStatus($envelopeStatus)
    {
        //px($envelopeStatus["RequestStatusResult"]);
        ?>
        <ul>
            <li>Type: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->Type ?></li>
            <li>Email: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->Email ?></li>
            <li>UserName: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->UserName ?></li>
            <li>Routing Order: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->RoutingOrder ?></li>
            <li>Sent: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->Sent ?></li>
            <li>Delivered: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->Delivered ?></li>
            <li>Signed: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->Signed ?></li>
            <li>Declined: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->Declined ?></li>
            <li>Decline Reason: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->DeclineReason ?></li>
            <li>Status: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->Status ?></li>
            <li>Recipient IP Address: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->RecipientIPAddress ?></li>
            <li>Client User Id: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->ClientUserId ?></li>
            <li>Auto Navigation: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->AutoNavigation ?></li>
            <li>ID Check Information: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->IDCheckInformation ?></li>
            <li>Recipient Authentication Status: <?= $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus[0]->RecipientAuthenticationStatus ?></li>
        </ul>
        <? 
        
        /*
        echo '<pre>';
        echo $envelopeStatus->RequestStatusResult->RecipientStatuses->RecipientStatus;
        print_r($envelopeStatus);
        echo  '</pre>';
        */
    }
    
    public function pop()
    {
        global $template;
        
        $template->pushReadyFunc('framepop();');
        
        $template->pushHeader('
            <script type="text/javascript">
                function framepop() {
                    if (top.frames.length != 0) {
                        //top.location = "getstatusanddocs.php" + window.location.search;
                        top.location = "getStatusAndDocs" + window.location.search;
                        alert(top.location);
                    }
                }
            </script>
        ');
    }
    
    public function pop2()
    {
        global $template;
        
        $template->pushReadyFunc('framepop();');
        
        $template->pushHeader('
            <script type="text/javascript">
                function framepop() {
                    if (top.frames.length != 0) {
                        //top.location = "embeddocusign.php" + window.location.search;
                        top.location = "embedDocuSign" + window.location.search;
                    }
                }
            </script>
        ');
    }
        
    public function error()
    {
        global $template;
        $template->pushHeader('<link rel="stylesheet" href="http://hashim.sporefanz.com/includes/css/docusign/homestyle.css" />');

        if (isset($_SESSION["errorMessage"])) 
        {
            echo '<pre>';
            print_r($_SESSION["errorMessage"]);
            echo  '</pre>';
        }
        else {
            echo "You shouldn't be on this page";
        }
    }
    
    
    
    
    
    // Depricated
    public function sendDocument1($arrResult)
    {
        $this->regClientEventsForSendDocument();
        
        $html = new htmlBlock;
        
        $html
        ->push(tag::form())
            ->id("SendDocumentForm")
            ->enctype("multipart/form_data")
            ->method("post")
        
        
            ->push(tag::input()) 
                ->id("subject") 
                ->name("subject") 
                ->type("text") 
                ->value("Test Subject") 
                ->placeholder("<enter the subject>") 
                ->autocomplete("off")
            ->ascend()
                
            ->push(tag::br())
            ->push(tag::br())
                        
            ->push(tag::textarea()) 
                ->id("emailblurb") 
                ->cols(20) 
                ->name("emailBlurb") 
                ->placeholder("<enter the e-mail blurb>") 
                ->rows("4") 
                ->class('email')
                ->append('Test Body')
            ->ascend()
            
            ->push(tag::br())
            ->push(tag::br())
                
            ->push(tag::table()) 
                ->id('recipientList') 
                ->name('recipientList') 
                ->class("recipientList")
                
                ->push(tag::tr()) 
                    ->class("recipientListHeader")
                    ->push(tag::th())
                        ->append('Recipient')
                    ->ascend()
                    ->push(tag::th())
                        ->append('E-mail')
                    ->ascend()
                    ->push(tag::th())
                        ->append('Security')
                    ->ascend()
                    ->push(tag::th())
                        ->append('Send E-mail Invite')
                    ->ascend()
                ->ascend()
                
                ->push(tag::tr()) 
                    ->id("Recipient1")
                    ->push(tag::td())
                        ->push(tag::input()) 
                        ->type("text") 
                        ->name("RecipientName[1]") 
                        ->id("txtRow1") 
                        ->value("")
                    ->ascend()
                
                    ->push(tag::td())
                        ->push(tag::input()) 
                        ->type("email") 
                        ->name("RecipientEmail[1]") 
                        ->id("txtRow1") 
                        ->value("")
                    ->ascend()
                
                    ->push(tag::td())
                        ->push(tag::select()) 
                            ->id("RecipientSecurity1") 
                            ->name("RecipientSecurity[1]") 
                            ->onchange("EnableDisableInput(1);")
                                ->push(tag::option()) 
                                    ->value("None")
                                    ->append('None')
                                ->ascend()
                                ->push(tag::option()) 
                                    ->value("IDCheck")
                                    ->append('ID Check')
                                ->ascend()
                                ->push(tag::option()) 
                                    ->value("AccessCode")
                                    ->append('Access Code:')
                                ->ascend()
                                ->push(tag::option()) 
                                    ->value("PhoneAuthentication")
                                    ->append('Phone Authentication')
                                ->ascend()
                        ->ascend()
                        ->push(tag::input()) 
                            ->type("text") 
                            ->name("RecipientSecuritySetting[1]") 
                            ->id("RecipientSecuritySetting1") 
                            ->value("12345") 
                            ->style("display: none;")
                        ->ascend()
                    ->ascend()
                
                    ->push(tag::td())
                        ->push(tag::ul()) 
                            ->class("switcher")
                                ->push(tag::li()) 
                                    ->class("active")
                                    ->push(tag::a()) 
                                        ->href("#") 
                                        ->title("On")
                                        ->append('ON')
                                    ->ascend()
                                ->ascend()
                            ->push(tag::li())
                                ->push(tag::a()) 
                                    ->href("#") 
                                    ->title("OFF")
                                    ->append('OFF')
                                ->ascend()
                            ->ascend()
                            ->push(tag::input()) 
                                ->title("RecipientInviteToggle1") 
                                ->id("RecipientInviteToggle1") 
                                ->name("RecipientInviteToggle[1]") 
                                ->class("RecipientInviteToggle") 
                                ->type("checkbox") 
                                ->checked('checked') 
                                ->style("display: none;")
                            ->ascend()
                        ->ascend()
                    ->ascend()
                ->ascend()
            ->ascend()
                
            ->push(tag::input()) 
                ->type("button") 
                ->onclick("addRecipientRowToTable()") 
                ->value("Add Recipient")
            ->ascend()
                
            ->push(tag::br())
            ->push(tag::br())
                
            ->push(tag::div()) 
                ->id("files") 
                ->style("display:none;")
                ->push(tag::p())
                    ->append("Document #1:")
                    ->push(tag::input()) 
                        ->class("upload") 
                        ->id("file1") 
                        ->type("file") 
                        ->name("file1") 
                    ->ascend()
                ->ascend()
                ->push(tag::p())
                    ->append("Document #2:")
                    ->push(tag::input()) 
                        ->class("upload") 
                        ->id("file2") 
                        ->type("file") 
                        ->name("file2")
                    ->ascend()
                ->ascend()
            ->ascend()
                
            ->push(tag::table()) 
                ->class("optionlist")
                ->push(tag::tr())
                    ->push(tag::td())
                        ->push(tag::input()) 
                            ->id("sendoption") 
                            ->class("options") 
                            ->type("checkbox") 
                            ->value("stockdoc") 
                            ->name("stockdoc")  
                            ->onclick("EnableDisableDiv()") 
                            ->checked("checked")
                        ->ascend()
                        ->append("Use a stock doc")
                    ->ascend()
                
                    ->push(tag::tr())
                        ->rowspan("3")
                        ->push(tag::input()) 
                            ->type("text") 
                            ->id("reminders") 
                            ->class("datepickers") 
                            ->name("reminders")
                        ->ascend()
                
                        ->push(tag::br())
                
                        ->append("Add Daily Reminders")
                    ->ascend()
                ->ascend()
                
                ->push(tag::tr())
                    ->push(tag::td())
                        ->push(tag::input()) 
                            ->class("options") 
                            ->type("checkbox") 
                            ->value("addsig") 
                            ->name("addsigs") 
                            ->checked("checked") 
                        ->ascend()
                        ->append("Add Signatures")
                    ->ascend()
                ->ascend()
                
                ->push(tag::tr())
                    ->push(tag::td())
                        ->push(tag::input()) 
                            ->class("options") 
                            ->type("checkbox") 
                            ->value("addformfield") 
                            ->name("formfields")
                        ->ascend()
                        ->append("Add Form Fields")
                    ->ascend()
                ->ascend()
                
                ->push(tag::tr())
                    ->push(tag::td())
                        ->push(tag::input()) 
                            ->class("options") 
                            ->type("checkbox") 
                            ->value("addcondfield") 
                            ->name("conditionalfields")
                        ->ascend()
                        ->append("Add Conditional Fields")
                    ->ascend()
                    ->push(tag::td())
                        ->rowspan("3")
                        ->push(tag::input()) 
                            ->type("text") 
                            ->id("expiration") 
                            ->class("datepickers") 
                            ->name("expiration")
                        ->ascend()
                        ->push(tag::br())
                        ->append("Add Expiration")
                    ->ascend()
                ->ascend()
                
                ->push(tag::tr())
                    ->push(tag::td())
                        ->push(tag::input()) 
                            ->class("options") 
                            ->type("checkbox") 
                            ->name("collabfields") 
                            ->value("addcollfield") 
                        ->ascend()
                        ->append("Add Collaborative Fields")
                    ->ascend()
                ->ascend()
                ->push(tag::tr())
                    ->push(tag::td())
                        ->push(tag::input()) 
                            ->class("options") 
                            ->type("checkbox") 
                            ->name("enablepaper") 
                            ->value("enablepaper") 
                        ->ascend()
                        ->append("Enable Signing on Paper")
                    ->ascend()
                ->ascend()
                ->push(tag::tr())
                    ->push(tag::td())
                        ->colspan("2")
                        ->push(tag::input()) 
                            ->class("options") 
                            ->type("checkbox") 
                            ->name("signerattachment") 
                            ->value("reqattachment") 
                        ->ascend()
                        ->append("Request a Signer to Add an Attachment")
                    ->ascend()
                ->ascend()
                ->push(tag::tr())
                    ->push(tag::td())
                        ->colspan("2")
                        ->push(tag::input()) 
                            ->class("options") 
                            ->type("checkbox") 
                            ->name("markup") 
                            ->value("enablemarkup") 
                        ->ascend()
                        ->append("Enable Signers to Mark Up the Documents")
                    ->ascend()
                ->ascend()
            ->ascend()
                
            ->push(tag::br())
            ->push(tag::br())
                
            ->push(tag::table()) 
                ->class("submit")
                ->push(tag::tr())
                    ->push(tag::td())
                        ->push(tag::input()) 
                        ->class("docusignbutton orange") 
                        ->type("submit") 
                        ->value("Send Now") 
                        ->name("SendNow")
                    ->ascend()
                ->ascend()
                ->push(tag::td())
                    ->push(tag::input()) 
                        ->class("docusignbutton brown") 
                        ->type("submit") 
                        ->value("Edit Before Sending") 
                        ->name("EditFirst")
                    ->ascend()
                ->ascend()
                ->ascend()
            ->ascend()
                
        ->ascend()
            
       ;    
        
        return $html;
            
    }
    
}

?>