<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class DocuSignAjax extends AppAjax
{
    public function loginSubmit()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        if (isset($_POST["submit"])) 
        {
            $_SESSION["UserID"] = $_POST["DevCenterEmail"];
            $_SESSION["Password"] = $_POST["DevCenterPassword"];
            $_SESSION["IntegratorsKey"] = $_POST["DevCenterIKey"];
            
            if ($logic->login())
            {
                $_SESSION["LoggedIn"] = true;
                $logic->redirectToSendDocument();
            }
            else 
            {
                $_SESSION["LoggedIn"] = false;
                $logic->redirectToError();
            }
        }

        // if the "reset" button has been pressed, set all values back to original
        if (isset($_POST["reset"])) {
            $logic->initCreds();
        }
    }
    
    public function sendDocumentSubmit($arrData)
    {
        $xajax = new xajaxResponse;
        $logic = $this->getLogic();
        $envelope = $logic->buildEnvelope($arrData);
        
        // If 'Send Now' button is pressed.
        if (isset($arrData["SendNow"])) 
        {
            $result = $logic->sendDocumentNow($envelope);
            $xajax->doAlertRedirect($result['url'], $result['message'], $result['title']);
            
            return $xajax;
        }
        
        // If 'Edit Before Sending' button is pressed.
        $result = $logic->embedDocumentSending($envelope);
        
        if ($result['status'] == false)
        {
            $xajax->script("alert('{$result['message']}', '{$result['title']}')");
            return $xajax;
        }
        
        $xajax->doAlertRedirect($result['url'], $result['message'], $result['title']);
        return $xajax;
    }
    
    public function sendTemplateSubmit($arrData)
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;
        $xajax->alert('called');
        return $xajax;
        
     
        $displayForms = true;
        if(isset($arrData['createSampleEnvelope']))
        {
            $logic->createSampleEnvelope($arrData);
        } 
        else 
        {
            $api = $logic->getAPI();
            $template = new RequestTemplate();
            $template->TemplateID = $arrData["TemplateTable"];
            $template->IncludeDocumentBytes = false;

            try {
                $templateDetails = $api->RequestTemplate($template);
            } 
            catch (SoapFault $e) 
            {
                $_SESSION["errorMessage"] = array($e, $api->__getLastRequest(), $csParams);
                $logic->redirectToError();
                $xajax->script('location.href = "http://hashimadmin.sporefanz.com/docusign/admin/error"');
            }
        }
        return $xajax;
    }
    
    public function embedDocuSignSubmit($arrData)
    {
        $xajax = new xajaxResponse;

        $logic = $this->getLogic();
        $logic->loginCheck();

        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
            $logic->_oneSigner = isset($arrData["OneSigner"]);
            $logic->createAndSend($logic->_oneSigner);
            
            if(!$logic->_oneSigner) {
                $logic->_showTwoSignerMessage = true;
            }
        } 
        else if ($_SERVER["REQUEST_METHOD"] == "GET") 
        {
            if (isset($arrData["envelopeID"])) 
            {
                // Display a message that we are moving on to Signer Number 2
                // - unless the message is suppressed (by signing from the GetStatusAndDocs page)
                if(isset($arrData['from_gsad'])) {
                    $logic->getToken($logic->getStatus($arrData['envelopeID']),$arrData['clientID']);
                } 
                else 
                {
                    $logic->_showTransitionMessage = true;
                    $logic->getToken($logic->getStatus($arrData['envelopeID']), 2);
                }
            }
            else {
                $_SESSION["embedToken"] = "";
            }
        }

        if($logic->_showTwoSignerMessage) {
            $xajax->script('$("#embedAction").append("<div class=\"sampleMessage\"> Have the first signer fill out the Envelope (only a signature is required for the first signer)</div>");');
        }

        if($logic->_showTransitionMessage) {
            $xajax->script('$("#embedAction").append("<div class=\"sampleMessage\">The first signer has completed the Envelope. Now the second signer will be asked to fill out details in the Envelope.</div>");');
        }
        
        if(isset($_SESSION["embedToken"]) && !empty($_SESSION["embedToken"])) {
            $xajax->script('$("#embedAction").append(\'<iframe class="embediframe" width="100%" height="70%" src="'. $_SESSION["embedToken"] .'" id="hostiframe" name="hostiframe"></iframe>\');');
        }

        return $xajax;
    }
    
}

?>