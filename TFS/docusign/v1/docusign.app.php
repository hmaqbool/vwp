<?php

//ini_set('display_errors', 1);
//error_reporting(E_ALL);


/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class DocuSignApp extends GenericApp
{
    protected  $class_logic = "DocuSignLogic";
    protected  $class_display = "DocuSignDisplay";
    protected  $class_ajax = "DocuSignAjax";
    protected  $class_db = "DocuSignDB";
    protected  $class_css = "DocuSignCSS";
    protected  $class_page = "DocuSignPage";

    protected static $appInstance;

    public static $concern = "DocuSign";
    public static $name = "DocuSign";
    public static $icon = "http://global.imranmedia.com/images/agg-icons/gig/affiliate_sales.png";

    public function __construct($db,$path,$vars=array())
    {
        parent::__construct($db,$path,$vars);
    }
    public static function getObj($db,$path,$arr=array())
    {
        if (!isset(self::$appInstance))
        {
            $c = __CLASS__;
            self::$appInstance = new $c($db,$path,$arr);
        }
        return self::$appInstance;
    }
    public function __varInit($arr=array())
    {
        return parent::__varInit( array_merge(
            array 
            (
                'userId' => 'hmaqbool@folio3.com',
                'password' => 'test123',
                'integratorsKey' => 'TEST-6932f29e-997f-46bb-adb4-1d90973b9a74',
                'accountId' => '',
                'credApiEndpoint' => 'https://demo.docusign.net/api/3.0/credential.asmx',
                'dsApiEndpoint' => 'https://demo.docusign.net/api/3.0/dsapi.asmx',
                'apiEndpoint' => 'https://demo.docusign.net/api/3.0/api.asmx',
                'timeZone' => 'America/Los_Angeles',
            ),$arr) );
    }
    public function getCrons()
    {
        //$this->crons[] = array("frequency"=>86400,"app"=>"Hashim","callback"=>"logicMethod","firstrun"=>0);
        return $this->crons;
    }
}

?>