<?

//echo $_CONF["rootPath"] ."sites/hashim/dev/templates/";

global $_CONF, $_TEMP, $_LANG, $classArr;

$sc = SiteConfig::getObj();
$sc
    ->sitePath("sites/hashim/dev")
    //->dashboardMode()
    ->forceLogin()
    ->setCONF("defaultLogin",Vars::getCONF("rootPath") ."sites/demo/vwpdemo/admin/templates/login.php")
    ->setCONF("sDisplayBlocksPath",$_CONF["rootPath"] ."sites/demo/vwpdemo/admin/templates/")
    ->setCONF("sLogicPath",$_CONF["rootPath"] ."sites/demo/vwpdemo/admin/logic/")
    //->setCONF('sTemplatesPath', $_CONF["rootPath"] ."sites/demo/vwpdemo/admin/templates/")
    ->setCONF('sTemplatesPath', $_CONF["rootPath"] ."sites/hashim/dev/templates/")
    ->setCONF("logoLoc",'http://a.wethrive.com/ext/im/vwpnew/images/logo-5.gif')
    ->setCONF('localUploadDir', "/mnt/stor1-wc2-dfw1/444787/rohan.wethrive.com/web/content/upload/")
    ->setCONF("critYearOffset", 3)
    
    ->devSite()
    
    ->template("blank.php")
    //->template("blank2.php")
    //->template("hello.php")
        
    ->CDN("http://cdn.incubelabs.com/")
    ->s3Bucket("iminc-goneg")
    ->mailDomain("imranmedia.com")
    ->setCONF("AWSMailDomain", 'talat@imranmedia.com')
    ->addClasses(
        array(
                'DashboardAppLogic'=>'rohan/dbextends/v4/dbapplogic.class.php',
                'DashboardAppPage'=>'rohan/dbextends/v4/dbapppage.class.php',
                'DashboardAppDisplay'=>'rohan/dbextends/v4/dbappdisplay.class.php',

               // "ICLBlocks" 	=> "cms/icl.blocks.php",
               // "ICLLayouts"    => "cms/icl.layouts.php",

                "CitrixAPI"    => "gotomeeting/gotomeeting.citrixapi.php",
                //'GoToMeetingDisplay' => 'hashim/gotomeeting/v1/gotomeeting.display.php',

              //  "CMSBlocks" => "rohan/newcms/v9/newcms.blocks.php",
              //  "CMSLayouts" => "rohan/newcms/v9/newcms.layout.php",


            //    'DealsCommonAjax'=>'rohan/icf/dealscommon/v1/dealscommon.ajax.php',
            //    'DealsCommonCSS'=>'rohan/icf/dealscommon/v1/dealscommon.css.php',
            //    'DealsCommonDisplay'=>'rohan/icf/dealscommon/v1/dealscommon.display.php',
           //     'DealsCommonPage'=>'rohan/icf/dealscommon/v1/dealscommon.page.php',

                "ManageAdminForm"=>"rohan/chainableclasses/add.manage.php",
                
                "RowActions"=>"rohan/chainableclasses/layouts.chainable.php",
                "ManageLayout"=>"rohan/chainableclasses/layouts.chainable.php",
                "manageTable"=>"rohan/chainableclasses/layouts.chainable.php",
                "newPseudoTable"=>"rohan/chainableclasses/layouts.chainable.php",

                'CompleteMenu'=>'rohan/classes/dropdownmenu.php',
                //'Api'=>'oauth/vendor/fkooman/php-oauth-client/src/fkooman/OAuth/Client/Api.php',
                'Common'=>'oauth/common.php',
                
                // Docusign lib classes
                //'Credential'=>'hashim/docusign/v1/api/Credential.php',
                //'APIService'=>'hashim/docusign/v1/api/APIService.php',
                //'CredentialWsdl'=>'hashim/docusign/v1/api/Credential.wsdl',
                //'Ping'=>'hashim/docusign/v1/api/Credential.php',
                "formChain" => "forms/form.chainable.php",
                
                // ICF Deals
                'DealsCommonAjax'=>'rohan/icf/dealscommon/v1/dealscommon.ajax.php',
                'DealsCommonCSS'=>'rohan/icf/dealscommon/v1/dealscommon.css.php',
                'DealsCommonDisplay'=>'rohan/icf/dealscommon/v1/dealscommon.display.php',
                'DealsCommonPage'=>'rohan/icf/dealscommon/v1/dealscommon.page.php',
                
            )
    )
    ->addDB("mysql51-031.wc2.dfw1.stabletransit.com","444787_hashim","444787_hashim","F0l10Three")
    
    ->addS3Bucket("iminc-goneg","http://d1iylknx45zy01.cloudfront.net/")
    
    ->addDashGroup('admin','Site Admin Tools')
    ->addDashGroup('utils','Utilities')
    
    
    ->addDashGroup('investors','Investors')
    ->addDashGroup('deals','Deals')
    ->addDashGroup('ent','Entreprenuers')
    ->addDashGroup('sponsors','Sponsors')

    //->addDashGroup('oauth','OAuth')
    //->addDashGroup('docusign','DocuSign')
    //->addDashGroup('gotomeeting','Go To Meeting')
    
    ->regApp("Dashboard",6,"rohan","")->dashGroup("admin")
    ->regApp("Hashim",3,"hashim","hashim")->dashGroup("admin")
    ->regApp("OAuth",3,"hashim","oauth")->dashGroup("admin")
    

    ->regApp("DocuSign",1,"hashim","docusign")->dashGroup("admin")
    ->regApp("DocuSign2",1,"hashim","docusign2")->dashGroup("admin")

    ->regApp("GoToMeeting",4,"hashim","gotomeeting")->dashGroup("admin")

    ->regApp("Session",5,"")->dashGroup("utils")
    ->regApp("SiteTypes",2,"rohan","types")->dashGroup("utils")
    ->regApp("Cron",2,"","cron")->dashGroup("utils")
    ->regApp("Scheduler",2,"rohan","schedule")->dashGroup("utils")
    ->regApp("Mail",1,"rohan","mail")->dashGroup("utils")
    
    
    ->regApp("ICFBase",1,"hashim/icf","icfbase")->dashGroup("utils")
    
    ->regApp("DealsAdminActive",1,"hashim/icf","active")->dashGroup("deals")
    ->regApp("DealsAdminApproved",1,"hashim/icf","approved")->dashGroup("deals")
    ->regApp("DealsAdminCrowdRejected",1,"hashim/icf","crowdrejected")->dashGroup("deals")
    ->regApp("DealsAdminCrowdReview",1,"hashim/icf","crowdreview")->dashGroup("deals")
    ->regApp("DealsAdminEscrow",1,"hashim/icf","escrow")->dashGroup("deals")
    ->regApp("DealsAdminExpired",1,"hashim/icf","expired")->dashGroup("deals")
    ->regApp("DealsAdminFunded",1,"hashim/icf","funded")->dashGroup("deals")
    ->regApp("DealsAdminPending",1,"hashim/icf","pending")->dashGroup("deals")
    ->regApp("DealsAdminRejected",1,"hashim/icf","rejected")->dashGroup("deals")
    ->regApp("DealsAdminSubmitted",1,"hashim/icf","submitted")->dashGroup("deals")
    
    ->regApp("DealsAdminAdd",1,"hashim/icf","add")->dashGroup("deals")
    
    ->regApp("EntAdminManage",1,"rohan/icf","ent")->dashGroup("ent")
    ->regApp("InvAdminManage",1,"rohan/icf","investors")->dashGroup("investors")
    ->regApp("ICFInvite",1,"rohan/icf","invites")->dashGroup("investors")
    ->regApp("SponsorAdminManage",1,"rohan/icf","sponsors")->dashGroup("sponsors")
    
    
;

//include ("docusign/v1/api/APIService.php");

//include("functions.php");
//include("docusign/v1/api/include/session.php");
//include("docusign/v1/api/include/utils.php");
/*
class AppConfigs extends DefaultAppConfigs
{
    public static function NewCMS()
    {
        return array
        (
        	"blocksClass" => "ICFBlocks",
    		"layoutClass" => "ICFLayouts",
        );
    }
}
*/

?>