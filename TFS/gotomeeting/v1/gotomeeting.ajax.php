<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingAjax extends AppAjax
{

	public function xajaxFunc()
    {
    	
        $xajax = new xajaxResponse;
        
        $display = $this->app->getDisplayObj();
        $xajax->assign("someID","innerHTML",$display->someDisplay());
        
        $xajax->assign("myInput1","style.color","red"); 
        
        return $xajax;
    }


}

?>