<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */
error_reporting(1);

class GoToMeetingDisplay extends DashboardAppDisplay
{
	public static $urlVars = array();

    /**
    *----------------  Search form    ----------------- *
    */
    public function searchform()
    {
        $search_form = tag::div(); // form container 
        $search_form
            ->addClass("gtm_formcontainer")
            /* 1st Row Start */
            ->push(tag::div())
                ->addClass("gtm_row")

                ->push(tag::span())
                    ->addClass("gtm_formlabel")
                    ->push(tag::label())
                        ->addClass("gtm_lbl")
                        ->append("Start Date Time")
                    ->ascend()
                ->ascend()

                ->push(tag::span())
                    ->addClass("gtm_formfeild")
                    ->push(shorttag::input())
                        ->addClass("gmt_inputbox")
                        ->setID("start")
                        ->setAttr("type","input")
                        ->setAttr("name","start")
                    ->ascend()
                ->ascend()
            
            ->ascend()
            /* 1st Row End */

            /* 2nd Row Start */
            ->push(tag::div())
                ->addClass("gtm_row")

                ->push(tag::span())
                    ->addClass("gtm_formlabel")
                    ->push(tag::label())
                        ->addClass("gtm_lbl")
                        ->append("End Date Time")
                    ->ascend()
                ->ascend()

                ->push(tag::span())
                    ->addClass("gtm_formfeild")
                    ->push(shorttag::input())
                        ->addClass("gmt_inputbox")
                        ->setID("end")
                        ->setAttr("type","input")
                        ->setAttr("name","end")
                    ->ascend()
                ->ascend()
                
            ->ascend()
            /* 2nd Row End */

            /* 3rd Row Start */
            ->push(tag::div())
                ->addClass("gtm_row")

                ->push(tag::span())
                    ->addClass("gtm_btnContainer")
                    ->push(shorttag::input())
                        ->addClass("gmt_btn")
                        ->addClass("gtm_btn_reset")
                        ->setAttr("type","button")
                        ->setAttr("onclick","location.reload();")
                        ->setAttr("Value","Reset")
                    ->ascend()

                    ->push(shorttag::input())
                        ->addClass("gmt_btn")
                        ->addClass("gtm_btn_submit")
                        ->setAttr("type","button")
                        ->setAttr("onclick","CreateMeeting();")
                        ->setAttr("Value","Search")
                    ->ascend()
                ->ascend()
            ->ascend()
            /* 3rd Row End */
        ->ascend()
        ;
            return $search_form;
    }
    /*----------------  Search form    ----------------- */


    /**
    *----------------  Create form    ----------------- *
    */
    public function createform($form)
    {
        $immediate="";
        $scheduled="";
        $recurring="";



        if($form){
            $btn = 'Update';

            if($form[0]["meetingType"] == 'immediate')
                $immediate="selected";

            if($form[0]["meetingType"] == 'scheduled')
                $scheduled="selected";

            if($form[0]["meetingType"] == 'recurring')
                $recurring="selected";
        }
        else
            $btn = 'Create';

        if( isset($form[0]['endTime']) )
        {
            $date = new DateTime( $form[0]['endTime'] );
            $form[0]['endTime'] = $date->format("m/d/Y H:i");
        }

        if( isset($form[0]["startTime"]) )
        {
            $date = new DateTime( $form[0]["startTime"] );
            $form[0]["startTime"] = $date->format("m/d/Y H:i");
        }

        $frm = tag::div(); /* form container */
        $frm  
            ->addClass("gtm_formcontainer")

            ->push(tag::div())/* row */
                ->addClass("gtm_row")
                ->push(shorttag::input())
                    ->setAttr("type", "hidden")
                    ->setAttr("value", $form[0]["meetingId"])
                    ->setAttr("name", "meetingId")
                ->ascend()

                ->push(tag::span())
                    ->addClass("gtm_formlabel")
                    ->push(tag::label())
                        ->addClass("gtm_lbl")
                        ->append("Subject")
                    ->ascend()
                ->ascend()

                ->push(tag::span())
                    ->addClass("gtm_formfeild")
                    ->push(shorttag::input())
                        ->addClass("gmt_inputbox")
                        ->setID("subject")
                        ->setAttr("type", "input")
                        ->setAttr("value", $form[0]["subject"])
                        ->setAttr("name", "subject")
                    ->ascend()
                ->ascend()
                
            ->ascend()/* row */

            ->push(tag::div())/* row */
                ->addClass("gtm_row")

                ->push(tag::span())
                    ->addClass("gtm_formlabel")
                    ->push(tag::label())
                        ->addClass("gtm_lbl")
                        ->append("Start Date Time")
                    ->ascend()
                ->ascend()

                ->push(tag::span())
                    ->addClass("gtm_formfeild")
                    ->push(shorttag::input())
                        ->addClass("gmt_inputbox")
                        ->setID("start")
                        ->setAttr("type","input")
                        ->setAttr("value", $form[0]["startTime"])
                        ->setAttr("name","start")
                    ->ascend()
                ->ascend()
                
            ->ascend()/* row */

            ->push(tag::div())/* row */
                ->addClass("gtm_row")

                ->push(tag::span())
                    ->addClass("gtm_formlabel")
                    ->push(tag::label())
                        ->addClass("gtm_lbl")
                        ->append("End Date Time")
                    ->ascend()
                ->ascend()

                ->push(tag::span())
                    ->addClass("gtm_formfeild")
                    ->push(shorttag::input())
                        ->addClass("gmt_inputbox")
                        ->setID("end")
                        ->setAttr("type","input")
                        ->setAttr("value", $form[0]["endTime"])
                        ->setAttr("name","end")
                    ->ascend()
                ->ascend()
                
            ->ascend()/* row */

            ->push(tag::div())/* row */
                ->addClass("gtm_row")

                ->push(tag::span())
                    ->addClass("gtm_formlabel")
                    ->push(tag::label())
                        ->addClass("gtm_lbl")
                        ->append("Conference Type")
                    ->ascend()
                ->ascend()

                ->push(tag::span())
                    ->addClass("gtm_formfeild")
                    ->push(tag::select())
                        ->addClass("gmt_inputbox")
                        ->setID("conference")
                        ->setAttr("name","conference")

                        ->push(shorttag::option())
                            ->append('Select')
                        ->ascend()

                        ->push(shorttag::option())
                            ->append('PSTN')
                        ->ascend()

                        ->push(shorttag::option())
                            ->append('Free')
                        ->ascend()

                        ->push(shorttag::option())
                            ->append('Hybrid')
                        ->ascend()

                        ->push(shorttag::option())
                            ->append('Private')
                        ->ascend()

                        ->push(shorttag::option())
                            ->append('VoIP')
                        ->ascend()


                    ->ascend()
                ->ascend()
                
            ->ascend()/* row */

            ->push(tag::div())/* row */
                ->addClass("gtm_row")

                ->push(tag::span())
                    ->addClass("gtm_formlabel")
                    ->push(tag::label())
                        ->addClass("gtm_lbl")
                        ->append("Meeting Type")
                    ->ascend()
                ->ascend()

                ->push(tag::span())
                    ->addClass("gtm_formfeild")
                    ->push(tag::select())
                        ->addClass("gmt_inputbox")
                        ->setAttr("name","meeting")
                        ->setID("meetingtype")

                        ->push(shorttag::option())
                            ->append('Select')
                        ->ascend()

                        ->push(shorttag::option())
                            ->append('Immediate')
                            ->setAttr($immediate, $immediate)
                        ->ascend()

                        ->push(shorttag::option())
                            ->setAttr($scheduled, $scheduled)
                            ->append('Scheduled')
                        ->ascend()

                        ->push(shorttag::option())
                            ->setAttr($recurring, $recurring)
                            ->append('Recurring')
                        ->ascend()

                    ->ascend()
                ->ascend()
                
            ->ascend()/* row */

            ->push(tag::div())/* row */
                ->addClass("gtm_row")

                ->push(tag::span())
                    ->addClass("gtm_btnContainer")
                    ->push(shorttag::input())
                        ->addClass("gmt_btn")
                        ->addClass("gtm_btn_reset")
                        ->setAttr("type","button")
                        ->setAttr("onclick","location.reload();")
                        ->setAttr("Value","Reset")
                    ->ascend()

                    ->push(shorttag::input())
                        ->addClass("gmt_btn")
                        ->addClass("gtm_btn_submit")
                        ->setAttr("type","button")
                        ->setAttr("onclick","CreateMeeting();")
                        ->setAttr("Value",$btn)
                    ->ascend()
                ->ascend()
            ->ascend()/* row */

        ->ascend()/* form container */
        ;

        return $frm;
    }
    /*----------------  Create form    ----------------- */


    /**
    *----------------  Search form Script   ----------------- *
    */
    public function searchformScript()
    {
        global $template;
        $template
            ->pushReadyFunc("
                $('#start').datetimepicker();
                $('#end').datetimepicker();
            ");

        $template->pushHeader("
            <script type=\"text/javascript\">
                function CreateMeeting(){

                    var error = 0;
                    var msg = '';
                    var dd;
                    var date;
                    var index = 1;
                    var now = new Date();

                    if(now.getDate() < 10)
                        dd = '0'+now.getDate();
                    else
                        dd = now.getDate();

                    date = now.getFullYear()+''+(now.getMonth()+1) + '' + dd + now.getHours() + '' + (now.getMinutes() - 5);
                    date = parseInt(date);

                    /* timestamp function for start */
                    start = $('#start').val();
                    var datetime = start.split(' ');
                    var strt_date = datetime[0];
                    var strt_time = datetime[1];
                    var start_date = strt_date.split('/');
                    var start_time = strt_time.split(':');
                    var start_timestamp = start_date[2]+''+start_date[0]+''+start_date[1]+''+start_time[0]+''+start_time[1];
                    start = parseInt(start_timestamp);
                    /* timestamp function for start */

                    /* timestamp function for end */
                    end = $('#end').val();
                    datetime = end.split(' ');
                    var ed_date = datetime[0];
                    var ed_time = datetime[1];
                    var end_date = ed_date.split('/');
                    var end_time = ed_time.split(':');
                    var end_timestamp = end_date[2]+''+end_date[0]+''+end_date[1]+''+end_time[0]+''+end_time[1];
                    end = parseInt(end_timestamp);
                    /* timestamp function for end */


                    if($('#start').val() == ''){
                        error = 1;
                        msg += index+'. Start Date Time can\'t be empty \\n';
                        index++;
                    }

                    if($('#end').val() == ''){
                        error = 1;
                        msg += index+'. End Date Time can\'t be empty \\n';
                        index++;
                    }

                    if(date > start){
                        error = 1;
                        msg += index+'. Start time should be greater than current time \\n';
                        index++;
                    }

                    if(start >= end){
                        error = 1;
                        msg += index+'. End time should be greater than start time \\n';
                        index++;
                    }

                    if(error != 1)
                        $('form#searchMeetings').submit();
                    else
                        alert(msg);
                }
            </script>
        ");  
    }
    /*----------------  Search form Script    ----------------- */
    

    /**
    *----------------  Create form Script   ----------------- *
    */
    public function createformScript()
    {
        global $template;
        $template
            ->pushReadyFunc("
                $('#start').datetimepicker();
                $('#end').datetimepicker();
            ");

        $template->pushHeader("
            <script type=\"text/javascript\">
                function CreateMeeting(){

                        var error = 0;
                        var msg = '';
                        var dd;
                        var date;
                        var index = 1;
                        var now = new Date();

                        if(now.getDate() < 10)
                            dd = '0'+now.getDate();
                        else
                            dd = now.getDate();

                        date = now.getFullYear()+''+(now.getMonth()+1) + '' + dd + now.getHours() + '' + (now.getMinutes() - 5);
                        date = parseInt(date);

                        /* timestamp function for start */
                        start = $('#start').val();
                        var datetime = start.split(' ');
                        var strt_date = datetime[0];
                        var strt_time = datetime[1];
                        var start_date = strt_date.split('/');
                        var start_time = strt_time.split(':');
                        var start_timestamp = start_date[2]+''+start_date[0]+''+start_date[1]+''+start_time[0]+''+start_time[1];
                        start = parseInt(start_timestamp);
                        /* timestamp function for start */

                        /* timestamp function for end */
                        end = $('#end').val();
                        datetime = end.split(' ');
                        var ed_date = datetime[0];
                        var ed_time = datetime[1];
                        var end_date = ed_date.split('/');
                        var end_time = ed_time.split(':');
                        var end_timestamp = end_date[2]+''+end_date[0]+''+end_date[1]+''+end_time[0]+''+end_time[1];
                        end = parseInt(end_timestamp);
                        /* timestamp function for end */


                        if($('#subject').val() == ''){
                            error = 1;
                            msg += index+'. Subject can\'t be empty \\n';
                            index++;
                        }

                        if($('#start').val() == ''){
                            error = 1;
                            msg += index+'. Start Date Time can\'t be empty \\n';
                            index++;
                        }

                        if($('#end').val() == ''){
                            error = 1;
                            msg += index+'. End Date Time can\'t be empty \\n';
                            index++;
                        }

                        if($('#conference').val() == 'Select'){
                            error = 1;
                            msg += index+'. Conference Type can\'t be empty \\n';
                            index++;
                        }

                        if($('#meetingtype').val() == 'Select'){
                            error = 1;
                            msg += index+'. Meeting Type can\'t be empty \\n';
                            index++;
                        }

                        if(start >= end){
                            error = 1;
                            msg += index+'. End time should be greater than start time \\n';
                            index++;
                        }

                        if(date > start){
                            error = 1;
                            msg += index+'. Start time should be greater than current time \\n';
                            index++;
                        }

                        if(error != 1)
                            $('form#CreateMeeting').submit();
                        else
                            alert(msg);
                    }
            </script>
        ");
    }
    /*----------------  Create form Script    ----------------- */
    

    /*----------------  Main Page View    ----------------- */
    public function mainview($data,$page,$pages)
    {
        $html = tag::div();
        $html
            ->setID("page_container")

            ->push(tag::p())
                ->setAttr("style","margin: 0 auto; color: green")
                ->append($data['message'].'<br />')
            ->ascend()

            ->push(tag::div())
                ->setAttr("style","margin-top: 200px;")
                ->push(tag::h2())
                    ->setAttr("style","margin: 0 auto; width: 500px;")
                    ->append('Click on navigation in order to perform Actions.')
                ->ascend()
            ->ascend()
        ->ascend()
        ;
        return $html;
    }
    /*----------------  Main Page View    ----------------- */


    /**
    *----------------  View Meeting Form Page Display    ----------------- *
    */
    public function FutureMeetingsFormview($data, $msg, $page, $pages)
    {
        $frm = $this->searchform();
        $scripts = $this->searchformScript();


        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($scripts)

            ->push(tag::div())
                ->addClass("gtm_data_container")
                ->push(tag::h1())
                    ->addClass("gmt_subtitle")
                    ->append("Search Meetings")
                ->ascend()
                ->push(tag::form()) /* form */
                    ->setAttr("method","POST")
                    ->setAttr("action","http://hashimadmin.sporefanz.com/gotomeeting/admin/futureMeetings")
                    ->setID("searchMeetings")
                    ->append($frm)
                ->ascend() /* form */
            ->ascend()
        ->ascend()
        ;
        return $html;
    }
    /*---------------- View Meeting Form Page Display    ----------------- */


    /**
    *----------------  View Meeting List Page Display    ----------------- *
    */
    public function FutureMeetingsview($data, $msg, $page, $pages)
    {   
        if($data == NULL)
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->setAttr("style","color: black")
                ->append($msg)
            ->ascend();

            $meetingList = tag::div();
        }
        else
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->append($msg)
            ->ascend();

            $meetingList = tag::div();
            $meetingList
                ->addClass('gtm_row_headerContainer')
                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->setAttr("style", "width: 250px;")
                    ->append('Subject')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Max Participants')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Type')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Start')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('End')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->setAttr("style", "width: 210px;")
                    ->append('Conference Info')
                ->ascend()
            ->ascend()
            ;

            $counter = 2;
            foreach ($data as $key => $info) {
                if($counter%2 == 0)
                    $class = "gtm_row_white";
                else
                    $class = "gtm_row_normal";

                $meetingList
                    ->push(tag::div())
                        ->addClass('gtm_row_headerContainer')
                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 250px;")
                            ->append($info['subject'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['participants'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['type'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['start'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['end'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 210px;")
                            ->append($info['conference'])
                        ->ascend()
                    ->ascend()
                ;
                $counter++;
            }
        }


        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($msg_box)
            ->append($meetingList)
        ->ascend()
        ;

        return $html;
    }
    /*---------------- View Meeting List Page Display    ----------------- */


    /**
    *----------------  Create Meeting List Page Display    ----------------- *
    */
    public function createdview($data, $msg, $page, $pages)
    {

        $html = tag::div();
        $html
            ->setID("page_container")
            //->append($scripts)
            //->append($header)

            ->push(tag::div())
                ->setID("gtm_msg")
                ->append($msg)
            ->ascend()


            ->push(tag::div())
                ->addClass("gtm_data_container")
                
                ->push(tag::h1())
                    ->addClass("gmt_subtitle")
                    ->append("Meeting Details")
                ->ascend()

                ->push(tag::p())

                    ->push(tag::span())
                        ->setAttr("style","width: 50px; color: green;")
                        ->append('Join Url : ')
                    ->ascend()
                    ->append($data[0]["joinURL"]."<br />")

                    ->push(tag::span())
                        ->setAttr("style","width: 50px; color: green;")
                        ->append('Meeting Id : ')
                    ->ascend()
                    ->append($data[0]["uniqueMeetingId"]."<br />")

                    ->push(tag::span())
                        ->setAttr("style","width: 50px; color: green;")
                        ->append('Max Participants : ')
                    ->ascend()
                    ->append($data[0]["maxParticipants"]."<br />")

                    ->push(tag::span())
                        ->setAttr("style","width: 50px; color: green;")
                        ->append('Conference Info : ')
                    ->ascend()
                    ->append($data[0]["conferenceCallInfo"]."<br />")
                
                ->ascend()

            ->ascend()

            ->push(tag::p())
                ->append('To create more meetings ')
                ->push(tag::a())
                    ->setAttr("style","text-decoration: none; color: green;")
                    ->setAttr("href","http://hashimadmin.sporefanz.com/gotomeeting/admin/createMeeting/")
                    ->append('click here')
                ->ascend()
            ->ascend()
            ;

        return $html;
    }
    /*----------------  Create Meeting List Page Display    ----------------- */


    /**
    *----------------  Create Meeting Form Page Display    ----------------- *
    */    
    public function createview($data,$page,$pages)
    {	
        $scripts = $this->createformScript();
        $form = $this->createform();

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($scripts)
            //->append($header)

            ->push(tag::div())
                ->addClass("gtm_data_container")
                ->push(tag::h1())
                    ->addClass("gmt_subtitle")
                    ->append("Create Meeting")
                ->ascend()

                ->push(tag::div()) /* form container */
                    ->addClass("gtm_formcontainer")

                    ->push(tag::form()) /* form */
                        ->setAttr("method","POST")
                        ->setAttr("action","http://hashimadmin.sporefanz.com/gotomeeting/admin/created")
                        ->setID("CreateMeeting")
                        ->append($form)
                     ->ascend() /* form */
                ->ascend()/* form container */

            ->ascend()

        ->ascend()
        ;
        return $html;
    }
    /* ---------------  Create Meeting Form Page Display    ---------------- */


    /**
    *----------------  Delete Meeting Form Page Display    ----------------- *
    */
    public function deleteFormview($data, $msg, $page, $pages)
    {
        $frm = $this->searchform();
        $scripts = $this->searchformScript();

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($scripts)

            ->push(tag::div())
                ->addClass("gtm_data_container")
                ->push(tag::h1())
                    ->addClass("gmt_subtitle")
                    ->append("Search Meetings")
                ->ascend()

                ->push(tag::form()) /* form */
                    ->setAttr("method","POST")
                    ->setAttr("action","http://hashimadmin.sporefanz.com/gotomeeting/admin/delete")
                    ->setID("searchMeetings")
                    ->append($frm)
                ->ascend() /* form */

            ->ascend()
        ->ascend()
        ;
        return $html;
    }
    /* ---------------  Delete Meeting Form Page Display    ---------------- */


    /**
    *----------------  Delete Meeting List Page Display    -----------------*
    */
    public function deleteMeetingsview($data, $msg, $form, $page, $pages)
    {   
        $scripts = tag::div();
        $scripts
            ->push(tag::script())
                ->append("
                    function DelMeeting(mid){
                        $('#meetingId').val(mid);
                        $('form#DeleteMeeting').submit();
                    }                    
                ")
                ->ascend()
            ->ascend()
        ;


        if($data == NULL)
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->setAttr("style","color: black")
                ->append($msg)
            ->ascend();

            $meetingList = tag::div();
        }
        else
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->append($msg)
            ->ascend();

            $meetingList = tag::div();
            $meetingList
                ->addClass('gtm_row_headerContainer')
                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->setAttr("style", "width: 250px;")
                    ->append('Subject')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Max Participants')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Type')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Start')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('End')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->setAttr("style", "width: 210px;")
                    ->append('Conference Info')
                ->ascend()
            ->ascend()
            ;

            $counter = 2;
            foreach ($data as $key => $info) {
                if($counter%2 == 0)
                    $class = "gtm_row_white";
                else
                    $class = "gtm_row_normal";

                $meetingList
                    ->push(tag::div())
                        ->addClass('gtm_row_headerContainer')
                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 250px;")
                            ->append($info['subject'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->push(tag::a())
                                ->setAttr("href", "#")
                                ->setAttr("onclick", "DelMeeting(".$info["meetingid"].")")
                                ->append('Delete')
                            ->ascend()
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['type'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['start'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['end'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 210px;")
                            ->append($info['conference'])
                        ->ascend()
                    ->ascend()
                ;
                $counter++;
            }
        }

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($scripts)

            ->push(tag::form()) /* form */
                ->setAttr("method","POST")
                ->setAttr("action","http://hashimadmin.sporefanz.com/gotomeeting/admin/delete")
                ->setID("DeleteMeeting")

                ->push(shorttag::input())
                    ->setAttr("type","hidden")
                    ->setAttr("name","start")
                    ->setAttr("value",$form['start'])
                ->ascend()

                ->push(shorttag::input())
                    ->setAttr("type","hidden")
                    ->setAttr("name","end")
                    ->setAttr("value",$form['end'])
                ->ascend()

                ->push(shorttag::input())
                    ->setAttr("type","hidden")
                    ->setAttr("name","meetingId")
                    ->setID("meetingId")
                ->ascend()

            ->ascend()

            ->append($msg_box)
            ->append($meetingList)
        ->ascend()
        ;

        return $html;
    }
    /* ----------------  Delete Meeting List Page Display    ----------------- */


    /**
    *----------------  Start Meeting Form Page Display    -----------------*
    */
    public function startFormview($data, $msg, $page, $pages)
    {
        $frm = $this->searchform();
        $scripts = $this->searchformScript();

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($scripts)

            ->push(tag::div())
                ->addClass("gtm_data_container")
                ->push(tag::h1())
                    ->addClass("gmt_subtitle")
                    ->append("Search Meetings")
                ->ascend()
                /* form start */
                ->push(tag::form()) 
                    ->setAttr("method","POST")
                    ->setAttr("action","http://hashimadmin.sporefanz.com/gotomeeting/admin/start")
                    ->setID("searchMeetings")
                    ->append($frm)
                ->ascend() 
                /* form end */
            ->ascend()
        ->ascend()
        ;
        return $html;
    }
    /* ---------------  Start Meeting Form Page Display    ---------------- */


    /**
    *----------------  Start Meeting List Page Display    -----------------*
    */
    public function startMeetingsview($data, $msg, $start, $form, $page, $pages)
    {   
        $scripts = tag::div();
        $scripts
            ->push(tag::script())
                ->append("
                    function DelMeeting(mid){
                        $('#meetingId').val(mid);
                        $('form#DeleteMeeting').submit();
                    }                    
                ")
                ->ascend()
            ->ascend()
        ;


        if( ($data == NULL && $msg == 'No meetings found') || strpos($msg,'No meetings found between') !== false || $msg == 'Fill form properly in order to search meetings.' )
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->setAttr("style","color: black;")
                ->append($msg)
            ->ascend();

            $meetingList = tag::div();
        }
        else if($data == NULL && $msg == 'In order to start meeting ')
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->setAttr("style","color: black;")
                ->append($msg)
                    ->push(tag::a())
                        ->setAttr("style","color: green; text-decoration: none;")
                        ->setAttr("href",$start)
                        ->append("click here")
                    ->ascend()
            ->ascend();

            $meetingList = tag::div();
        }
        else
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->append($msg)
            ->ascend();

            $meetingList = tag::div();
            $meetingList
                ->addClass('gtm_row_headerContainer')
                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->setAttr("style", "width: 250px;")
                    ->append('Subject')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Max Participants')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Type')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Start')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('End')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->setAttr("style", "width: 210px;")
                    ->append('Conference Info')
                ->ascend()
            ->ascend()
            ;

            $counter = 2;
            foreach ($data as $key => $info) {
                if($counter%2 == 0)
                    $class = "gtm_row_white";
                else
                    $class = "gtm_row_normal";

                $meetingList
                    ->push(tag::div())
                        ->addClass('gtm_row_headerContainer')
                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 250px;")
                            ->append($info['subject'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->push(tag::a())
                                ->setAttr("href", "#")
                                ->setAttr("onclick", "DelMeeting(".$info["meetingid"].")")
                                ->append('Start')
                            ->ascend()
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['type'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['start'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['end'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 210px;")
                            ->append($info['conference'])
                        ->ascend()
                    ->ascend()
                ;
                $counter++;
            }
        }

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($scripts)

            ->push(tag::form()) /* form */
                ->setAttr("method","POST")
                ->setAttr("action","http://hashimadmin.sporefanz.com/gotomeeting/admin/start")
                ->setID("DeleteMeeting")

                ->push(shorttag::input())
                    ->setAttr("type","hidden")
                    ->setAttr("name","start")
                    ->setAttr("value",$form['start'])
                ->ascend()

                ->push(shorttag::input())
                    ->setAttr("type","hidden")
                    ->setAttr("name","end")
                    ->setAttr("value",$form['end'])
                ->ascend()

                ->push(shorttag::input())
                    ->setAttr("type","hidden")
                    ->setAttr("name","meetingId")
                    ->setID("meetingId")
                ->ascend()

            ->ascend()

            ->append($msg_box)
            ->append($meetingList)
        ->ascend()
        ;

        return $html;
    }
    /* ----------------  Start Meeting List Page Display    ----------------- */


    /**
    *----------------  Update Meeting Form Page Display    ------------------*
    */
    public function updateFormview($data, $msg, $page, $pages)
    {
        $frm = $this->searchform();
        $scripts = $this->searchformScript();

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($scripts)

            ->push(tag::div())
                ->addClass("gtm_data_container")
                ->push(tag::h1())
                    ->addClass("gmt_subtitle")
                    ->append("Search Meetings")
                ->ascend()
                /* form start */
                ->push(tag::form()) 
                    ->setAttr("method","POST")
                    ->setAttr("action","http://hashimadmin.sporefanz.com/gotomeeting/admin/update")
                    ->setID("searchMeetings") 
                    ->append($frm)
                ->ascend() 
                /* form end */
            ->ascend()
        ->ascend()
        ;
        return $html;
    }
    /* ---------------  Update Meeting Form Page Display    ---------------- */


    /**
    *----------------  Update Meeting List Page Display    -----------------*
    */
    public function updateview($data, $msg, $form, $page, $pages)
    {   
        $scripts = tag::div();
        $scripts
            ->push(tag::script())
                ->append("
                    function DelMeeting(mid){
                        $('#meetingId').val(mid);
                        $('form#DeleteMeeting').submit();
                    }                    
                ")
                ->ascend()
            ->ascend()
        ;


        if($data == NULL && $start == NULL )
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->setAttr("style","color: black;")
                ->append($msg)
            ->ascend();

            $meetingList = tag::div();
        }
        else if($data == NULL)
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->setAttr("style","color: black;")
                ->append($msg)
                    ->push(tag::a())
                        ->setAttr("style","color: green; text-decoration: none;")
                        ->setAttr("href",$start)
                        ->append('click here')
                    ->ascend()
            ->ascend();

            $meetingList = tag::div();
        }
        else
        {
            $msg_box = tag::div();
            $msg_box
                ->setID("gtm_msg")
                ->append($msg)
            ->ascend();

            $meetingList = tag::div();
            $meetingList
                ->addClass('gtm_row_headerContainer')
                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->setAttr("style", "width: 250px;")
                    ->append('Subject')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Max Participants')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Type')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('Start')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->append('End')
                ->ascend()

                ->push(tag::div())
                    ->addClass("gtm_row_white")
                    ->addClass("gtm_row_header")
                    ->setAttr("style", "width: 210px;")
                    ->append('Conference Info')
                ->ascend()
            ->ascend()
            ;

            $counter = 2;
            foreach ($data as $key => $info) {
                if($counter%2 == 0)
                    $class = "gtm_row_white";
                else
                    $class = "gtm_row_normal";

                $meetingList
                    ->push(tag::div())
                        ->addClass('gtm_row_headerContainer')
                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 250px;")
                            ->append($info['subject'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->push(tag::a())
                                ->setAttr("href", "#")
                                ->setAttr("onclick", "DelMeeting(".$info["meetingid"].")")
                                ->append('Update')
                            ->ascend()
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['type'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['start'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($info['end'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 210px;")
                            ->append($info['conference'])
                        ->ascend()
                    ->ascend()
                ;
                $counter++;
            }
        }

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($scripts)

            ->push(tag::form()) /* form */
                ->setAttr("method","POST")
                ->setAttr("action","http://hashimadmin.sporefanz.com/gotomeeting/admin/updateEdit")
                ->setID("DeleteMeeting")

                ->push(shorttag::input())
                    ->setAttr("type", "hidden")
                    ->setAttr("name", "start")
                    ->setAttr("value", $form['start'])
                ->ascend()

                ->push(shorttag::input())
                    ->setAttr("type", "hidden")
                    ->setAttr("name", "end")
                    ->setAttr("value", $form['end'])
                ->ascend()

                ->push(shorttag::input())
                    ->setAttr("type", "hidden")
                    ->setAttr("name", "meetingId")
                    ->setID("meetingId")
                ->ascend()

            ->ascend()

            ->append($msg_box)
            ->append($meetingList)
        ->ascend()
        ;

        return $html;
    }
    /* ----------------  Update Meeting List Page Display    ----------------- */


    /**
    *----------------  Update Meeting List Page Display    -----------------*
    */
    public function updateEditview( $data, $msg )
    {
        if($data)
        {
            $scripts = $this->createformScript();
            $form = $this->createform($data);
            
            $html = tag::div();
            $html
                ->setID("page_container")
                ->append($scripts)

                ->push(tag::div())
                    ->addClass("gtm_data_container")
                    ->push(tag::h1())
                        ->addClass("gmt_subtitle")
                        ->append("Update Meeting")
                    ->ascend()
                    ->push(tag::form()) /* form */
                        ->setAttr("method","POST")
                        ->setAttr("action","http://hashimadmin.sporefanz.com/gotomeeting/admin/updated")
                        ->setID("CreateMeeting")
                        ->append($form)
                    ->ascend() /* form */
                ->ascend()
            ->ascend()
            ;
        }
        return $html;
    }
    /* ----------------  Update Meeting List Page Display    ----------------- */


    /**
    *----------------  Updated Meeting Display    -----------------*
    */
    public function updatedview( $msg )
    {
        $html = tag::div();
        $html
            ->push(tag::div())
                ->setID("gtm_msg")
                ->append($msg)
            ->ascend()
        ->ascend()
        ;

        return $html;
    }
    /* ----------------  Updated Meeting Display    ----------------- */


    /* ----------------  Basic Form    ----------------- */
    public function someForm($data)
    {
        $form = new basicForm;
        $form
            ->setAjaxFunc("formProcess")
            ->setData($data) 
        ;
        $form
            ->newRow("First Name")
                ->pushIText("fname")
            ->ascend()

            ->newRow("Last Name")
                ->pushIText("lname")
            ->ascend()
            
            ->newRow("Email")
                ->pushIText("Email")
            ->ascend()

            ->newRow() // no label
            ->pushISubmit()
        ;
        return $form;
    }
    public function someDisplay($arr)
    {

        $html = tag::div();
        $html
            ->setID("myDiv1")
            ->push(tag::div())
                ->setID("myDiv1")
            ->ascend()
            ->push($this->someForm($arr))
        ;
        return $html;
    }
    /* ----------------  Basic Form    ----------------- */


    

}

?>