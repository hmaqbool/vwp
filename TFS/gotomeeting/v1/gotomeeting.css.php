<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingCSS extends AppCSS
{
    public function addCSS()
    {
      ?>/*
            
            header('Content-type: text/css');
            $css =" 
            */
                  <style>
                        #gtm_data_container{
                              padding-left: 200px;
                              overflow: hidden;
                        }

                        .gtm_data_container{
                              padding-left: 200px;
                              overflow: hidden;
                        }

            		.gmt_subtitle{
            			margin-bottom: 30px;
                              margin-top: 50px;
            		}

            		.gmt_inputbox{
                              width: 200px;
                        }

                        .gmt_btn{
                              width: 100px;
                              float: left;
                        }

                        .gtm_btnContainer{
                              margin-left: 150px;
                              width: 400px;
                              float: left;
                        }

            		.gtm_formcontainer{
            		}

            		.gtm_formfeild{
                              width: 200px;
                              float: left;
                        }

            		.gtm_formlabel{
            			width: 150px;
            			font-weight: bold;
            			float: left;
            			padding-top: 6px;
						font-size: 14px;
            		}

            		.gtm_row{
                              width: 600px;
                              padding-top: 15px;
                              overflow: hidden;
                              float: none;                        
                        }

                        #gtm_msg{
                              text-align: center;
                              color: green;  
                              font-size: 14px;  
                              margin-top: 10px;              
                        }

                        .gtm_btn_submit:hover
                        {
                              text-shadow: 0 1px 1px #6f3a02;
                              border:1px solid #e6791c;
                              border-bottom:1px solid #d86f15;
                              background: #f48423;
                              background: -moz-linear-gradient(top, #ffdf9e, #f5b026 2%, #f48423);
                              background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #ffd683), color-stop(.01, #f5b026), to(#f48423)); 
                              cursor:pointer;
                        }

                        .gtm_btn_submit
                        {
                              color:#fff;
                              text-decoration:none;
                              font-size:14px;
                              font-family:Arial, Helvetica, sans-serif;
                              font-weight:bold;
                              text-shadow:0 1px 1px #0c507b;
                              letter-spacing:0px;
                              text-transform:uppercase;
                              padding:8px 12px 6px 12px;
                              margin:0 10px 5px 0;
                              background: #3aa3e6;
                              background: -moz-linear-gradient(top, #87c6ee, #3aa3e6 2%, #028fe8);
                              background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #87c6ee), color-stop(.01, #3aa3e6), to(#028fe8)); 
                              border:1px solid #0082d5;
                              border-radius: 5px;
                              -moz-border-radius: 5px;
                              -webkit-border-radius: 5px;
                              outline:none;
                        }

                        .gtm_btn_reset
                        {
                              color: #fff; 
                              text-decoration: none;
                              font-size: 14px;
                              font-family: Arial, Helvetica, sans-serif;
                              font-weight: bold;
                              text-shadow: 0 1px 1px #707070;
                              letter-spacing: 0px;
                              text-transform: uppercase;
                              padding: 8px 12px 6px 12px;
                              margin: 0 10px 5px 0;
                              border:1px solid #ADADAD;
                              border-radius: 5px;
                              -moz-border-radius: 5px;
                              -webkit-border-radius: 5px;
                              background: #C9C9C9;
                              outline:none;
                        }

                        #ui-datepicker-div, .ui-datepicker{ font-size: 100%; position: fixed;}

                        .gtm_row_white{
                              float: left; 
                              width: 110px; 
                              height: 49px; 
                              padding-top: 15px; 
                              text-align: center;
                              background-color: white;
                        }

                        .gtm_row_normal{
                              float: left; 
                              width: 110px; 
                              height: 49px; 
                              padding-top: 15px; 
                              text-align: center;
                              background-color: #D4E9FC;
                        }

                        .gtm_row_header{
                              height: 30px; 
                              color: white; 
                              background-color: #343434;
                        }

                        .gtm_row_headerContainer{
                              width: 900px; 
                              height: auto; 
                              overflow: hidden;
                        }
                  </style>
                  <?
                  /*
            ";
            echo $css;
            */
            parent::addCSS();
    }
}

?>