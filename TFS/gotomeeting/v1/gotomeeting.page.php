<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingPage extends DashboardAppPage
{

    /**
    * Function      : admin()
    * Description   : Main Landing Page
    */
    public function admin()
    {
        $sc = SiteConfig::getObj();
        $sc->template("blank2.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $user = $logic->landingPageLogic();

        $this->regDisplay("mainview", $msg);
    }


    
    /* ------------------    Main Landing Page Ends   ----------------------- */





























    /**
    * Function      : admin_create()
    * Description   : Create Page, this page contains meeting creation form.
    */
    public function admin_createMeeting()
    {
        $sc = SiteConfig::getObj();
        $sc->template("template3.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }


        $this->regDisplay("createview");
    }
    /* ------------------    Create Page Ends        -------------------------- */



    /**
    * Function      : admin_created()
    * Description   : Created Page, this page contains information regarding meeting created.
    */
    public function admin_created()
    {
        $sc = SiteConfig::getObj();
        $sc->template("template3Simple.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {
            $user = $logic->getToken();
            $startDateTime = new DateTime($_POST['start']);
            $_POST['start'] = $startDateTime->format('c');
            $endDateTime = new DateTime($_POST['end']);
            $_POST['end'] = $endDateTime->format('c');

            if(isset($user['access_token']))
            {
                $citrix = new CitrixAPI($user['access_token'], $user['organizer_key']);
                $result = $citrix->CreateMeeting($_POST["subject"], $_POST["start"], $_POST["end"], $_POST["conference"], $_POST["meeting"]);
                $data = json_decode($result,true);

                if($data)
                    $msg = 'Meeting Created Sucessfully';
                else
                    $msg = 'Something went wrong please try again';
            } 
            else
                $msg = 'Please update token first';
        }
        else
            $msg = 'Fill form in order to create meeting.';

        $this->regDisplay("createdview",$data,$msg);
    }
    /* ------------------    Created Page Ends        --------------------------- */



    /**
    * Function      : admin_getFutureMeetings()
    * Description   : getFutureMeetings Page, this page contains form for searching meeting.
    */
    public function admin_getFutureMeetings()
    {
        $sc = SiteConfig::getObj();
        $sc->template("blank2.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regDisplay("FutureMeetingsFormview");
    }
    /* --------------------    getFutureMeetings Page Ends      ------------------- */



    /**
    * Function      : admin_futureMeetings()
    * Description   : futureMeetings Page, this page display future meetings.
    */
    public function admin_futureMeetings()
    {
        $sc = SiteConfig::getObj();
        $sc->template("blank2.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {    
            $user = $logic->getToken();
            $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
            $startDateTime = new DateTime($_POST['start']);
            $start = $startDateTime->format('c');
            $endDateTime = new DateTime($_POST['end']);
            $end = $endDateTime->format('c');

            if(isset($user['access_token']) )
            {
                $result = $citrix->GetFutuerMeeting($start, $end );
                $data = json_decode($result, true);

                if(!$data)
                    $msg = 'Something went wrong please try again';
            } 
            else
                $msg = 'Please update token first';
        }
        else
            $msg = 'Fill form properly in order to search meetings.';


        if($data)
        {
            $count = 0;

            foreach ($data as $info) 
            {
                $invalid = 0;

                if( strtotime($info["startTime"]) <= strtotime($_POST['start']) )
                    $invalid = 1;

                if( strtotime($info["startTime"]) > strtotime($_POST['end']) )
                    $invalid = 1;

                if($invalid == 0)
                {
                    $time = strtotime($info["startTime"]);
                    $StartedAt = date('Y-m-d H:i',$time);
                    $time = strtotime($info["endTime"]);
                    $EndedAt = date('Y-m-d H:i',$time);
                    $searches[$count]['subject'] = $info['subject'];
                    $searches[$count]['participants'] = $info['maxParticipants'];
                    $searches[$count]["meetingid"] = $info["meetingid"];
                    $searches[$count]['type'] = $info["meetingType"];
                    $searches[$count]['start'] = $StartedAt;
                    $searches[$count]['end'] = $EndedAt;
                    $searches[$count]['conference'] = $info["conferenceCallInfo"];

                    $count++;
                }
            }

            if($searches == null)
                $msg = "No meetings found between ".$_POST['start']." and ".$_POST['end'];
        }

        $this->regDisplay("FutureMeetingsview", $searches, $msg );
    }
    /* --------------------     futureMeetings Page Ends       ------------------- */



    /**
    * Function      : admin_deleteMeeting()
    * Description   : deleteMeeting Page, this page contains form for meeting search in order to delete.
    */
    public function admin_deleteMeeting()
    {
        $sc = SiteConfig::getObj();
        $sc->template("blank2.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regDisplay("deleteFormview");
    }
    /* ------------------    Delete Meeting Form Page      ------------------ */



    /**
    * Function      : admin_deleteMeeting()
    * Description   : delete Page, this page display future meetings with delete option.
    */
    public function admin_delete()
    {
        $sc = SiteConfig::getObj();
        $sc->template("blank2.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {                
            $user = $logic->getToken();
            $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
            $startDateTime = new DateTime($_POST['start']);
            $start = $startDateTime->format('c');
            $endDateTime = new DateTime($_POST['end']);
            $end = $endDateTime->format('c');

            if( isset($user['access_token']) )
            {
                if(isset($_POST['meetingId']) )
                {
                    $result = $citrix->DeleteMeeting($_POST['meetingId']);

                    if($result == 204 || $result == '204' )
                        $msg = 'Success: Meeting has been deleted';
                    else
                        $msg = '';
                }

                $result = $citrix->GetFutuerMeeting($start, $end );
                $data = json_decode($result, true);

                if(!$data)
                    $msg = 'No Meetings Found';
            } 
            else
                $msg = 'Please update token first';
        }
        else
            $msg = 'Fill form properly in order to search meetings.';

        if($data)
        {
            $count = 0;

            foreach ($data as $info) 
            {
                $invalid = 0;

                if( strtotime($info["startTime"]) <= strtotime($_POST['start']) )
                    $invalid = 1;

                if( strtotime($info["startTime"]) > strtotime($_POST['end']) )
                    $invalid = 1;

                if($invalid == 0)
                {
                    $time = strtotime($info["startTime"]);
                    $StartedAt = date('Y-m-d H:i',$time);
                    $time = strtotime($info["endTime"]);
                    $EndedAt = date('Y-m-d H:i',$time);
                    $searches[$count]['subject'] = $info['subject'];
                    $searches[$count]['participants'] = $info['maxParticipants'];
                    $searches[$count]["meetingid"] = $info["meetingid"];
                    $searches[$count]['type'] = $info["meetingType"];
                    $searches[$count]['start'] = $StartedAt;
                    $searches[$count]['end'] = $EndedAt;
                    $searches[$count]['conference'] = $info["conferenceCallInfo"];

                    $count++;
                }
            }

            if($searches == null)
                $msg = "No meetings found between ".$_POST['start']." and ".$_POST['end'];
        }

        $this->regDisplay("deleteMeetingsview", $searches, $msg, $_POST );
    }
    /* ------------------    Delete Meeting List Page      ------------------ */



    /**
    * Function      : admin_startMeeting()
    * Description   : startMeeting Page, this page contains form for meeting search in order to start meeting.
    */
    public function admin_startMeeting()
    {
        $sc = SiteConfig::getObj();
        $sc->template("blank2.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regDisplay("startFormview");
    }
    /* ------------------    Start Meeting Form Page       ------------------ */



    /**
    * Function      : admin_deleteMeeting()
    * Description   : start Page, this page display future meetings with start option.
    */
    public function admin_start()
    {
        $sc = SiteConfig::getObj();
        $sc->template("blank2.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {                
            $user = $logic->getToken();
            $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
            $startDateTime = new DateTime($_POST['start']);
            $start = $startDateTime->format('c');
            $endDateTime = new DateTime($_POST['end']);
            $end = $endDateTime->format('c');

            if( isset($user['access_token']) )
            {
                if(isset($_POST['meetingId']) )
                {
                    $result = $citrix->StartMeeting($_POST['meetingId']);
                    $start = json_decode($result, true);

                    if($start["hostURL"])
                        $msg = 'In order to start meeting ';
                }
                else
                {
                    $result = $citrix->GetFutuerMeeting($start, $end );
                    $data = json_decode($result, true);

                    if(!$data)
                        $msg = 'No meetings found';
                }
            } 
            else
                $msg = 'Please update token first';
        }
        else
            $msg = 'Fill form properly in order to search meetings.';

        if($data)
        {
            $count = 0;

            foreach ($data as $info) 
            {
                $invalid = 0;

                if( strtotime($info["startTime"]) <= strtotime($_POST['start']) )
                    $invalid = 1;

                if( strtotime($info["startTime"]) > strtotime($_POST['end']) )
                    $invalid = 1;

                if($invalid == 0)
                {
                    $time = strtotime($info["startTime"]);
                    $StartedAt = date('Y-m-d H:i',$time);
                    $time = strtotime($info["endTime"]);
                    $EndedAt = date('Y-m-d H:i',$time);
                    $searches[$count]['subject'] = $info['subject'];
                    $searches[$count]['participants'] = $info['maxParticipants'];
                    $searches[$count]["meetingid"] = $info["meetingid"];
                    $searches[$count]['type'] = $info["meetingType"];
                    $searches[$count]['start'] = $StartedAt;
                    $searches[$count]['end'] = $EndedAt;
                    $searches[$count]['conference'] = $info["conferenceCallInfo"];

                    $count++;
                }
            }

            if($searches == null)
                $msg = "No meetings found between ".$_POST['start']." and ".$_POST['end'];
        }

        $this->regDisplay("startMeetingsview", $searches, $msg, $start["hostURL"], $_POST );
    }
    /* ------------------    Start Meeting List Page      ------------------ */



    /**
    * Function      : admin_updateMeeting()
    * Description   : updateMeeting Page, this page contains form for meeting search in order to update.
    */
    public function admin_updateMeeting()
    {
        $sc = SiteConfig::getObj();
        $sc->template("template3.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regDisplay("updateFormview");
    }

    /* ------------------    Update Meeting Form Page      ------------------ */



    /**
    * Function      : admin_update()
    * Description   : update Page, this page display future meetings with update option.
    */
    public function admin_update()
    {
        $sc = SiteConfig::getObj();
        $sc->template("blank2.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {                
            $user = $logic->getToken();

            if( isset($user['access_token']) )
            {
                $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
                $startDateTime = new DateTime($_POST['start']);
                $start = $startDateTime->format('c');
                $endDateTime = new DateTime($_POST['end']);
                $end = $endDateTime->format('c');
                $result = $citrix->GetFutuerMeeting($start, $end );
                $data = json_decode($result, true);

                if(!$data)
                    $msg = 'No Meeting Found';
            } 
            else
                $msg = 'Please update token first';
        }
        else
            $msg = 'Fill form properly in order to search meetings.';

        if($data)
        {
            $count = 0;

            foreach ($data as $info) 
            {
                $invalid = 0;

                if( strtotime($info["startTime"]) <= strtotime($_POST['start']) )
                    $invalid = 1;

                if( strtotime($info["startTime"]) > strtotime($_POST['end']) )
                    $invalid = 1;

                if($invalid == 0)
                {
                    $time = strtotime($info["startTime"]);
                    $StartedAt = date('Y-m-d H:i',$time);
                    $time = strtotime($info["endTime"]);
                    $EndedAt = date('Y-m-d H:i',$time);
                    $searches[$count]['subject'] = $info['subject'];
                    $searches[$count]['participants'] = $info['maxParticipants'];
                    $searches[$count]["meetingid"] = $info["meetingid"];
                    $searches[$count]['type'] = $info["meetingType"];
                    $searches[$count]['start'] = $StartedAt;
                    $searches[$count]['end'] = $EndedAt;
                    $searches[$count]['conference'] = $info["conferenceCallInfo"];

                    $count++;
                }
            }

            if($searches == null)
                $msg = "No meetings found between ".$_POST['start']." and ".$_POST['end'];
        }

        $this->regDisplay("updateview", $searches, $msg);
    }
    /* ------------------    Update Meeting List Page      ------------------ */



    /**
    * Function      : admin_updateEdit()
    * Description   : updateEdit Page, this page display update meeting form.
    */
    public function admin_updateEdit()
    {
        $sc = SiteConfig::getObj();
        $sc->template("blank2.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {                
            $user = $logic->getToken();

            if($user['access_token'])
            {
                $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
                $result = $citrix->GetMeeting($_POST['meetingId']);
                $data = json_decode($result, true);

                if(!$data)
                    $msg = 'Something went wrong please try again';
            }
            else
                $msg = 'Please update token first';
        }
        else
            $msg = 'No meeinting selected for update';

        $this->regDisplay("updateEditview", $data, $msg);

    }
    /* ------------------    Update Edit Meeting Page      ------------------ */



    /**
    * Function      : admin_updated()
    * Description   : updated Page, this page display update meeting conformation.
    */
    public function admin_updated()
    {
        $sc = SiteConfig::getObj();
        $sc->template("template3.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {                
            $user = $logic->getToken();
            if($user['access_token'])
            {
                $startDateTime = new DateTime($_POST['start']);
                $start = $startDateTime->format('c');
                $endDateTime = new DateTime($_POST['end']);
                $end = $endDateTime->format('c');
                $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
                $result = $citrix->UpdateMeeting($_POST["meetingId"], $_POST["subject"], $start, $end, $_POST["conference"], $_POST["meeting"]);

                if($result == 204)
                    $msg = 'Congratulations! meeting has been updated';
                else
                    $msg = 'Unable to delete meeting now please try again';
            }
            else
                $msg = 'Please update token first';
        }
        else
            $msg = 'No meeinting selected for update';
        
        $this->regDisplay("updatedview", $msg);
    }
    /* ------------------    Updated Meeting Page      ------------------ */



    /**
    * Function      : admin_basicForm()
    * Description   : basicForm Page, it is demo form.
    */
    public function admin_basicForm()
    {
        $sc = SiteConfig::getObj();
        $sc->template("template3.php");

         $this->regStdAjax(
            "formProcess",
            "formProcess",
            "Processed",
            "Sucess", 
            array()
        );

        $this->doAjax();
        $this->regDisplay("someDisplay");
    }
    /* ------------------    basicForm Demo     ------------------ */
}
?>