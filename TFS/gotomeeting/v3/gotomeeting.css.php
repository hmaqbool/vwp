<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingCSS extends AppCSS
{
    public function addCSS()
    {
      ?>
      <style>
            #gtmDataContainer{
                  padding-left: 200px;
                  overflow: hidden;
            }

            .gtmDataContainers{
                  padding-left: 200px;
            }

            .gtmSubtitle{
                  margin-bottom: 30px;
                  margin-top: 50px;
            }

            .gtmInputbox{
                  width: 200px;
            }

            .gtmBtn{
                  width: 100px;
                  float: left;
            }

            .gtmBtnContainer{
                  margin-left: 150px;
                  width: 400px;
                  float: left;
            }

            .gtmFormContainer{
            }

            .gtmFormFeild{
                  width: 200px;
                  float: left;
            }

            .gtmFormLabel{
                  width: 150px;
                  font-weight: bold;
                  float: left;
                  padding-top: 6px;
                  font-size: 14px;
            }

            .gtmRow{
                  width: 600px;
                  padding-top: 15px;
                  overflow: hidden;
                  float: none;                        
            }

            #gtmMsg{
                  text-align: center;
                  color: green;  
                  font-size: 14px;  
                  margin-top: 10px;              
            }

            .gtmBtnSubmit:hover{
                  text-shadow: 0 1px 1px #6f3a02;
                  border:1px solid #e6791c;
                  border-bottom:1px solid #d86f15;
                  background: #f48423;
                  background: -moz-linear-gradient(top, #ffdf9e, #f5b026 2%, #f48423);
                  background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #ffd683), color-stop(.01, #f5b026), to(#f48423)); 
                  cursor:pointer;
            }

            .gtmBtnSubmit{
                  color:#fff;
                  text-decoration:none;
                  font-size:14px;
                  font-family:Arial, Helvetica, sans-serif;
                  font-weight:bold;
                  text-shadow:0 1px 1px #0c507b;
                  letter-spacing:0px;
                  text-transform:uppercase;
                  padding:8px 12px 6px 12px;
                  margin:0 10px 5px 0;
                  background: #3aa3e6;
                  background: -moz-linear-gradient(top, #87c6ee, #3aa3e6 2%, #028fe8);
                  background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #87c6ee), color-stop(.01, #3aa3e6), to(#028fe8)); 
                  border:1px solid #0082d5;
                  border-radius: 5px;
                  -moz-border-radius: 5px;
                  -webkit-border-radius: 5px;
                  outline:none;
            }

            .gtmBtnReset{
                  color: #fff; 
                  text-decoration: none;
                  font-size: 14px;
                  font-family: Arial, Helvetica, sans-serif;
                  font-weight: bold;
                  text-shadow: 0 1px 1px #707070;
                  letter-spacing: 0px;
                  text-transform: uppercase;
                  padding: 8px 12px 6px 12px;
                  margin: 0 10px 5px 0;
                  border:1px solid #ADADAD;
                  border-radius: 5px;
                  -moz-border-radius: 5px;
                  -webkit-border-radius: 5px;
                  background: #C9C9C9;
                  outline:none;
                  margin-top: 30px;
                  margin-left: 50px;
            }

            #ui-datepicker-div, .ui-datepicker{ 
                  font-size: 85%; 
                  position: fixed;
            }

            .gtmRowWhite{
                  float: left; 
                  width: 110px; 
                  height: 49px; 
                  padding-top: 15px; 
                  text-align: center;
                  background-color: white;
            }

            .gtmRowNormal{
                  float: left; 
                  width: 110px; 
                  height: 49px; 
                  padding-top: 15px; 
                  text-align: center;
                  background-color: #D4E9FC;
            }

            .gtmRowHeader{
                  float: left; 
                  width: 110px; 
                  padding-top: 15px; 
                  text-align: center;
                  height: 30px; 
                  color: white; 
                  background-color: #343434;
            }

            .gtmRowHeaderContainer{
                  width: 900px; 
                  height: auto; 
                  overflow: hidden;
            }
            
            .frmLabel{
                  width: 150px !important;   
            }

            .frmFieldCont{
                  width: 450px;
            }
            
            .frmField{
                  float: left;
            }

            .frmErr {
                  width: 150px;
                  height: 30px;
                  float: left;
            }

            .frmRowNorm {
                  background-color: transparent !important;
            }

            .frmRowAlt {
                background-color: transparent !important;  
            }
            
            select{
                  width: 130px;
            }

            .gtmBtnContainer{
                  margin-top: 20px;
                  margin-left: 20px;
            }

            .gtmMeetingInfo{
                  width: 50px; 
                  color: green;
            }

      </style>
      <?
      parent::addCSS();
    }
}

?>