<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */
class GoToMeetingDisplay extends DashboardAppDisplay
{
	public static $urlVars = array();

    /**
    * @author:  M. Hashim
    * @date:    Friday, October 11, 2013
    * @desc:    Landing page's view
    */
    public function homeView($data, $page, $pages)
    {
        $html = tag::div();
        $html
            ->setID("pageContainer")

            ->push(tag::p())
                ->setAttr("style","text-align: center; color: green")
                ->append($data)
                ->push(tag::br())
                ->ascend()
            ->ascend()

            ->push(tag::div())
                ->setAttr("style","margin-top: 200px;")
                ->push(tag::h3())
                    ->setAttr("style","text-align: center;")
                    ->append('Click on above navigation in order to perform actions.')
                ->ascend()
            ->ascend()
        ->ascend()
        ;
        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 14, 2013
    * @desc:    create meeting form
    */
    public function createForm($data)
    {
        $form = new basicForm;
        $form
            ->setData($data)
        ;
        $typeText = ($data["meetingType"]) ? $data["meetingType"] : 'Scheduled';
        $typeVal = ($data["meetingType"]) ? $data["meetingType"] : 'scheduled';
        $smbt = ($data["subject"]) ? 'Update' : 'Create';

        if($data["startTime"] != null && $data["endTime"] != null)
        {
            $time = strtotime($data["startTime"]);
            $data["startTime"] = date('Y-m-d H:i',$time);

            $time = strtotime($data["endTime"]);
            $data["endTime"] = date('Y-m-d H:i',$time);
        }

        $conferenceType = array(
            array("value"=>"PSTN", "name"=>"PSTN"),
            array("value"=>"Free", "name"=>"Free"),
            array("value"=>"Hybrid", "name"=>"Hybrid"),
            array("value"=>"Private", "name"=>"Private"),
            array("value"=>"VoIP", "name"=>"VoIP"),
        );

        $meetingType = array(
            array("value"=>"immediate", "name"=>"Immediate"),
            array("value"=>"recurring", "name"=>"Recurring"),
        );

        $form
            ->newRow("Subject")
                ->pushIText("subject")
                    ->setID("subject")
                ->ascend()

            ->newRow("Start Date Time")
                ->pushIText("start")
                    ->setID("start")
                    ->setVal($data["startTime"])
                ->ascend()

            ->newRow("End Date Time")
                ->pushIText("end")
                    ->setID("end")
                    ->setVal($data["endTime"])
                ->ascend()

            ->newRow("Conference Type")
                ->pushSelect("conference", $conferenceType, "name", "value")
                    ->startOption('select', "Select")
                ->ascend()

            ->newRow("Meeting Type")
                ->pushSelect("meeting",$meetingType, "name", "value")
                    ->startOption($typeVal, $typeText)
                ->ascend()
            ->newRow()
                ->pushIhidden("meetId")
                    ->setID("meetId")
                ->ascend()

            ->newRow()
                ->pushIButton("Reset")
                    ->setVal("Reset")
                    ->addClass("gtmBtnReset")
                    ->addAttr("onclick","location.reload();")
                ->ascend()
                ->pushIButton("create")
                    ->setVal($smbt)
                    ->setID("create")
                    ->addClass("gtmBtnSubmit")
                    ->addAttr("onclick","createValidation();")
        ;
        return $form;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    action form for meeting's delete, update, start functions
    */
    public function actionForm()
    {
        $form = new basicForm;
        $form
            ->pushIhidden("mid")
                ->setID("mid")
            ->ascend()

            ->pushIhidden("start")
                ->setID("start")
            ->ascend()

            ->pushIhidden("end")
                ->setID("end")
            ->ascend()
        ;
        return $form;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    action script for meeting's delete, update, start functions
    */
    public function actionScript()
    {
        global $template;
        $template->pushHeader("
            <script type=\"text/javascript\">
                function action(mid)
                {
                    $('#mid').val(mid);
                    if($('#mid').val() != '')
                        $('#frm').submit();
                }
            </script>"
        );
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    search meeting form
    */
    public function searchForm($data, $type = '')
    {
        $form = new basicForm;
        $form
            ->setData($data);

        $form
            ->newRow("Start Date Time")
                ->pushIText("start")
                    ->setID("start")
                ->ascend()

            ->newRow("End Date Time")
                ->pushIText("end")
                    ->setID("end")
                ->ascend()

            ->newRow()
                ->pushIButton("Reset")
                    ->setVal("Reset")
                    ->addClass("gtmBtnReset")
                    ->addAttr("onclick","location.reload();")
                ->ascend()
                ->pushIButton("search")
                    ->setVal($smbt)
                    ->setID("search")
                    ->setVal("Search")
                    ->addClass("gtmBtnSubmit")
                    ->addAttr("onclick","searchValidation(\"$type\");")
        ;
        return $form;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    create meeting form script
    */
    public function createFormScript()
    {
        global $template;
        $template->pushHeader("
            <script type=\"text/javascript\">
                function createValidation()
                {
                    var error = 0;
                    var msg = '';
                    var dd;
                    var date;
                    var index = 1;
                    var now = new Date();

                    if(now.getDate() < 10)
                        dd = '0'+now.getDate();
                    else
                        dd = now.getDate();

                    date = now.getFullYear()+''+(now.getMonth()+1) + '' + dd + now.getHours() + '' + (now.getMinutes() - 5);
                    date = parseInt(date);


                    if($('#start').val() == '')
                    {
                        error = 1;
                        msg += index+'. Start Date Time can\'t be empty <br/>';
                        index++;
                    }
                    else
                    {
                        /* timestamp function for start */
                        start = $('#start').val();
                        var datetime = start.split(' ');
                        var strt_date = datetime[0];
                        var strt_time = datetime[1];
                        var start_date = strt_date.split('/');
                        var start_time = strt_time.split(':');
                        var start_timestamp = start_date[2]+''+start_date[0]+''+start_date[1]+''+start_time[0]+''+start_time[1];
                        start = parseInt(start_timestamp);
                    }

                    if($('#end').val() == '')
                    {
                        error = 1;
                        msg += index+'. End Date Time can\'t be empty <br/>';
                        index++;
                    }
                    else
                    {
                        /* timestamp function for end */
                        end = $('#end').val();
                        datetime = end.split(' ');
                        var ed_date = datetime[0];
                        var ed_time = datetime[1];
                        var end_date = ed_date.split('/');
                        var end_time = ed_time.split(':');
                        var end_timestamp = end_date[2]+''+end_date[0]+''+end_date[1]+''+end_time[0]+''+end_time[1];
                        end = parseInt(end_timestamp);
                    }

                    if($('#subject').val() == '')
                    {
                        error = 1;
                        msg += index+'. Subject can\'t be empty <br/>';
                        index++;
                    }

                    if($('#conference').val() == 'Select')
                    {
                        error = 1;
                        msg += index+'. Conference Type can\'t be empty <br/>';
                        index++;
                    }

                    if($('#meetingtype').val() == 'Select')
                    {
                        error = 1;
                        msg += index+'. Meeting Type can\'t be empty <br/>';
                        index++;
                    }

                    if(start >= end)
                    {
                        error = 1;
                        msg += index+'. End time should be greater than start time <br/>';
                        index++;
                    }

                    if(date > start)
                    {
                        error = 1;
                        msg += index+'. Start time should be greater than current time <br/>';
                        index++;
                    }

                    if(error == 0)
                        $('#frm').submit();
                    else
                        alert(msg);
                }
            </script>"
        );
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    create meeting form script
    */
    public function searchFormScript()
    {
        global $template;
        $template->pushHeader("
            <script type=\"text/javascript\">
                function searchValidation(type)
                {
                    var error = 0;
                    var msg = '';
                    var dd;
                    var date;
                    var index = 1;
                    var now = new Date();

                    if(now.getDate() < 10)
                        dd = '0'+now.getDate();
                    else
                        dd = now.getDate();

                    if(now.getHours() < 10)
                        hrs = '0'+now.getHours();
                    else
                        hrs = now.getHours();

                    if(now.getMinutes() < 10)
                        mnts = '0'+now.getMinutes();
                    else
                        mnts = now.getMinutes();



                    date = now.getFullYear()+''+(now.getMonth()+1) + '' + dd + hrs + '' + mnts;
                    currentDate = parseInt(date);

                    startDate = (now.getMonth()+1) + '/' + dd +'/'+ now.getFullYear()+' '+ hrs + ':' + mnts;

                    if($('#start').val() == '')
                    {
                        error = 1;
                        msg += index+'. Start Date Time can\'t be empty <br/>';
                        index++;
                    }
                    else
                    {
                        /* timestamp function for start */
                        var start = $('#start').val();
                        var datetime = start.split(' ');
                        var strt_date = datetime[0];
                        var strt_time = datetime[1];
                        var start_date = strt_date.split('/');
                        var start_time = strt_time.split(':');
                        var start_timestamp = start_date[2]+''+start_date[0]+''+start_date[1]+''+start_time[0]+''+start_time[1];
                        start = parseInt(start_timestamp);
                    }

                    if($('#end').val() == '')
                    {
                        error = 1;
                        msg += index+'. End Date Time can\'t be empty <br/>';
                        index++;
                    }
                    else
                    {
                        /* timestamp function for end */
                        end = $('#end').val();
                        datetime = end.split(' ');
                        var ed_date = datetime[0];
                        var ed_time = datetime[1];
                        var end_date = ed_date.split('/');
                        var end_time = ed_time.split(':');
                        var end_timestamp = end_date[2]+''+end_date[0]+''+end_date[1]+''+end_time[0]+''+end_time[1];
                        end = parseInt(end_timestamp);
                    }

                    if( start < currentDate )
                    {
                        if(type != 'searchMeetings'){
                            $('#start').val(startDate);
                        }
                    }

                    if(start >= end)
                    {
                        error = 1;
                        msg += index+'. End time should be greater than start time <br/>';
                        index++;
                    }

                    if(error != 1)
                    {
                        $('#frm').submit();
                    }
                    else
                        alert(msg);
                }
            </script>"
        );
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 14, 2013
    * @desc:    create meeting form page's view
    */
    public function createFormView($formData)
    {
        global $template;
        $template
            ->pushReadyFunc('
                $("#start").datetimepicker();
                $("#end").datetimepicker();
                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
                $("form").get(0).setAttribute("id", "frm");
                $("form").get(0).setAttribute("action", "'.$this->getPathX().'admin/meetingCreated");
            ');
        $this->createFormScript();
        $html = tag::div();
        $html
            ->setID("pageContainer")
            ->push(tag::div())
                ->addClass("gtmDataContainers")
                ->push(tag::h1())
                    ->addClass("gtmSubtitle")
                    ->append("Create Meetings")
                ->ascend()
                ->push($this->createForm($formData))
            ->ascend()
        ->ascend()
        ;
        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    Meeting details view
    */
    public function meetingCreatedView($data)
    {
        $html = tag::div();
        $html
            ->setID("pageContainer")
            ->push(tag::div())
                ->setID("gtmMsg")
                ->append($data['msg'])
            ->ascend()

            ->push(tag::div())
                ->addClass("gtmDataContainers")
                
                ->push(tag::h1())
                    ->addClass("gtmSubtitle")
                    ->append("Meeting Details")
                ->ascend()

                ->push(tag::p())
                    ->push(tag::span())
                        ->addClass("gtmMeetingInfo")
                        ->append('Join Url : ')
                    ->ascend()
                    ->append($data[0]["joinURL"])
                    ->push(tag::br())

                    ->push(tag::span())
                        ->addClass("gtmMeetingInfo")
                        ->append('Meeting Id : ')
                    ->ascend()
                    ->append($data[0]["uniqueMeetingId"])
                    ->push(tag::br())

                    ->push(tag::span())
                        ->addClass("gtmMeetingInfo")
                        ->append('Max Participants : ')
                    ->ascend()
                    ->append($data[0]["maxParticipants"])
                    ->push(tag::br())

                    ->push(tag::span())
                        ->addClass("gtmMeetingInfo")
                        ->append('Conference Info : ')
                    ->ascend()
                    ->append($data[0]["conferenceCallInfo"])
                    ->push(tag::br())
                
                ->ascend()
            ->ascend()

            ->push(tag::p())
                ->append('To create more meetings ')
                ->push(tag::a())
                    ->setAttr("style","text-decoration: none; color: green;")
                    ->setAttr("href",$this->getPathX()."admin/createMeeting/")
                    ->append('click here')
                ->ascend()
            ->ascend()
            ;
        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    Meeting List header
    */
    public function meetingListHeader($headerNames)
    {
        $meetingList = tag::div();
        $meetingList
            ->addClass('gtmRowHeaderContainer')
            ->push(tag::div())
                ->addClass("gtmRowHeader")
                ->setAttr("style", "width: 250px;")
                ->append($headerNames[0])
            ->ascend()

            ->push(tag::div())
                ->addClass("gtmRowHeader")
                ->append($headerNames[1])
            ->ascend()

            ->push(tag::div())
                ->addClass("gtmRowHeader")
                ->append($headerNames[2])
            ->ascend()

            ->push(tag::div())
                ->addClass("gtmRowHeader")
                ->append($headerNames[3])
            ->ascend()

            ->push(tag::div())
                ->addClass("gtmRowHeader")
                ->append($headerNames[4])
            ->ascend()

            ->push(tag::div())
                ->addClass("gtmRowHeader")
                ->setAttr("style", "width: 210px;")
                ->append($headerNames[5])
            ->ascend()
        ->ascend()
        ;

        return $meetingList;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    start meeting form view
    */
    public function startFormView($formData)
    {
        global $template;
        $template
            ->pushReadyFunc('
                $("#start").datetimepicker();
                $("#end").datetimepicker();
                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
                $("form").get(0).setAttribute("id", "frm");
                $("form").get(0).setAttribute("action", "'.$this->getPathX().'admin/startMeetingList");
            ');

        $this->searchFormScript();
        $html = tag::div();
        $html
            ->setID("pageContainer")
            ->push(tag::div())
                ->addClass("gtmDataContainers")
                ->push(tag::h1())
                    ->addClass("gtmSubtitle")
                    ->append("Search Meetings")
                ->ascend()
                ->push($this->searchForm($formData))
            ->ascend()
        ->ascend()
        ;
        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    start meeting form view
    */
    public function meetingsList($meetings, $action)
    { 
        $counter = 2;
        $meetingList = tag::div();
        foreach ($meetings as $meeting ) 
        {
            if(is_numeric($meeting["meetingid"]))
            {
                $class = ($counter%2 == 0) ? "gtmRowWhite" : "gtmRowNormal";
                
                if($action == ''){
                    $link = $meeting['participants'];
                }
                elseif($action == 'Delete')
                {
                    $link = tag::a();
                    $link
                        ->setAttr("href", "javascript:void(0)")
                        ->setAttr("onclick", "xajax_delete(".$meeting["meetingid"].")")
                        ->append($action)
                    ->ascend();
                }
                else
                {
                    $link = tag::a();
                    $link
                        ->setAttr("href", "javascript:void(0)")
                        ->setAttr("onclick", "action(".$meeting["meetingid"].")")
                        ->append($action)
                    ->ascend();
                }

                $meetingList
                    ->push(tag::div())
                        ->addClass('gtmRowHeaderContainer')
                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 250px;")
                            ->append($meeting['subject'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($link)
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($meeting['type'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($meeting['start'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->append($meeting['end'])
                        ->ascend()

                        ->push(tag::div())
                            ->addClass($class)
                            ->setAttr("style", "width: 210px;")
                            ->append($meeting['conference'])
                        ->ascend()
                    ->ascend();
                $counter++;
            }
        }
        return $meetingList;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    start meeting form view
    */
    public function startMeetingListView($data)
    {
        global $template;
        $template
            ->pushReadyFunc('
                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
                $("form").get(0).setAttribute("id", "frm");
                $("form").get(0).setAttribute("action", "'.$this->getPathX().'admin/startMeetingList");
            ');

        $msgBox = tag::div();
        $msgBox
            ->setID("gtmMsg")
            ->append($data['msg'])
        ->ascend();

        if(!isset($data['hostURL']) && is_numeric($data[0]["meetingid"]))
        {
            $headerNames = array(
                0 => 'Subject', 
                1 => 'Action', 
                2 => 'Type', 
                3 => 'Start', 
                4 => 'End', 
                5 => 'Conference Info', 
            );

            $listHeader = $this->meetingListHeader($headerNames);
            $listBody = $this->meetingsList($data, 'Start');
            $actionForm = $this->actionForm();
            $actionScript = $this->actionScript();
        }

        $html = tag::div();
        $html 
            ->append($msgBox)
            ->append($listHeader)
            ->append($listBody)
            ->append($actionForm)
            ->append($actionScript);

        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    start meeting form view
    */
    public function futureMeetingFormView($formData)
    {
        global $template;
        $template
            ->pushReadyFunc('
                $("#start").datetimepicker();
                $("#end").datetimepicker();
                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
                $("form").get(0).setAttribute("id", "frm");
                $("form").get(0).setAttribute("action", "'.$this->getPathX().'admin/futureMeetingList");
            ');

        $this->searchFormScript();
        $html = tag::div();
        $html
            ->setID("pageContainer")
            ->push(tag::div())
                ->addClass("gtmDataContainers")
                ->push(tag::h1())
                    ->addClass("gtmSubtitle")
                    ->append("Search Meetings")
                ->ascend()
                ->push($this->searchForm($formData,'searchMeetings'))
            ->ascend()
        ->ascend()
        ;
        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    start meeting form view
    */
    public function futureMeetingListView($data)
    {
        $msgBox = tag::div();
        $msgBox
            ->setID("gtmMsg")
            ->append($data['msg'])
        ->ascend();

        if(is_numeric($data[0]["meetingid"]))
        {
            $headerNames = array(
                0 => 'Subject', 
                1 => 'Max Participants', 
                2 => 'Type', 
                3 => 'Start', 
                4 => 'End', 
                5 => 'Conference Info', 
            );

            $listHeader = $this->meetingListHeader($headerNames);
            $listBody = $this->meetingsList($data, '');
        }

        $html = tag::div();
        $html 
            ->append($msgBox)
            ->append($listHeader)
            ->append($listBody)
            ->append($actionForm)
            ->append($actionScript);

        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    delete meeting form view
    */
    public function deleteMeetingFormView($formData)
    {
        global $template;
        $template
            ->pushReadyFunc('
                $("#start").datetimepicker();
                $("#end").datetimepicker();
                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
                $("form").get(0).setAttribute("id", "frm");
                $("form").get(0).setAttribute("action", "'.$this->getPathX().'admin/deleteMeetingList");
            ');

        $this->searchFormScript();
        $html = tag::div();
        $html
            ->setID("pageContainer")
            ->push(tag::div())
                ->addClass("gtmDataContainers")
                ->push(tag::h1())
                    ->addClass("gtmSubtitle")
                    ->append("Search Meetings")
                ->ascend()
                ->push($this->searchForm($formData))
            ->ascend()
        ->ascend()
        ;
        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    start meeting form view
    */
    public function deleteMeetingListView($data)
    {

        global $template;
        $template
            ->pushReadyFunc('
                $("#start").val("'.$_POST["start"].'");
                $("#end").val("'.$_POST["end"].'");

                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
                $("form").get(0).setAttribute("id", "frm");
                $("form").get(0).setAttribute("action", "'.$this->getPathX().'admin/deleteMeetingList");
            ');

        $msgBox = tag::div();
        $msgBox
            ->setID("gtmMsg")
            ->append($data['msg'])
        ->ascend();

        if(is_numeric($data[0]["meetingid"]))
        {
            $headerNames = array(
                0 => 'Subject', 
                1 => 'Action', 
                2 => 'Type', 
                3 => 'Start', 
                4 => 'End', 
                5 => 'Conference Info', 
            );

            $listHeader = $this->meetingListHeader($headerNames);
            $listBody = $this->meetingsList($data, 'Delete');
            $actionForm = $this->actionForm();
            //$actionScript = $this->actionScript();
        }

        $html = tag::div();
        $html 
            ->append($msgBox)
            ->append($listHeader)
            ->append($listBody)
            ->append($actionForm)
            ->append($actionScript);

        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    delete meeting form view
    */
    public function updateMeetingFormView($formData)
    {
        global $template;
        $template
            ->pushReadyFunc('
                $("#start").datetimepicker();
                $("#end").datetimepicker();
                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
                $("form").get(0).setAttribute("id", "frm");
                $("form").get(0).setAttribute("action", "'.$this->getPathX().'admin/updateMeetingList");
            ');

        $this->searchFormScript();
        $html = tag::div();
        $html
            ->setID("pageContainer")
            ->push(tag::div())
                ->addClass("gtmDataContainers")
                ->push(tag::h1())
                    ->addClass("gtmSubtitle")
                    ->append("Search Meetings")
                ->ascend()
                ->push($this->searchForm($formData))
            ->ascend()
        ->ascend()
        ;
        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    start meeting form view
    */
    public function updateMeetingListView($data)
    {
        global $template;
        $template
            ->pushReadyFunc('

                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
                $("form").get(0).setAttribute("id", "frm");
                $("form").get(0).setAttribute("action", "'.$this->getPathX().'admin/updateMeetingForm");
            ');

        $msgBox = tag::div();
        $msgBox
            ->setID("gtmMsg")
            ->append($data['msg'])
        ->ascend();

        if(is_numeric($data[0]["meetingid"]))
        {
            $headerNames = array(
                0 => 'Subject', 
                1 => 'Action', 
                2 => 'Type', 
                3 => 'Start', 
                4 => 'End', 
                5 => 'Conference Info', 
            );

            $listHeader = $this->meetingListHeader($headerNames);
            $listBody = $this->meetingsList($data, 'update');
            $actionForm = $this->actionForm();
            $actionScript = $this->actionScript();
        }

        $html = tag::div();
        $html 
            ->append($msgBox)
            ->append($listHeader)
            ->append($listBody)
            ->append($actionForm)
            ->append($actionScript);

        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    create meeting form page's view
    */
    public function updateFormView($formData)
    {
        global $template;
        $template
            ->pushReadyFunc('
                $("#start").datetimepicker();
                $("#end").datetimepicker();
                $("#meetId").val("'.$formData[0]["meetingId"].'");
                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
                $("form").get(0).setAttribute("id", "frm");
                $("form").get(0).setAttribute("action", "'.$this->getPathX().'admin/updatedMeetingInfo");
            ');
        $this->createFormScript();
        $html = tag::div();
        $html
            ->setID("pageContainer")
            ->push(tag::div())
                ->addClass("gtmDataContainers")
                ->push(tag::h1())
                    ->addClass("gtmSubtitle")
                    ->append("update Meetings")
                ->ascend()
                ->push($this->createForm($formData[0]))
            ->ascend()
        ->ascend()
        ;
        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    start meeting form view
    */
    public function updatedMeetingInfoView($data)
    {

        $msgBox = tag::div();
        $msgBox
            ->setID("gtmMsg")
            ->append($data['msg'])
        ->ascend();

        $html = tag::div();
        $html 
            ->append($msgBox);

        return $html;
    }

}

?>