<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingAjax extends AppAjax
{
    public function delete($mid) 
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;

        $meetingData = array('mid' => $mid);

        $data = $logic->deleteMeetingLogic($meetingData);

        if($data['error'] == 1){
            $xajax->alert($data["message"],$data["title"]);
        }
        else{
            $xajax->alert($data["message"],$data["title"]);
            $xajax->script('$("#frm").submit()');   
        }
        return $xajax;
    } 
}

?>