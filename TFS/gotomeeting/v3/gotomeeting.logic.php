<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingLogic extends DashboardAppLogic
{
    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    get token
    */
    public function getToken()
    {
        $tmpData = $this->getRecord("Token",array("email"=>'talat@venturehealth.com'),600);
        $userData = array();
        if($tmpData["access_token"] != null && $tmpData["organizer_key"] != null)
        {
            $userData["access_token"] = $tmpData["access_token"];
            $userData["organizer_key"] = $tmpData["organizer_key"];
        }
        return $userData;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    update token 
    */
    public function tokenUpdate($setArr){
        $this->doAppUpdate("Token", $setArr, array("email"=>'talat@venturehealth.com'));
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    landing Page Logic
    */
    public function landingPageLogic()
    {
        $key  = $this->getConfig("key");
        $url  = $this->getConfig("oauthUrl");
        $isAuthorize = $this->getNumericVar('authorize',0);
        $code = null;
        $code = $this->getVar('code');

        if($isAuthorize == 1){
            $isAuthorize = 1;
        }
        else{
            $isAuthorize = null;
        }

        if($code != null)
        {
            $arrTemp = explode("?code=", $code);
            $codeString = $arrTemp[1];
        }
        else{
            $codeString = null;
        }

        $citrix = new CitrixAPI();
        $oauth = $citrix->getOAuthToken($key, $url, $isAuthorize, $codeString);

        if($oauth != null)
        {
            $oauthData = json_decode($oauth, true);
            $tokenData = array();
            $tokenData["fname"] = $oauthData["firstName"];
            $tokenData["lname"] = $oauthData["lastName"];
            $tokenData["email"] = $oauthData["email"];
            $tokenData["access_token"] = $oauthData["access_token"];
            $tokenData["organizer_key"] = $oauthData["organizer_key"];
            $tokenData["account_key"] = $oauthData["account_key"];
            $tokenData["account_type"] = $oauthData["account_type"];
            $tokenData["issue_time"] = date("Y-m-d");
            $tokenData["expires_in"] = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 355 day"));

            if($tokenData["access_token"] != null)
            {
               $update = $this->tokenUpdate($tokenData);
               $msg = 'Token Updated sucess fully.';
            }
        }
        return $msg;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    meetingCreated Page Logic
    */
    public function meetingCreatedPageLogic()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {
            $user = $this->getToken();
            $startDateTime = new DateTime($_POST['start']);
            $_POST['start'] = $startDateTime->format('c');
            $endDateTime = new DateTime($_POST['end']);
            $_POST['end'] = $endDateTime->format('c');

            if(isset($user['access_token']))
            {
                $citrix = new CitrixAPI($user['access_token'], $user['organizer_key']);
                $result = $citrix->createMeeting($_POST["subject"], $_POST["start"], $_POST["end"], $_POST["conference"], $_POST["meeting"]);
                $data = json_decode($result,true);
                if($data){
                    $data['msg']  = 'Meeting Created Sucessfully';
                }
                else{
                    $data['msg'] = 'Something went wrong please try again';
                }
            } 
            else{
                $data['msg'] = 'Please update token first';
            }
        }
        else{
            $data['msg'] = 'Fill form in order to create meeting.';
        }
        
        return $data;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    startMeetingList Page Logic
    */
    public function startMeetingListPageLogic()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {                
            $user = $this->getToken();
            $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
            $startDateTime = new DateTime($_POST['start']);
            $start = $startDateTime->format('c');
            $endDateTime = new DateTime($_POST['end']);
            $end = $endDateTime->format('c');

            if(isset($user['access_token']))
            {
                if(isset($_POST['mid']))
                {
                    $result = $citrix->startMeeting($_POST['mid']);
                    $start = json_decode($result, true);
                    if($start["hostURL"]){
                        $searches['msg'] = 'In order to start meeting <a href="'.$start["hostURL"].'">click here</a> ';
                        $searches['hostURL'] = $start["hostURL"];
                    }
                }
                else
                {
                    $result = $citrix->GetFutuerMeeting($start, $end);
                    $data = json_decode($result, true);
                    if(!$data){
                        $searches['msg'] = 'No meetings found';
                    }
                }
            } 
            else{
                $searches['msg'] = 'Please update token first';
            }
        }
        else{
            $searches['msg'] = 'Fill form properly in order to search meetings.';
        }

        if($data)
        {
            $count = 0;

            foreach ($data as $info) 
            {
                $invalid = 0;

                if( strtotime($info["startTime"]) <= strtotime($_POST['start']) ){
                    $invalid = 1;
                }

                if( strtotime($info["startTime"]) > strtotime($_POST['end']) ){
                    $invalid = 1;
                }

                if($invalid == 0)
                {
                    $time = strtotime($info["startTime"]);
                    $StartedAt = date('Y-m-d H:i',$time);
                    $time = strtotime($info["endTime"]);
                    $EndedAt = date('Y-m-d H:i',$time);
                    $searches[$count]['subject'] = $info['subject'];
                    $searches[$count]['participants'] = $info['maxParticipants'];
                    $searches[$count]["meetingid"] = $info["meetingid"];
                    $searches[$count]['type'] = $info["meetingType"];
                    $searches[$count]['start'] = $StartedAt;
                    $searches[$count]['end'] = $EndedAt;
                    $searches[$count]['conference'] = $info["conferenceCallInfo"];

                    $count++;
                }
            }

            if($searches == null)
            {
                $searches['msg'] = "No meetings found between ".$_POST['start']." and ".$_POST['end'];
            }
        }
        return $searches;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    futureMeetingList Page Logic
    */
    public function futureMeetingListPageLogic()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {    
            $user = $this->getToken();
            $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
            $startDateTime = new DateTime($_POST['start']);
            $start = $startDateTime->format('c');
            $endDateTime = new DateTime($_POST['end']);
            $end = $endDateTime->format('c');

            if(isset($user['access_token']) )
            {
                $result = $citrix->getFutuerMeeting($start, $end );
                $data = json_decode($result, true);

                if(!$data){
                    $searches['msg'] = 'Something went wrong please try again';
                }
            } 
            else{
                $searches['msg'] = 'Please update token first';
            }
        }
        else{
            $searches['msg'] = 'Fill form properly in order to search meetings.';
        }

        if($data)
        {
            $count = 0;
            foreach ($data as $info) 
            {
                $invalid = 0;

                if( strtotime($info["startTime"]) <= strtotime($_POST['start'])){
                    $invalid = 1;
                }

                if( strtotime($info["startTime"]) > strtotime($_POST['end']) ){
                    $invalid = 1;
                }

                if($invalid == 0)
                {
                    $time = strtotime($info["startTime"]);
                    $StartedAt = date('Y-m-d H:i',$time);
                    $time = strtotime($info["endTime"]);
                    $EndedAt = date('Y-m-d H:i',$time);
                    $searches[$count]['subject'] = $info['subject'];
                    $searches[$count]['participants'] = $info['maxParticipants'];
                    $searches[$count]["meetingid"] = $info["meetingid"];
                    $searches[$count]['type'] = $info["meetingType"];
                    $searches[$count]['start'] = $StartedAt;
                    $searches[$count]['end'] = $EndedAt;
                    $searches[$count]['conference'] = $info["conferenceCallInfo"];

                    $count++;
                }
            }

            if($searches == null){
                $searches['msg'] = "No meetings found between ".$_POST['start']." and ".$_POST['end'];
            }
        }
        return $searches;
    }



    public function deleteMeetingLogic($meetingData)
    {
        if(isset($meetingData['mid']))
        {
            $user = $this->getToken();
            $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
            $result = $citrix->deleteMeeting($meetingData['mid']);

            if($result == 303 || $result == 204 )
            {
                $data['error'] = 0;
                $data['title'] = 'Sucess';
                $data['message'] = 'Meeting has been deleted.';
            }
            else
            {
                $data['error'] = 1;
                $data['title'] = 'Error';
            	$data['message'] = 'Unable to delete please try again.';
            }
        }
        else
        {
            $data['error'] = 1;
            $data['title'] = 'Error';
            $data['message'] = 'Unable to delete please try again.';
        }
        return $data;
    }



    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    deleteMeetingList Page Logic
    */
    public function deleteMeetingListPageLogic()
    {
               
        $user = $this->getToken();
        /* Get All Meetings */
        if( isset($user['access_token']) )
        {
            $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
            $startDateTime = new DateTime($_POST['start']);
            $start = $startDateTime->format('c');
            $endDateTime = new DateTime($_POST['end']);
            $end = $endDateTime->format('c');

            /*
            if(isset($data['mid']) )
            {
                $result = $citrix->deleteMeeting($data['mid']);

                if($result == 204 || $result == '204' )
                {
                    $searches['msg'] = 'Success: Meeting has been deleted';
                    $data['title'] = 'Sucess';
                    $data['message'] = 'Meeting has been deleted.';
                }
                else
                {
                    $searches['msg'] = '';
                    $data['title'] = 'Error';
                    $data['message'] = 'Unable to delete please try again.';
                }
                return $data;
            }
            */

            $result = $citrix->getFutuerMeeting($start, $end );
            $data = json_decode($result, true);

            if(!$data){
                $searches['msg'] = 'No Meetings Found';
            }
        } 
        else{
            $searches['msg'] = 'Please update token first';
        }
        /* Sorting meetings according to date time range */
        if($data)
        {
            $count = 0;

            foreach ($data as $info) 
            {
                $invalid = 0;

                if( strtotime($info["startTime"]) <= strtotime($_POST['start']) ){
                    $invalid = 1;
                }

                if( strtotime($info["startTime"]) > strtotime($_POST['end']) ){
                    $invalid = 1;
                }

                if($invalid == 0)
                {
                    $time = strtotime($info["startTime"]);
                    $StartedAt = date('Y-m-d H:i',$time);
                    $time = strtotime($info["endTime"]);
                    $EndedAt = date('Y-m-d H:i',$time);
                    $searches[$count]['subject'] = $info['subject'];
                    $searches[$count]['participants'] = $info['maxParticipants'];
                    $searches[$count]["meetingid"] = $info["meetingid"];
                    $searches[$count]['type'] = $info["meetingType"];
                    $searches[$count]['start'] = $StartedAt;
                    $searches[$count]['end'] = $EndedAt;
                    $searches[$count]['conference'] = $info["conferenceCallInfo"];

                    $count++;
                }
            }

            if($searches == null){
                $searches['msg'] = "No meetings found between ".$_POST['start']." and ".$_POST['end'];
            }
        }
        //$searches['startigDate'] = $_POST['start'];
        //$searches['startigDate'] = $_POST['end'];
        return $searches;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    updateMeetingList Page Logic
    */
    public function updateMeetingListPageLogic()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {                
            $user = $this->getToken();

            if( isset($user['access_token']) )
            {
                $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
                $startDateTime = new DateTime($_POST['start']);
                $start = $startDateTime->format('c');
                $endDateTime = new DateTime($_POST['end']);
                $end = $endDateTime->format('c');
                $result = $citrix->GetFutuerMeeting($start, $end );
                $data = json_decode($result, true);

                if(!$data){
                    $searches['msg'] = 'No Meeting Found';
                }
            } 
            else{
                $searches['msg'] = 'Please update token first';
            }
        }
        else{
            $searches['msg'] = 'Fill form properly in order to search meetings.';
        }

        if($data)
        {
            $count = 0;

            foreach ($data as $info) 
            {
                $invalid = 0;

                if( strtotime($info["startTime"]) <= strtotime($_POST['start'])){
                    $invalid = 1;
                }

                if( strtotime($info["startTime"]) > strtotime($_POST['end'])){
                    $invalid = 1;
                }

                if($invalid == 0)
                {
                    $time = strtotime($info["startTime"]);
                    $StartedAt = date('Y-m-d H:i',$time);
                    $time = strtotime($info["endTime"]);
                    $EndedAt = date('Y-m-d H:i',$time);
                    $searches[$count]['subject'] = $info['subject'];
                    $searches[$count]['participants'] = $info['maxParticipants'];
                    $searches[$count]["meetingid"] = $info["meetingid"];
                    $searches[$count]['type'] = $info["meetingType"];
                    $searches[$count]['start'] = $StartedAt;
                    $searches[$count]['end'] = $EndedAt;
                    $searches[$count]['conference'] = $info["conferenceCallInfo"];

                    $count++;
                }
            }

            if($searches == null){
                $searches['msg'] = "No meetings found between ".$_POST['start']." and ".$_POST['end'];
            }
        }
        return $searches;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    updateMeeting Form Logic
    */
    public function updateMeetingFormLogic()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {                
            $user = $this->getToken();

            if($user['access_token'])
            {
                $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
                $result = $citrix->getMeeting($_POST['mid']);
                $data = json_decode($result, true);

                if(!$data){
                    $data['msg'] = 'Something went wrong please try again';
                }
            }
            else{
                $data['msg'] = 'Please update token first';
            }
        }
        else{
            $data['msg'] = 'No meeinting selected for update';
        }

        return $data;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    updatedMeetingInfo Page Logic
    */
    public function updatedMeetingInfoPageLogic()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') 
        {                
            $user = $this->getToken();
            if($user['access_token'])
            {
                $startDateTime = new DateTime($_POST['start']);
                $start = $startDateTime->format('c');
                $endDateTime = new DateTime($_POST['end']);
                $end = $endDateTime->format('c');
                $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);
                $result = $citrix->updateMeeting($_POST["meetId"], $_POST["subject"], $start, $end, $_POST["conference"], $_POST["meeting"]);

                if($result == 204){
                    $data['msg'] = 'Congratulations! meeting has been updated';
                }
                else{
                    $data['msg'] = 'Unable to update meeting now please try again';
                }
            }
            else{
                $data['msg'] = 'Please update token first';
            }
        }
        else{
            $data['msg'] = 'No meeinting selected for update';
        }
        return $data;
    }

}

?>