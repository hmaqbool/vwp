<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingDB 
{
	/**
    * @author:  Hashim Maqbool
    * @date:    Friday, October 11, 2013
    * @desc:    Token Storage Method
    */
	public static $Token = array(
		"tableName"=>"tokens",
		"schema"=>array(
			"id" =>array("type"=>"int(11)","null"=>"NO","autoIncrement"=>"auto_increment",),
			"fname" =>array("type"=>"varchar(255)","null"=>"NO",),
			"lname" =>array("type"=>"varchar(255)","null"=>"NO",),
			"email" =>array("type"=>"varchar(255)", "null"=>"NO",),
			"access_token" =>array("type"=>"varchar(255)","null"=>"NO",),
			"organizer_key" =>array("type"=>"varchar(255)","null"=>"NO",),
			"account_key" =>array("type"=>"varchar(255)","null"=>"NO",),
			"account_type" =>array("type"=>"varchar(255)","null"=>"NO",),
			"issue_time" =>array("type"=>"varchar(255)","null"=>"NO",),
			"expires_in" =>array("type"=>"varchar(255)","null"=>"NO",)
		),
		"minfields"=>array(),
		"uniquefields"=>array(),
		"singular"=>"Token",
		"primary"=>"id",
		"keys"=>"",
	);
}

?>