<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingLogic extends DashboardAppLogic
{

	// Contact CRUD Methods
    public function addContact($arr)
    {
        return $this->_addTable("Contact",$arr);
    }
    public function updateContact($arr)
    {
        return $this->_updateTable("Contact",$arr);
    }
    public function deleteContact($arr)
    {
        return $this->_deleteContact("Contact",$arr);
    }
    public function getContactByID($id)
    {
        return $this->_getTableByID("Contact",$id);
    }
    public function getAllContact()
    {
        return $this->_getAllTable("Contact");
    }
    public function getPagedContact($page,$length=10)
    {
        return $this->_getPagedTable("Contact",$page,$length);
    }
    public function getIndexedContact()
    {
        return $this->_getIndexedTable("Contact");
    }

    public function addregister($arr)
    {
        $id = $this->_addTable("Register",$arr);
    }

    public function getToken()
    {
        $tmp_data = $this->getRecord("Token",array("email"=>'talat@venturehealth.com'),600);
        $user_data = array();
        if($tmp_data["access_token"] != null && $tmp_data["organizer_key"] != null)
        {
            $user_data["access_token"] = $tmp_data["access_token"];
            $user_data["organizer_key"] = $tmp_data["organizer_key"];
        }
        return $user_data;
    }

    public function tokenUpdate($setArr)
    {
        $this->doAppUpdate("Token",$setArr,array("email"=>'talat@venturehealth.com'));
    }

    public function formProcess($arr)
    {

        if(Helper::isEmptyString($arr["fname"]))
        {
            Form::setFormErrs("fname", "* Field not entered");
        }

        if(Helper::isEmptyString($arr["lname"]))
        {
            Form::setFormErrs("lname", "* Field not entered");
        }

        if(Helper::isEmptyString($arr["Email"]))
        {
            Form::setFormErrs("Email", "* Field not entered");
        }

        if(Form::getNumFormErrs() > 0) // checks collated errors
        {
            return Error::newSetError("ERROR_CODE", "Form Error", "Empty Fields are not allowed" ); 
        }

        //return $this->doInsertGetID($arr);
        //$id = $this->_addTable("Register",$arr);
        //return true;

        $display = $this->app->getDisplayObj();

        var_dump($display);
        die();
        //$display->regDisplay("created2", $arr);
    }

    




}

?>