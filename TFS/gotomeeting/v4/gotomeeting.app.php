<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingApp extends GenericApp
{
    protected  $class_logic = "GoToMeetingLogic";
    protected  $class_display = "GoToMeetingDisplay";
    protected  $class_ajax = "GoToMeetingAjax";
    protected  $class_db = "GoToMeetingDB";
    protected  $class_css = "GoToMeetingCSS";
    protected  $class_page = "GoToMeetingPage";

    protected static $appInstance;

    public static $concern = "gotomeeting";
    public static $name = "Go To Meeting";
    public static $icon = "http://global.imranmedia.com/images/agg-icons/gig/affiliate_sales.png";

    public function __construct($db,$path,$vars=array())
    {
        parent::__construct($db,$path,$vars);
    }

    public static function getObj($db,$path,$arr=array())
    {
        if (!isset(self::$appInstance))
        {
            $c = __CLASS__;
            self::$appInstance = new $c($db,$path,$arr);
        }
        return self::$appInstance;
    }

    public function __varInit($arr=array())
    {
        return parent::__varInit( array_merge(
            array(
                'key' => '4075af03b3297462695654a370f02ffc',
                'oauthUrl' => 'http://hashimadmin.sporefanz.com/gotomeeting/admin/code_',
            ),$arr) );
    }
    
    public function getCrons()
    {
        //$this->crons[] = array("frequency"=>86400,"app"=>"Hashim","callback"=>"logicMethod","firstrun"=>0);
        return $this->crons;
    }
}

?>