<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */
class GoToMeetingPage extends DashboardAppPage
{
    /**
    * @author:  M. Hashim
    * @date:    Friday, October 11, 2013
    * @desc:    Landing page of application
    */
    public function admin()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingSimpleTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $data = $logic->landingPageLogic();
        $this->regDisplay("homeView", $data);
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 14, 2013
    * @desc:    Create meeting Form page of application
    */
    public function admin_createMeeting()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regAjax("create");
        $this->doAjax();
        $this->regDisplay("createFormView");
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    Meeting created information
    */
    public function admin_meetingCreated()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingTemplate.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $data = $logic->meetingCreatedPageLogic();
        $this->regDisplay("meetingCreatedView",$data);
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    Start meeting Form page of application
    */
    public function admin_startMeeting()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regAjax("search");
        $this->doAjax();
        $this->regDisplay("startFormView");
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, October 15, 2013
    * @desc:    Start meeting list
    */
    public function admin_startMeetingList()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingSimpleTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $data = $logic->startMeetingListPageLogic();

        $this->regAjax("action");
        $this->doAjax();
        $this->regDisplay("startMeetingListView", $data);
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Search meeting Form page of application
    */
    public function admin_getFutureMeetings()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regAjax("search");
        $this->doAjax();
        $this->regDisplay("futureMeetingFormView");   
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Meetings list
    */
    public function admin_futureMeetingList()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingSimpleTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $data = $logic->futureMeetingListPageLogic();
        $this->regDisplay("futureMeetingListView", $data);
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Delete meeting search Form
    */
    public function admin_deleteMeeting()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regAjax("search");
        $this->doAjax();
        $this->regDisplay("deleteMeetingFormView");
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Delete meeting list
    */
    public function admin_deleteMeetingList()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingSimpleTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $data = $logic->deleteMeetingListPageLogic();
        $this->regAjax("delete");
        $this->doAjax();
        $this->regDisplay("deleteMeetingListView", $data);
    }


    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Update meeting search Form
    */
    public function admin_updateMeeting()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regAjax("search");
        $this->doAjax();
        $this->regDisplay("updateMeetingFormView");
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Update meeting list
    */
    public function admin_updateMeetingList()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingSimpleTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $data = $logic->updateMeetingListPageLogic();
        $this->regAjax("action");
        $this->doAjax();
        $this->regDisplay("updateMeetingListView", $data);
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Update meeting form
    */
    public function admin_updateMeetingForm()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $data = $logic->updateMeetingFormLogic();
        $this->regAjax("create");
        $this->doAjax();
        $this->regDisplay("updateFormView",$data);
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Updated meeting information
    */
    public function admin_updatedMeetingInfo()
    {
        $sc = SiteConfig::getObj();
        $sc->template("goToMeetingSimpleTemplate.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $data = $logic->updatedMeetingInfoPageLogic();
        $this->regDisplay("updatedMeetingInfoView",$data);
    }

}
?>