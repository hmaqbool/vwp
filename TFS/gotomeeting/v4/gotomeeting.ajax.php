<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingAjax extends AppAjax
{
    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Delete meeting
    */
    public function delete($mid) 
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;

        $meetingData = array('mid' => $mid);

        $data = $logic->deleteMeetingLogic($meetingData);

        if($data['error'] == 1){
            $xajax->alert($data["message"],$data["title"]);
        }
        else{
            $xajax->alert($data["message"],$data["title"]);
            $xajax->script('$("#frm").submit()');   
        }
        return $xajax;
    } 

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Create/Update meeting
    */
    public function create() 
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;
        $xajax->script('$("#frm").submit()');   
        return $xajax;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Search meeting
    */
    public function search() 
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;
        $xajax->script('$("#frm").submit()');   
        return $xajax;
    }

    /**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    Form submition
    */
    public function action() 
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;
        $xajax->script('$("#frm").submit()');   
        return $xajax;
    }

}

?>