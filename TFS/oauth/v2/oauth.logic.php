<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 * 
 * 
 * Todo: Common class should be a singalton class.
 * 
 */

class OAuthLogic extends DashboardAppLogic
{

    // Contact CRUD Methods
    public function addContact($arr)
    {
        return $this->_addTable("Contact",$arr);
    }
    
    public function updateContact($arr)
    {
        return $this->_updateTable("Contact",$arr);
    }
    
    public function deleteContact($arr)
    {
        return $this->_deleteContact("Contact",$arr);
    }
    
    public function getContactByID($id)
    {
        return $this->_getTableByID("Contact",$id);
    }
    
    public function getAllContact()
    {
        return $this->_getAllTable("Contact");
    }
    
    public function getPagedContact($page,$length=10)
    {
        return $this->_getPagedTable("Contact",$page,$length);
    }
    
    public function getIndexedContact()
    {
        return $this->_getIndexedTable("Contact");
    }

    public function authenticate()
    {
        try
        {
            $arrResult = array();
            $objCommon = Common::getObj();
            $accessToken = $objCommon->getAccessToken();

            // if token not found or expired.
            if ($accessToken === false)
            {
                /* no valid access token available, go to authorization server */
                header("HTTP/1.1 302 Found");
                header("Location: " . $objCommon->getAuthorizeUri());
                exit;
            }
            
            $arrResult['status'] = true;
            $arrResult['title'] = 'Success';
            $arrResult['message'] = 'Successfuly authenticated from OAuth server';
        }
        //catch (Exception\BearerErrorResponseException $e)
        catch (Exception $e)
        {
            //if ("invalid_token" === $e->getBearerReason())
            //{
                // the token we used was invalid, possibly revoked, we throw it away
            $objCommon->deleteAccessToken();
            $objCommon->deleteRefreshToken();

            /* no valid access token available, go to authorization server */
            header("HTTP/1.1 302 Found");
            header("Location: " . $objCommon->getAuthorizeUri());
            exit;
            //}

            //$arrResult['status'] = false;
            //$arrResult['message'] = 'There are some errors in processing your request, please try again later or contact your systems administrator.';
        }
        //catch (\Exception $e)
        catch (Exception $e)
        {
            $arrResult['status'] = false;
            $arrResult['title'] = 'Error';
            $arrResult['message'] = 'There are some errors in processing your request, please try again later or contact your systems administrator.';
        }
        
        return $arrResult;
    }
    
    public function callback()
    {
        try 
        {
            $objCommon = Common::getObj();
            
            $callback = $objCommon->getCallback();
            $callback->handleCallback($_GET);

            header("HTTP/1.1 302 Found");
            header("Location: http://hashimadmin.sporefanz.com/oauth/admin/authenticate");
            exit;
        } 
        //catch (Client\AuthorizeException $e) 
        catch (Exception $e) 
        {
            // this exception is thrown by Callback when the OAuth server returns a
            // specific error message for the client, e.g.: the user did not authorize
            // the request
            //die(sprintf("ERROR: %s, DESCRIPTION: %s", $e->getMessage(), $e->getDescription()));
        } 
        //catch (\Exception $e) 
        catch (Exception $e) 
        {
            // other error, these should never occur in the normal flow
            //die(sprintf("ERROR: %s", $e->getMessage()));
        }
    }
    
    public function workspaces()
    {
        $arrResult = array();
        $objCommon = Common::getObj();
        $client = $objCommon->getHttpClient();
        
        //if ($client == null)
        if (true)
        {
            $arrResult['status'] = false;
            $arrResult['title'] = 'Error';
            $arrResult['message'] = 'Unable to authenticate, your access token might has expired.';
            $arrResult['data'] = null;
            
            return $arrResult;
        }
        
        $apiUri = $this->getConfig('workspaceUri');
        $response = $client->get($apiUri)->send();
        $arrWorkspaces = $response->json();
        
        $arrResult['status'] = true;
        $arrResult['title'] = 'Success';
        $arrResult['message'] = 'Successfuly access workspaces';
        $arrResult['data'] = $arrWorkspaces;
        
        return $arrResult;
    }
    
    public function deleteWorkspace($workspaceId)
    {
        $arrResult = array();
        
        try
        {
            $apiUri = str_replace('{WORKSPACE_ID}', $workspaceId, $this->getConfig('deleteWorkspaceUri'));
            $objCommon = Common::getObj();
            $client = $objCommon->getHttpClient();

            $request = $client->delete($apiUri);
            $request->getParams()->set('redirect.disable', true);
            $request->send();
            
            $arrResult['status'] = true;
            $arrResult['title'] = 'Success';
            $arrResult['message'] = "Workspace successfuly deleted.";
            
        }
        catch (Exception $e)
        {
            $arrResult['status'] = false;
            $arrResult['title'] = 'Error';
            $arrResult['message'] = "There are some errors in processing your request, please try again later or contact your system's administrator.";
        }
        
        return $arrResult;
        
    }
    
    public function createWorkspace($newWorkspace)
    {
        $arrResult = array();

        try 
        {
            $apiUri = $this->getConfig('workspaceUri');
            
            $objCommon = Common::getObj();
            $client = $objCommon->getHttpClient();

            $response = $client->post($apiUri)->addPostFields(array(
                        'workspace' => array('name' => $newWorkspace),
                    ))->send();

            $arrResult['status'] = true;
            $arrResult['title'] = 'Success';
            $arrResult['message'] = "Workspace successfuly created.";
        }
        catch (Exception $e)
        {
            $arrResult['status'] = false;
            $arrResult['title'] = 'Error';
            $arrResult['message'] = "There are some errors in processing your request, please try again later or contact your system's administrator.";
        }
        
        return $arrResult;
    }
    
    public function workspaceResources($workspaceId, $folderId)
    {
        if ($folderId > 0) {
            $apiUri = str_replace(array('{WORKSPACE_ID}', '{FOLDER_ID}'), array($workspaceId, $folderId), $this->getConfig('workspaceFolderUri'));
        }
        else {
            $apiUri = str_replace('{WORKSPACE_ID}', $workspaceId, $this->getConfig('workspaceFileUri'));
        }
        
        $objCommon = Common::getObj();
        $client = $objCommon->getHttpClient();
        $response = $client->get($apiUri)->send();
        return $response->json();
    }
    
    public function updateResource($id, $name, $type)
    {
        $arrResult = array();
        
        if ($id <= 0 || empty($name))
        {
            $arrResult['status'] = false;
            $arrResult['title'] = 'Error';
            $arrResult['message'] = "Some required parameters are missing.";
            
            return $arrResult;
        }

        try
        {
            $apiUri = str_replace(array('{RESOURCE_TYPE}', '{RESOURCE_ID}'), array($type, $id), $this->getConfig('updateResourceUri'));
            
            $objCommon = Common::getObj();
            $client = $objCommon->getHttpClient();

            $p[$type] = array("filename" => $name);
            $response = $client->put($apiUri)->addPostFields($p)->send();
            $arrResponse = $response->json();

            if ( count($arrResponse[$type]['errors']) > 0)
            {
                $arrResult['status'] = false;
                $arrResult['title'] = 'Error';
                $arrResult['message'] = "There are some errors in processing your request, please try again later or contact your system's administrator.";

                return $arrResult;
            }
            
            $arrResult['status'] = true;
            $arrResult['title'] = 'Success';
            $arrResult['message'] = "Resource successfuly updated.";
        }
        //catch (ClientErrorResponseException $e)
        catch (Exception $e)
        {
            $arrResult['status'] = false;
            $arrResult['title'] = 'Error';
            $arrResult['message'] = "There are some errors in processing your request, please try again later or contact your system's administrator.";
        }
        
        return $arrResult;
    }
    
    public function downloadFile($workspaceId, $fileId)
    {
        $apiUri = str_replace(array('{WORKSPACE_ID}', '{FILE_ID}'), array($workspaceId, $fileId), $this->getConfig('downloadFileUri'));

        $objCommon = Common::getObj();
        $client = $objCommon->getHttpClient();

        $request = $client->get($apiUri);
        $response = $request->send();

        header("Content-Type: {$response->getContentType()}");
        header("Content-Transfer-Encoding: {$response->getTransferEncoding()}");
        header("Cache-Control: {$response->getCacheControl()}",false); 
        header("Content-Disposition:  {$response->getContentDisposition()}" ); 
        header("Content-Length:  {$response->getContentLength()}" ); 

        echo $response->getBody();
    }
    
    public function deleteResource($resourceId, $type)
    {
        $arrResult = array();
        $apiUri = str_replace(array('{RESOURCE_TYPE}', '{RESOURCE_ID}'), array($type, $resourceId), $this->getConfig('deleteResourceUri'));

        try 
        {
            $objCommon = Common::getObj();
            $client = $objCommon->getHttpClient();

            $request = $client->delete($apiUri);
            $request->getParams()->set('redirect.disable', true);
            $request->send();

            $arrResult['status'] = true;
            $arrResult['title'] = 'Success';
            $arrResult['message'] = ucfirst($type) ." successfuly deleted.";
        }
        catch (Exception $e)
        {
            $arrResult['status'] = false;
            $arrResult['title'] = 'Error';
            $arrResult['message'] = "There is some errors in processing your request, please try again later or contact your system's administrator.";
        }

        return $arrResult;
    }
    
    public function createFolder($workspaceId, $folderId, $newFolder)
    {
        $arrResult = array();

        if ($folderId <= 0) {
            $apiUri = str_replace('{WORKSPACE_ID}', $workspaceId, $this->getConfig('createFolderUri'));
        }
        else {
            $apiUri = str_replace(array('{WORKSPACE_ID}', "{FOLDER_ID}"), array($workspaceId, $folderId), $this->getConfig('createFolderOfFolderUri'));
        }

        try 
        {
            $objCommon = Common::getObj();
            $client = $objCommon->getHttpClient();

            $request = $client->post($apiUri);
            $request->addPostFields(array(
                'folder' => array('filename' => $newFolder),
            ));

            $request->send();

            $arrResult['status'] = true;
            $arrResult['title'] = 'Success';
            $arrResult['message'] = "Folder successfuly created.";
        }
        catch (Exception $e)
        {
            $arrResult['status'] = false;
            $arrResult['title'] = 'Error';
            $arrResult['message'] = "There is some errors in processing your request, please try again later or contact your system's administrator.";
        }
        
        return $arrResult;
    }
    
    public function uploadFile()
    {
        $arrResult = array();
        
        try 
        {
            if ($_FILES["file"]["error"] > 0) {
                Form::setFormErrs("File", $_FILES["file"]["error"]);
            }

            if(Form::getNumFormErrs() > 0) {
                return Error::setError("FORM_ERRORS");  
            }

            //$path = $this->getConfig('uploadPath');
            //move_uploaded_file($_FILES["file"]["tmp_name"], $path . $_FILES["file"]["name"]); 

            if ($_POST['folderId'] > 0) {
                $apiUri = str_replace(array('{WORKSPACE_ID}', '{FOLDER_ID}'), array($_POST['workspaceId'], $_POST['folderId']), $this->getConfig('uploadFileInFolderUri'));
            }
            else {
                $apiUri = str_replace(array('{WORKSPACE_ID}', '{ROOT_FOLDER_ID}'), array($_POST['workspaceId'], $_POST['rootFolderId']), $this->getConfig('uploadFileInRootUri'));
            }

            //$filePath = "@$path{$_FILES["file"]["name"]}";
            //$filePath = $_FILES["file"]["tmp_name"];

            $objCommon = Common::getObj();
            $client = $objCommon->getHttpClient();

            $request = $client->post($apiUri, null, array(
                'file_field'   => $_FILES["file"]["tmp_name"]
            ));

            $request->send();
            
            $arrResult['status'] = true;
            $arrResult['title'] = 'Success';
            $arrResult['message'] = "File successfuly uploaded.";
        }
        catch (Exception $e)
        {
            $arrResult['status'] = false;
            $arrResult['title'] = 'Error';
            $arrResult['message'] = "There is some errors in processing your request, please try again later or contact your system's administrator.";
        }
        
        return $arrResult;
    }

    public function rolemaps()
    {
        $workspaceId = $this->getConfig('workspaceId');
        $folderId = $this->getConfig('parentfolder');
        $objCommon = Common::getObj();
        $client = $objCommon->getHttpClient();

        /* Deleting rolemap after getting parameters from url. */
        if (isset($role['userId']))
        {
            $apiUri = str_replace(array('{USER_ID}'), array($role['userId']), $this->getConfig('roleMapDelUri'));
            $request = $client->delete($apiUri);
            $request->addPostFields(array(
                $role['fileType'] => array('filename' => $role['fileName']),
            ));
            $request->getParams()->set('redirect.disable', true);
            $result = $request->send();
            if($result->getStatusCode() == 303)
            {
                $directory['error'] = 0;
                $directory['title'] = 'Success';
                $directory['message'] = "Folder/File successfuly deleted.";
            }
            else
            {
                $directory['error'] = 1;
                $directory['title'] = 'Error';
                $directory['message'] = "Unable to deleted Folder/File please try again later.";
            }
            return $directory;
        }

        /* Collecting data related to folders. */
        $apiUri = str_replace(array('{WORKSPACE_ID}', '{FOLDER_ID}'), array($workspaceId, $folderId), $this->getConfig('workspaceFolderUri'));
        $response = $client->get($apiUri)->send();
        $allFolders = $response->json();
        $fileCount = count($allFolders['items']);
        $folderIndex = 0;

        /* Iterating folders and collectiong rolemaps of each folder.*/
        while ($folderIndex < $fileCount )
        {
            if(isset($allFolders['items'][$folderIndex]['folder'])){
                $apiUri = str_replace(array('{FOLDER_ID}'), array($allFolders['items'][$folderIndex]['folder']['id']), $this->getConfig('roleMapFolderUri'));
            }
            else if(isset($allFolders['items'][$folderIndex]['file'])){
                $apiUri = str_replace(array('{FILE_ID}'), array($allFolders['items'][$folderIndex]['file']['id']), $this->getConfig('roleMapFileUri'));
            }
            else{
                break;
            }

            $response = $client->get($apiUri)->send();
            $foldersRolemap = $response->json();

            $roleMapIndex = 0;
            while($roleMapIndex < count($foldersRolemap['items']))
            {
                if(isset($allFolders['items'][$folderIndex]['folder']['filename']))
                {
                    $name = $allFolders['items'][$folderIndex]['folder']['filename'];
                    $directory[$folderIndex]['fileType'][$roleMapIndex] = 'Folder';
                }
                else
                {
                    $name = $allFolders['items'][$folderIndex]['file']['filename'];
                    $directory[$folderIndex]['fileType'][$roleMapIndex] = 'File';
                }

                $apiUri = str_replace(array('{USER_ID}'), array($foldersRolemap['items'][$roleMapIndex]['rolemap']['user_id']), $this->getConfig('userProfilesUri'));
                $response = $client->get($apiUri)->send();
                $userdata = $response->json();

                $userName = $userdata['user']['first_name'].' '.$userdata['user']['last_name'];
                $directory[$folderIndex]['fileName'][$roleMapIndex] = $name;
                $directory[$folderIndex]['roleMapId'][$roleMapIndex] = $foldersRolemap['items'][$roleMapIndex]['rolemap']['id'];
                $directory[$folderIndex]['roleName'][$roleMapIndex] = $foldersRolemap['items'][$roleMapIndex]['rolemap']['role_name'];
                $directory[$folderIndex]['userId'][$roleMapIndex] = $userName;
                $roleMapIndex++;
            }
            $folderIndex++;
        }
        return $directory;
    }

    public function users()
    {
        $workspaceId = $this->getConfig('workspaceId');
        $folderId = $this->getConfig('parentfolder');
        $objCommon = Common::getObj();
        $client = $objCommon->getHttpClient();
        $index = 0;
        $indexer = 0;

        $apiUri = str_replace(array('{WORKSPACE_ID}', '{FOLDER_ID}'), array($workspaceId, $folderId), $this->getConfig('workspaceFolderUri'));
        $response = $client->get($apiUri)->send();
        $allFolders = $response->json();

        foreach ($allFolders['items'] as $folder)
        {
            if(isset($folder['folder']))
            {
                if (!in_array($folder['folder']['user_id'], $users))
                {
                    $users[$index] = $folder['folder']['user_id'];    
                    $index++;
                }
            }
            else
            {
                if (!in_array($folder['file']['user_id'] , $users))
                {
                    $users[$index] = $folder['file']['user_id'];    
                    $index++;
                }   
            }
        }

        foreach ($users as $user)
        {
            $apiUri = str_replace(array('{USER_ID}'), array($users[$indexer]), $this->getConfig('userProfilesUri'));
            $response = $client->get($apiUri)->send();
            $profile[$indexer] = $response->json();

            $data[$indexer]['name'] = $profile[$indexer]['user']['first_name'].' '.$profile[$indexer]['user']['last_name'];
            $data[$indexer]['email'] = $profile[$indexer]['user']['email'];
            $indexer++;
        }
        return $data;
    }

    public function events()
    {
        $objCommon = Common::getObj();
        $client = $objCommon->getHttpClient();

        $apiUri = $this->getConfig('activityUri');
        $response = $client->get($apiUri)->send();
        $activitites = $response->json();
        return $activitites['items'];
    }

    public function invitations()
    {
        
        $workspaceId = $this->getConfig('workspaceId');
        $folderId = $this->getConfig('parentfolder');
        $objCommon = Common::getObj();
        $client = $objCommon->getHttpClient();

        try
        {
            $client->getEventDispatcher()->addListener('request.error', function(\Guzzle\Common\Event $event) 
            {
                if ($event['response']->getStatusCode() == 422){
                    throw new ErrorException("Unable to assign role either email incorrect or already assigned with some role.");
                }
            });

            /* invite user */
            if (isset($_POST['email']) && isset($_POST['fileId']))
            {
                $id = $_POST['fileId'];
                if($_POST['fileType'] == 'folder'){
                    $apiUri = str_replace(array('{ID}'), array($id), $this->getConfig('invitationsFolderUri'));
                }
                else{
                    $apiUri = str_replace(array('{ID}'), array($id), $this->getConfig('invitationsFileUri'));
                }

                $request = $client->post($apiUri);
                $request->addPostFields(array(
                    'invitation' => array(
                        'emails' => $_POST["email"],
                        'role_name' => $_POST["role"]
                        ),
                    )
                );      
                $inviteResponse = $request->send();

                if($inviteResponse->getStatusCode() == 201)
                {
                    $tmpData['title'] = 'Sucess';
                    $tmpData['message'] = 'Invitation to specific folder sent.';
                }
            }
            $apiUri = $apiUri = str_replace(array('{WORKSPACE_ID}', '{FOLDER_ID}'), array($workspaceId, $folderId), $this->getConfig('workspaceFolderUri'));
            $response = $client->get($apiUri)->send();
            $allFolders = $response->json();
            $data = $allFolders["items"];
            
            if($tmpData['title'])
            {
                $data['title'] = $tmpData['title'];
                $data['message'] = $tmpData['message'];
            }
            return $data;
        }
        catch(ErrorException $e)
        {
            $data['title'] = 'Error';
            $data['message'] = $e->getMessage();
            return $data;
        }
    }
}

?>

