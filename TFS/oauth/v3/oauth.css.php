<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class OAuthCSS extends AppCSS
{
    public function addCSS()
    {
            parent::addCSS();
            header('Content-type: text/css');
            $css =" 
                        .oauthRowWhite
                        {
                              float: left; 
                              width: 150px; 
                              height: 35px; 
                              padding-top: 15px; 
                              text-align: center;
                              background-color: white;
                        }

                        .oauthRowNormal
                        {
                              float: left; 
                              width: 150px; 
                              height: 35px; 
                              padding-top: 15px; 
                              text-align: center;
                              background-color: #D4E9FC;
                        }

                        .oauthRowHeaderContainer
                        {
                              width: 900px; 
                              height: auto; 
                              overflow: hidden;
                        }

                        .oauthTableHeader
                        {
                              float: left; 
                              width: 150px; 
                              height: 30px; 
                              padding-top: 15px; 
                              text-align: center;
                              background-color: #343434;
                              color: white;
                        }
                        
                        .frmRowNorm {
                              background-color: transparent !important;
                        }

                        .frmRowAlt {
                            background-color: transparent !important;  
                        }

                        .wrap
                        {
                                /* white-space: pre-wrap; */ /* css-3 */
                                white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
                                white-space: -pre-wrap; /* Opera 4-6 */
                                white-space: -o-pre-wrap; /* Opera 7 */
                                word-wrap: break-word; /* Internet Explorer 5.5+ */
                        }

                        #folderTable { border: 1px solid lightgray; width: 500px; }
                        #folderTable td { border: 1px solid lightgray; }

            ";
            echo $css;
    }
}

?>