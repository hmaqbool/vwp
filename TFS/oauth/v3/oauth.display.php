<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class OAuthDisplay extends DashboardAppDisplay
{
    public static $urlVars = array();

    public function admin($data,$page,$pages)
    {
        global $template;
        
        $template->pushHeader('
            <script>
                function popupwindow(url, title, w, h) 
                {
                    var left = (screen.width/2)-(w/2);
                    var top = (screen.height/2)-(h/2);
                    return window.open(url, title, "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width="+w+", height="+h+", top="+top+", left="+left);
                } 
            </script>
        ');
        
        $html = new htmlBlock;
        $html
            ->push(new basicH1)
                ->append("OAuth Management")
            ->ascend()
                
            ->push(new basicDiv)
                ->push(tag::p())
                    ->append('Press the following button to get Authentication Token from OAuth service:')
                ->ascend()
                ->push(tag::input())
                    ->type('button')
                    ->value('Get OAuth Token')
                    //->onclick('xajax_authenticate')
                    ->onclick('popupwindow("http://hashimadmin.sporefanz.com/oauth/admin/authenticate", "OAuth Authentication", 800, 500);')
                ->ascend()
            ->ascend()
                
            ->push(tag::br())
                
            ->push(new divLink)
                ->setLink($this->getPathX() ."admin/workspaces","Workspace Management")
            ->ascend()
            ->push(new divLink)
                ->setLink($this->getPathX() ."admin/events","Event Management")
            ->ascend()
            ->push(new divLink)
                ->setLink($this->getPathX() ."admin/rolemaps","Role Management")
            ->ascend()
            ->push(new divLink)
                ->setLink($this->getPathX() ."admin/users","User Management")
            ->ascend()
            ->push(new divLink)
                ->setLink($this->getPathX() ."admin/invitations","Invitation Management")
            ->ascend()
            ;
        
        return $html;
    }
    
    public function regResources()
    {
        // Access to the Template controller object
        global $template;
        
        // Loaded jQuery colorbox.js file in the header
        $template->pushHeader('<link rel="stylesheet" href="http://hashim.sporefanz.com/includes/css/colorbox.css" />');
        $template->pushHeader('<script src="http://hashim.sporefanz.com/includes/js/jquery.colorbox.js"></script>');
    }
    
    public function regClientEventsForWorkspaces()
    {
        // Include js, css resources in page.
        $this->regResources();
        
        // Access to the Template controller object
        global $template;
        
        // jQuery event bindings and plugin calls here
        $template->pushReadyFunc('

            $(".delete").click(function()
            {
                /*
                if (!confirm("Are you sure you want to delete this workspace?. If yes then all of its sub-folders and files will also be deleted.")) {
                    return false;
                }
                */

                $("#imgLoading").show();
                xajax_deleteWorkspace($(this).attr("workspaceId"));
                return false;
            });

            $("#btnCreate").click(function()
            {
                $("#imgLoading").show();
                xajax_createWorkspace($("#newWorkspace").val());
            });

        ');
    }
    
    public function showMessage($message, $title='Alert')
    {
        // Access to the Template controller object
        global $template;
        $template->pushReadyFunc("alert('$message', '$title');");
    }    
    
    public function workspaces($arrResult)
    {
        if ($arrResult['status'] == false)
        {
            $this->showMessage($arrResult['message'], $arrResult['title']);
            return;
        }
        
        $this->regClientEventsForWorkspaces();
        
        $html = new htmlBlock;
        $html
            ->push(new basicH1)
                ->append("Workspace Management")
            ->ascend()
                
            ->push(new basicDiv)
                ->push(tag::table())
                    ->push(tag::tr())
                        ->push(tag::td())
                            ->append("New Workspace:")
                        ->ascend()
                        
                        ->push(tag::td())
                            ->push(tag::input())
                                ->type('text')
                                ->id('newWorkspace')
                                ->value('')
                            ->ascend()
                        ->ascend()
                        
                        ->push(tag::td())
                            ->align('right')
                            ->push(tag::input())
                                ->type('button')
                                ->id('btnCreate')
                                ->value('Create')
                            ->ascend()
                        ->ascend()
                    ->ascend()
                ->ascend()
            ->ascend()
                
            ->push(tag::br())
            
            ->push(tag::div())
                ->style('font-size: 14px; width: 700px; font-style: italic; margin-bottom: 3px;')
                ->append('Note: There are only 2 workspaces are allowed to create. if you want to create a new workspace then delete any existing workspace and then create a new one.')
            ->ascend()

            ->push(tag::div())
                ->style('border: 1px solid lightgray; padding: 20px; width: 700px;  margin-bottom: 50px;')
                ->push(tag::div())
                    ->class('wrap')
                    ->style('width: 100%; height: 200px;')
                    ->push(tag::div())
                        ->style('font-size: 14px; font-style: italic; margin-bottom: 3px;')
                        ->append('Select any workspace to view its childs.')
                        ->push(tag::table())
                            ->id('table')
                            ->cellpadding('0')
                            ->cellspacing('0')
                            ->push($this->getWorkspacesHtml($arrResult['data']))
                            ->ascend()
                        ->ascend()
                    ->ascend()
                ->ascend()
            ->ascend()
            ; 
        
        return $html;
    }
    
    public function getWorkspacesHtml($arrWorkspaces)
    {
        $html = new htmlBlock;
        
        foreach ($arrWorkspaces['items'] as $workspace) 
        {
            $html
                ->push(tag::tr())
                    ->push(tag::td())
                        ->style('width: 400px;')
                        ->push(tag::a())
                            ->href('workspaceResources/workspaceId_'. $workspace['workspace']['id'] .'/rootFolderId_'. $workspace['workspace']['root_folder_id'])
                            ->append($workspace['workspace']['name'])
                        ->ascend()
                    ->ascend()
                    
                    ->push(tag::td())
                        ->style('width: 80px; text-align: center;')
                        ->push(tag::a())
                            ->class('delete')
                            ->href('javascript:void()')
                            ->workspaceId($workspace['workspace']['id'])
                            ->append('Delete')
                        ->ascend()
                    ->ascend()
                ->ascend()
            ;
        }
        
        return $html;
    }
        
    public function regClientEventsForWorkspaceResources($workspaceId, $folderId)
    {
        // Include js, css resources in page.
        $this->regResources();
        
        // Access to the Template controller object
        global $template;
        
        // jQuery event bindings and plugin calls here
        $template->pushReadyFunc('
        
            var folderId = "'. $folderId .'";
            var workspaceId = "'. $workspaceId .'";

            $(".update").click(function()
            {
                var resourceId = $(this).attr("resourceId");
                var resourceType = $(this).attr("resourceType");
                $("#resourceName").val($(this).attr("resourceName"));

                $.colorbox ({
                    href: "#updateContent",
                    inline: true,
                    width: "300px",
                    height: "170px"
                });

                $("#btnUpdate").click(function() 
                {
                    if ($("#resourceName").val().trim() == "")
                    {
                        alert("Please provide any name");
                        return;
                    }

                    $(this).attr("disabled", true);
                    $("#updateMessage").show();

                    // Need to send id as well.
                    var result = xajax_updateResource(resourceId, $("#resourceName").val(), resourceType);
                
                });

                return false;
            });
            

            $("#btnCreate").click(function()
            {
                if ($("#newFolder").val().trim() == "") 
                {
                    alert("Please provide new folder name.");
                    return;
                }

                $("#imgLoading").show();
                xajax_createFolder(workspaceId, folderId, $("#newFolder").val().trim());

            });
            
            $(".delete").click(function()
            {
                var resourceId = $(this).attr("resourceId");
                var resourceType = $(this).attr("resourceType");

                /*
                if (resourceType == "folder")
                {
                    if (!confirm("Are you sure you want to delete this folder?. If yes then all of its sub-folders and files will also be deleted.")) {
                        return false;
                    }
                }
                else
                {
                    if (!confirm("Are you sure you want to delete this file?.")) {
                        return false;
                    }
                }
                */

                $("#imgLoading").show();
                xajax_deleteResource(resourceId, resourceType);
            });



        ');
    }
    
    public function workspaceResources($arrResources, $workspaceId, $folderId, $rootFolderId)
    {
        $this->regClientEventsForWorkspaceResources($workspaceId, $folderId);
        
        $html = new htmlBlock;
        
        $html->push(tag::div())
                ->push(tag::div())
                    ->addClass('wrap')
                
                    ->push(new basicH1)
                        ->append('Folders & Files Management (OAuth)')
                    ->ascend()
                
                    ->push(tag::img())
                        ->id('imgLoading')
                        ->src('http://hashim.sporefanz.com/includes/images/loading2.gif')
                        ->style('display: none;')
                    ->ascend()
                
                    ->push(tag::div())
                        ->style('border: 1px solid lightgray; padding: 20px; width: 700px; margin-bottom: 10px;')
                        ->push($this->getCreateNewFolderHtml())
                        ->ascend()
                    ->ascend()
                
                    ->push(tag::div())
                        ->style('border: 1px solid lightgray; padding: 20px; width: 700px; margin-bottom: 10px;')
                        ->push($this->getFileUploadHtml($workspaceId, $rootFolderId, $folderId))
                        ->ascend()
                    ->ascend()

                    ->push(tag::div())
                        ->style('border: 1px solid lightgray; padding: 20px; width: 700px;')
                        ->push(tag::p())
                            ->append('Folders & Files List:')
                        ->ascend()
                        ->push($this->getFilesAndFoldersHtml($arrResources, $workspaceId, $rootFolderId))
                        ->ascend()
                    ->ascend()
                
                ->ascend()
             ->ascend()
        ;
        
        $html->push($this->getUpdatePopupHtml());
                    
        return $html;
    }
    
    public function getUpdatePopupHtml()
    {
        $html = new htmlBlock;
        
        $html
            ->push(tag::div())
                ->style('width: 300px; height: 100px; padding-top: 20px; padding-left: 40px; display: none;')
                ->push(tag::div())
                    ->id('updateContent')
                    ->style('padding:10px; background:#fff;')
                    ->push(tag::table())
                        ->cellspacing('3')
                
                        ->push(tag::tr())
                            ->push(tag::td())
                                ->append('Name:')
                            ->ascend()
                            ->push(tag::td())
                                ->push(tag::input())
                                    ->id('resourceName')
                                    ->type('text')
                                    ->value('')
                                ->ascend()
                            ->ascend()
                        ->ascend()
                
                        ->push(tag::tr())
                            ->push(tag::td())
                            ->ascend()
                            ->push(tag::td())
                                ->push(tag::input())
                                    ->align('right')
                                    ->id('btnUpdate')
                                    ->type('button')
                                    ->value('Update')
                                ->ascend()
                            ->ascend()
                        ->ascend()
                        
                    ->ascend()
                
                    ->push(tag::div())
                        ->id('updateMessage')
                        ->style('display: none')
                        ->append('Please Wait..')
                    ->ascend()
                
                ->ascend()
            ->ascend()
        ;
                
        return $html;
    }
    
    public function getFilesAndFoldersHtml($arrResources=array(), $workspaceId, $rootFolderId)
    {
        $html = new htmlBlock;
        $table = $html->push(tag::table());
            
        foreach ($arrResources['items'] as $resource)
        {
            if (isset($resource['folder'])) 
            {
                $id = $resource['folder']['id'];
                $name = $resource['folder']['filename'];
                $path = $this->getPathX() . "admin/workspaceResources/id_$id/workspaceId_$workspaceId/rootFolderId_$rootFolderId";
                $type = 'folder';
            }
            else
            {
                $id = $resource['file']['id'];
                $name = $resource['file']['filename'];
                $path = $this->getPathX() . "admin/downloadFile/id_$id/workspaceId_$workspaceId";
                $type = 'file';
            }
            
            $table
                ->push(tag::tr())
                    
                    ->push(tag::td())
                        ->style('width: 400px;')
                        ->push(tag::a())
                            ->href($path)
                            ->append($name)
                        ->ascend()
                    ->ascend()
                    
                    ->push(tag::td())
                        ->style('width: 80px; text-align: center;')
                        ->push(tag::a())
                            ->href('javascript:void(0)')
                            ->addClass('update')
                            ->append('Update')
                            ->resourceId($id)
                            ->resourceName($name)
                            ->resourceType($type)
                        ->ascend()
                    ->ascend()
                    
                    ->push(tag::td())
                        ->style('width: 80px; text-align: center;')
                        ->push(tag::a())
                            ->href('javascript:void(0)')
                            ->addClass('delete')
                            ->append('Delete')
                            ->resourceId($id)
                            ->resourceType($type)
                        ->ascend()
                    ->ascend()
                    
                ->ascend();
        }
        
        return $table;
    }
    
    public function getFileUploadHtml($workspaceId, $rootFolderId, $folderId)
    {
        $form = new basicForm;
        //$form->setAjaxFunc("fileUpload");
        //$form->pushIAction($this->getPathX().'/admin/uploadFile');
        
        $form 
            ->newRow('Browse for file to upload')
                ->pushIFile('file')
                ->addAttr('id', 'file')
                ->addAttr('name', 'file')
                ->addAttr('size', '100')
        ;
        
        $form
            ->appendIHidden('workspaceId', $workspaceId)
            ->appendIHidden('folderId', $folderId)
            ->appendIHidden('rootFolderId', $rootFolderId)
        ;
        
        $form
            ->newRow('')
            ->pushISubmit('Upload')
        ;
        
        return $form;
    }
        
    public function getCreateNewFolderHtml()
    {
        $html = new htmlBlock;
        
        $html->push(tag::table())
                ->push(tag::tr())
                    ->push(tag::td())
                        ->append('New Folder:')
                    ->ascend()
                    ->push(tag::td())
                        ->push(tag::input())
                            ->type('text')
                            ->id('newFolder')
                        ->ascend()
                    ->ascend()
                    ->push(tag::td())
                        ->align('right')
                        ->push(tag::input())
                            ->type('button')
                            ->id('btnCreate')
                            ->value('Create')
                        ->ascend()
                    ->ascend()
                ->ascend()
            ->ascend();
        
        return $html;
    }
    
    public function rolemaps($data)
    {
        global $template;
        $template
            ->pushReadyFunc('

                $("form").get(0).setAttribute("method", "POST");
                $("form").get(0).setAttribute("onsubmit", "");
            ');

        $meetingList = tag::div();
        $meetingList
            ->addClass('oauthRowHeaderContainer')
            ->push(tag::div())
                ->addClass("oauthTableHeader")
                ->setAttr("style", "width: 250px;")
                ->append('File/Folder Name')
            ->ascend()

            ->push(tag::div())
				->addClass("oauthTableHeader")
				->setAttr("style", "width: 200px;")
                ->append('User Name')
            ->ascend()

            ->push(tag::div())
				->addClass("oauthTableHeader")
                ->append('File Type')
            ->ascend()

            ->push(tag::div())
				->addClass("oauthTableHeader")
                ->append('Role Name')
            ->ascend()

            ->push(tag::div())
				->addClass("oauthTableHeader")
                ->append('Action')
            ->ascend()
        ->ascend()
        ;

		$meetingLists = tag::div();
		$meetingLists->addClass('oauthRowHeaderContainer');
		$classCounter = 2;
        $record = 0;
        while ( $record < count($data))
        {
        	$index = 0;
            while($index < count($data[$record]['roleName']))
            {
				if($classCounter %2 == 0)
                    $class = "oauthRowWhite";
                else
                    $class = "oauthRowNormal";


                if( $data[$record]['roleName'][$index] != 'Administrator')
                {
                    $link = tag::a();
                    $link
                        ->setAttr("href", "javascript:void(0)")
                        ->setAttr("onclick", "xajax_rolemaps(\"".$data[$record]["roleMapId"][$index]."\",\"".$data[$record]["fileName"][$index]."\",\"".$data[$record]["fileType"][$index]."\")")
                        ->append('Delete')
                    ->ascend();
                }
                else{
                    $link = '';
                }

				$meetingLists
					->push(tag::div())
						->addClass($class)
						->setAttr("style", "width: 250px;")
						->append($data[$record]["fileName"][$index])
					->ascend()

					->push(tag::div())
						->addClass($class)
						->setAttr("style", "width: 200px;")
						->append($data[$record]["userId"][$index])
					->ascend()

					->push(tag::div())
						->addClass($class)
						->append($data[$record]["fileType"][$index])
					->ascend()

					->push(tag::div())
						->addClass($class)
						->append($data[$record]['roleName'][$index])
					->ascend()

					->push(tag::div())
						->addClass($class)
						->append($link)
					->ascend()
				->ascend()
				;
            	$index++;
            	$classCounter++;
            }
            $record++;
        }

        if(isset($data['message']) && isset($data['title'])){
            $this->showMessage($data['message'], $data['title']);
        }

        $actionForm = $this->actionForm();
        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($actionForm)
            ->append($meetingList)
            ->append($meetingLists)
        ->ascend()
        ;
        return $html;
    }

    public function users($data)
    {
        $meetingListHeader = tag::div();
        $meetingListHeader
            ->addClass('oauthRowHeaderContainer')
            ->push(tag::div())
                ->addClass("oauthTableHeader")
                ->setAttr("style", "width: 450px;")
                ->append('User Name')
            ->ascend()

            ->push(tag::div())
                ->addClass("oauthTableHeader")
                ->setAttr("style", "width: 450px;")
                ->append('Email Address')
            ->ascend()
        ->ascend()
        ;

        $meetingLists = tag::div();
        $meetingLists->addClass('oauthRowHeaderContainer');
        $classCounter = 2;
        foreach ($data as $userInfo) 
        {
            if($classCounter %2 == 0)
                $class = "oauthRowWhite";
            else
                $class = "oauthRowNormal";

            $meetingLists
                ->push(tag::div())
                    ->addClass($class)
                    ->setAttr("style", "width: 270px; text-align: left; padding-left: 180px;")
                    ->append($userInfo['name'])
                ->ascend()
                ->push(tag::div())
                    ->addClass($class)
                    ->setAttr("style", "width: 270px; text-align: left; padding-left: 180px;")
                    ->append($userInfo['email'])
                ->ascend()
            ->ascend()
            ;
            $classCounter++;
        }

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($meetingListHeader)
            ->append($meetingLists)
        ->ascend()
        ;
        return $html;
    }

    public function events($data)
    {

        $meetingListHeader = tag::div();
        $meetingListHeader
            ->addClass('oauthRowHeaderContainer')
            ->push(tag::div())
                ->addClass("oauthTableHeader")
                ->setAttr("style", "width: 900px;")
                ->append('Activity Details')
            ->ascend()

        ->ascend()
        ;

        $meetingLists = tag::div();
        $meetingLists->addClass('oauthRowHeaderContainer');
        $classCounter = 2;
        foreach ($data as $userInfo) 
        {
            if($classCounter %2 == 0)
                $class = "oauthRowWhite";
            else
                $class = "oauthRowNormal";

            $meetingLists
                ->push(tag::div())
                    ->addClass($class)
                    ->setAttr("style", "width: 750px; text-align: left; padding-left: 150px;")
                    ->append($userInfo["event"]["detail"])
                ->ascend()
            ->ascend()
            ;
            $classCounter++;
        }

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($meetingListHeader)
            ->append($meetingLists)
        ->ascend()
        ;
        return $html;
    }
    
    public function invitations($data)
    {

        global $template;
        $template->pushHeader("
            <script type=\"text/javascript\">
                function formsubmition(email, file, fileType, roleName) 
                { 
                    if($(email).val() == ''){
                        alert('Please provide valid email address');
                    }
                    else{
                        xajax_invitations($(email).val(), file, fileType, $(roleName).val());
                    }
                }
            </script>"
            );

        $meetingListHeader = tag::div();
        $meetingListHeader
            ->addClass('oauthRowHeaderContainer')
            ->push(tag::div())
                ->addClass("oauthTableHeader")
                ->setAttr("style", "width: 250px;")
                ->append('File/Folder Name')
            ->ascend()

            ->push(tag::div())
                ->addClass("oauthTableHeader")
                ->setAttr("style", "width: 250px;")
                ->append('Email Address')
            ->ascend()

            ->push(tag::div())
                ->addClass("oauthTableHeader")
                ->setAttr("style", "width: 250px;")
                ->append('User Role')
            ->ascend()

            ->push(tag::div())
                ->addClass("oauthTableHeader")
                ->setAttr("style", "width: 150px;")
                ->append('Action')
            ->ascend()
        ->ascend()
        ;

        $meetingLists = tag::div();
        $meetingLists->addClass('oauthRowHeaderContainer');
        $counter = 0;

        foreach ($data as $directory) 
        {
            $class = (($counter+2)%2 == 0) ? "oauthRowWhite" : "oauthRowNormal";
            $directoryData = (isset($directory["folder"])) ? $directory["folder"] : $directory["file"];
            $dataType = (isset($directory["folder"])) ? "folder" : "file";

            $link = tag::a();
            $link
                ->setAttr("href", "javascript:void(0)")
                ->setAttr("onclick", "formsubmition(\""."#email".$counter."\", \"".$directoryData["id"]."\", \"".$dataType."\", \""."#role".$counter."\")")
                ->append('Invite')
            ->ascend();

            if(!$directory["message"])
            {
                $meetingLists
                    ->addClass('oauthRowHeaderContainer')
                    ->push(tag::div())
                        ->addClass($class)
                        ->setAttr("style", "width: 250px;")
                        ->append($directoryData["filename"])
                    ->ascend()

                    ->push(tag::div())
                        ->addClass($class)
                        ->setAttr("style", "width: 250px;")
                        ->push(shorttag::input())
                            ->setID("email".$counter)
                            ->setAttr("type","input")
                            ->setAttr("name","email".$counter)
                        ->ascend()
                    ->ascend()

                    ->push(tag::div())
                        ->addClass($class)
                        ->setAttr("style", "width: 250px;")
                        ->push(tag::select())
                            ->setID("role".$counter)
                            ->setAttr("name","role".$counter)

                            ->push(shorttag::option())
                                ->append('Administrator')
                            ->ascend()

                            ->push(shorttag::option())
                                ->append('Moderator')
                            ->ascend()

                            ->push(shorttag::option())
                                ->append('Creator')
                            ->ascend()

                            ->push(shorttag::option())
                                ->append('Downloader')
                            ->ascend()

                            ->push(shorttag::option())
                                ->append('Printer')
                            ->ascend()

                            ->push(shorttag::option())
                                ->append('Viewer')
                            ->ascend()

                        ->ascend()
                    ->ascend()

                    ->push(tag::div())
                        ->addClass($class)
                        ->setAttr("style", "width: 150px;")
                        ->append($link)
                    ->ascend()
                ->ascend()
                ;
                $counter++;
            }
        }

        if(isset($data['message'])){
            $this->showMessage($data['message'], $data['title']);
        }

        $html = tag::div();
        $html
            ->setID("page_container")
            ->append($meetingListHeader)
            ->append($meetingLists)
        ->ascend()
        ;
        return $html;
    }
    
    /*
    public function actionForm()
    {
        global $template;
        $template->pushHeader("
            <script type=\"text/javascript\">
                function formsubmition(uid,file,type) 
                { 
                    $('input[name=userId]').val(uid);
                    $('input[name=fileName]').val(file);
                    $('input[name=fileType]').val(type);
                    $('form').get(0).submit();
                }
            </script>"
            );

        $form = new basicForm;
        $form
            ->pushIhidden("userId")
            ->ascend()

            ->pushIhidden("fileName")
            ->ascend()

            ->pushIhidden("fileType")
            ->ascend()
        ;
        return $form;
    }
    */
}

?>