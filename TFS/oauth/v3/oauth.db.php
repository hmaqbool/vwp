<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class OAuthDB 
{
    public static $Contact = array(
		"tableName"=>"aw_contact",
		"schema"=>array(
				"submissionID" =>array("type"=>"int(10) unsigned","null"=>"NO","autoIncrement"=>"auto_increment","field"=>"int","form"=>"text",),
				"fName" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"lName" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"name" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"addr1" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"addr2" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"city" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"state" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"country" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"email" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"phone" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"isDel" =>array("type"=>"tinyint(1)","null"=>"NO","field"=>"tinyint","form"=>"radio",)
		),
		"minfields"=>array(),
		"uniquefields"=>array(),
        "singular"=>"Contact",
        "primary"=>"submissionID",
        "keys"=>"",
	);

}

?>