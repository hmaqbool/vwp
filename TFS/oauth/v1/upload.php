<?

class SomePage
{
    public function some_upload()
    {
        if($_FILES())
        {
            $logic = $this->getLogic();
            $logic->uploadFile($_FILES);
            $this->doAjax();
            $this->regDisplay("someDisplay");
        }
        else
        {
            $logic = $this->getLogic();
        
            if(!$logic->isAdmin())
            {
                $this->doAjax();
                $this->regDisplay("access_denied");
            }
            
            $this->regAjax("updateResource");
            $this->regAjax("createFolder");
            $this->regAjax("deleteResource");
            
            $this->doAjax();
            
            $arrResources = $logic->workspaceResources($this->getVar('workspaceId'), $this->getId());
    
            // TODO: Display message to user either success of failure.
            $this->regDisplay("workspaceResources", $arrResources, $this->getVar('workspaceId'), $this->getId(), $this->getVar('rootFolderId'));
        }
    }
}


?>