<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class OAuthPage extends DashboardAppPage
{

    public function admin()
    {
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $this->regAjax("authenticate");
        $this->doAjax();
        
        $this->regDisplay("admin",$arr);
    }
    
    public function admin_authenticate()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $arrResult = $logic->authenticate();
        $this->regDisplay("showMessage", $arrResult['message']);
    }
    
    public function admin_callback()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $logic->callback();
    }
    
    public function admin_workspaces()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $this->regAjax("deleteWorkspace");
        $this->regAjax("createWorkspace");
        $this->doAjax();
        
        $data = $logic->workspaces();
        $this->regDisplay("workspaces", $data);
    }
    
    public function admin_deleteWorkspace()
    {        
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        $logic->deleteWorkspace($this->getId);
        // TODO: Display message to user either success of failure.
    }
    
    public function admin_workspaceResources()
    {
        //px($_SERVER);
        
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        if ($this->getVar('workspaceId') <= 0) 
        {
            $this->regDisplay("showMessage", 'Missing parameter Id', 'Missing Parameter');
            return;
        }
        
        if ($this->getVar('rootFolderId') <= 0) 
        {
            // TODO: Have to change it later.
            $this->regDisplay("showMessage", 'Missing parameter Root Folder Id', 'Missing Parameter');
            return;
        }
        
        //$this->regStdAjax("frmFileUpload", "fileUpload", "File successfuly Uploaded", "Success", array("path"=>$_SERVER['REDIRECT_SCRIPT_URI']));
        $this->regStdAjax("fileUpload", "fileUpload", "File successfuly Uploaded", "Success");
        
        $this->regAjax("updateResource");
        $this->regAjax("createFolder");
        $this->regAjax("deleteResource");
        
        $this->doAjax();
        
        $arrResources = $logic->workspaceResources($this->getVar('workspaceId'), $this->getId());

        // TODO: Display message to user either success of failure.
        $this->regDisplay("workspaceResources", $arrResources, $this->getVar('workspaceId'), $this->getId(), $this->getVar('rootFolderId'));
    }
           
    public function admin_downloadFile()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        if ($this->getVar('id') <= 0) {
            $this->regDisplay("showMessage", 'Missing parameter Id', 'Missing Parameter');
            return;
        }

        if ($this->getVar('workspaceId') <= 0) {
            $this->regDisplay("showMessage", 'Missing parameter Workspace Id', 'Missing Parameter');
            return;
        }

        $logic->downloadFile($this->getVar('workspaceId'), $this->getVar('id'));

    }

    public function admin_uploadFile()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        if ($this->getVar('folderId') <= 0)
        {
            $this->regDisplay("showMessage", 'Missing parameter Folder Id', 'Missing Parameter');
            return;
        }

        if ($this->getVar('workspaceId') <= 0)
        {
            $this->regDisplay("showMessage", 'Missing parameter Workspace Id', 'Missing Parameter');
            return;
        }

        if ($this->getVar('rootFolderId') <= 0)
        {
            $this->regDisplay("showMessage", 'Missing parameter Root Folder Id', 'Missing Parameter');
            return;
        }
        
        $logic->uploadFile();
    }


    /*------------------------Adil Abbasi ------------------------------------*/
    
    public function admin_rolemaps()
    {
        $sc = SiteConfig::getObj();
        $sc->template("oauth.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $data = $logic->rolemaps();
        $this->regDisplay("rolemaps", $data);
    }

    public function admin_users()
    {
        $sc = SiteConfig::getObj();
        $sc->template("oauth.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $data = $logic->users();
        $this->regDisplay("users", $data);
    }

    public function admin_events()
    {
        $sc = SiteConfig::getObj();
        $sc->template("oauth.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $data = $logic->events();
        $this->regDisplay("events", $data);
    }

    public function admin_invitations()
    {
        $sc = SiteConfig::getObj();
        $sc->template("oauth.php");
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $data = $logic->invitations();
        //vx($data); die();
        $this->regDisplay("invitations", $data);
    }
    
    /*------------------------Adil Abbasi ------------------------------------*/
    
    
    
}

?>