<?php

//ini_set('display_errors', 1);
// error_reporting(E_ALL);


/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class OAuthApp extends GenericApp
{
    protected  $class_logic = "OAuthLogic";
    protected  $class_display = "OAuthDisplay";
    protected  $class_ajax = "OAuthAjax";
    protected  $class_db = "OAuthDB";
    protected  $class_css = "OAuthCSS";
    protected  $class_page = "OAuthPage";

    protected static $appInstance;

    public static $concern = "oauth";
    public static $name = "OAuth";
    public static $icon = "http://global.imranmedia.com/images/agg-icons/gig/affiliate_sales.png";

    public function __construct($db,$path,$vars=array())
    {
        parent::__construct($db,$path,$vars);
    }
    public static function getObj($db,$path,$arr=array())
    {
        if (!isset(self::$appInstance))
        {
            $c = __CLASS__;
            self::$appInstance = new $c($db,$path,$arr);
        }
        return self::$appInstance;
    }
    public function __varInit($arr=array())
    {
        return parent::__varInit( array_merge(
            array(
                'createFolderUri' => 'https://ws-api.onehub.com//workspaces/{WORKSPACE_ID}/folders.json',
                'createFolderOfFolderUri' => 'https://ws-api.onehub.com//workspaces/{WORKSPACE_ID}/folders/{FOLDER_ID}/folders.json',
                'updateResourceUri' => 'https://ws-api.onehub.com/{RESOURCE_TYPE}s/{RESOURCE_ID}',
                'deleteResourceUri' => 'https://ws-api.onehub.com/{RESOURCE_TYPE}s/{RESOURCE_ID}',
                'workspaceFolderUri' => 'https://ws-api.onehub.com/workspaces/{WORKSPACE_ID}/folders/{FOLDER_ID}.json',
                'workspaceFileUri' => 'https://ws-api.onehub.com/workspaces/{WORKSPACE_ID}/files.json',
                'deleteWorkspaceUri' => 'https://ws-api.onehub.com/workspaces/{WORKSPACE_ID}',
                'workspaceUri' => 'https://ws-api.onehub.com/workspaces',
                'downloadFileUri' => 'https://ws-api.onehub.com/workspaces/{WORKSPACE_ID}/files/{FILE_ID}/download',
                'uploadFileInFolderUri' => 'https://ws-api.onehub.com/workspaces/{WORKSPACE_ID}/folders/{FOLDER_ID}/files',
                'uploadFileInRootUri' => 'https://ws-api.onehub.com/workspaces/{WORKSPACE_ID}/folders/{ROOT_FOLDER_ID}/files',
                'rolemapUri' => 'https://ws-api.onehub.com/rolemaps/{USER_ID}',
                'folderRolemapUri' => 'https://ws-api.onehub.com/folders/{FOLDER_ID}/rolemaps',
                'fileRolemapUri' => 'https://ws-api.onehub.com/files/{FOLDER_ID}/rolemaps',
                'userProfileUri' => 'https://ws-api.onehub.com/user_profiles/{USER_ID}.json',
                'uploadPath' => '',
                'roleMapDelUri' => 'https://ws-api.onehub.com/rolemaps/{USER_ID}',
                'roleMapFolderUri' => 'https://ws-api.onehub.com/folders/{FOLDER_ID}/rolemaps',
                'roleMapFileUri' => 'https://ws-api.onehub.com/files/{FILE_ID}/rolemaps',
                'userProfilesUri' => 'https://ws-api.onehub.com/user_profiles/{USER_ID}.json',
                'workspaceId' => '390845',
                'parentfolder' => '248324455',
                'activityUri' => 'https://ws-api.onehub.com/activity.json',
                'invitationsFolderUri' => 'https://ws-api.onehub.com/folders/{ID}/invitations.json',
                'invitationsFileUri' => 'https://ws-api.onehub.com/files/{ID}/invitations.json',
            ),$arr) );
    }
    public function getCrons()
    {
        //$this->crons[] = array("frequency"=>86400,"app"=>"Hashim","callback"=>"logicMethod","firstrun"=>0);
        return $this->crons;
    }
}

?>