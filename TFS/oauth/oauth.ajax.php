<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class OAuthAjax extends AppAjax
{
    public function updateResource($id, $name, $type)
    {
        $logic = $this->getLogic();
        $result = $logic->updateResource($id, $name, $type);
        
        $xajax = new xajaxResponse;
        
        if ($result['status'] == false)
        {
            $xajax->script("$('#btnUpdate').attr('disabled', false); $('#updateMessage').hide(); parent.jQuery.colorbox.close();");
            $xajax->alert($result['message']);
        }
        else {
            $xajax->script("parent.jQuery.colorbox.close(); alert('{$result['message']}', '{$result['title']}'); location.href=location.href;");
        }

        return $xajax;
    }
    
    public function createFolder($workspaceId, $currentFolderId, $newFolder)
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;
        
        if ($workspaceId <= 0) 
        {
            $xajax->alert("Error! missing parameter workspace Id.");
            return $xajax;
        }
        
        if ($currentFolderId === null) 
        {
            $xajax->alert("Error! missing parameter Current Folder Id.");
            return $xajax;
        }
        
        if ($newFolder == '') 
        {
            $xajax->alert("Error! missing parameter New Folder.");
            return $xajax;
        }
                
        $result = $logic->createFolder($workspaceId, $currentFolderId, $newFolder);
        
        if ($result['status'] == false)
        {
            $xajax->script('$("#imgLoading").hide();');
            $xajax->alert($result['message']);
            
            return $xajax;
        }

        $xajax->script("alert('{$result['message']}', '{$result['title']}'); $('#imgLoading').show(); location.href=location.href;");
        return $xajax;
    }
    
    public function deleteResource($resourceId, $type)
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;
        
        if ($resourceId <= 0) 
        {
            $xajax->alert("Error! missing parameter Resource Id.");
            return $xajax;
        }
        
        if ($type == '') 
        {
            $xajax->alert("Error! missing parameter Type.");
            return $xajax;
        }
        
        $result = $logic->deleteResource($resourceId, $type);
        
        if ($result['status'] == false)
        {
            $xajax->script('$("#imgLoading").hide();');
            $xajax->alert($result['message']);
            
            return $xajax;
        }
        
        $xajax->script("alert('{$result['message']}', '{$result['title']}'); $('#imgLoading').show(); location.href=location.href;");
        
        return $xajax;
    }
    
    public function deleteWorkspace($workspaceId)
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;
        
        if ($workspaceId <= 0) 
        {
            $xajax->alert("Error! missing parameter Workspace Id.");
            return $xajax;
        }
        
        $result = $logic->deleteWorkspace($workspaceId);
        
        if ($result['status'] == false)
        {
            $xajax->script('$("#imgLoading").hide();');
            $xajax->alert($result['message']);
            
            return $xajax;
        }
        
        $xajax->script("alert('{$result['message']}', '{$result['title']}'); $('#imgLoading').show(); location.href=location.href;");
        
        return $xajax;
    }
    
    public function createWorkspace($newWorkspace)
    {
        $logic = $this->getLogic();
        $xajax = new xajaxResponse;
        
        if ($newWorkspace == '') 
        {
            $xajax->alert("Error! missing parameter New Workspace.");
            return $xajax;
        }
                
        $result = $logic->createWorkspace($newWorkspace);
        
        if ($result['status'] == false)
        {
            $xajax->script('$("#imgLoading").hide();');
            $xajax->alert($result['message']);
            
            return $xajax;
        }
            
        $xajax->script("alert('{$result['message']}', '{$result['title']}'); $('#imgLoading').show(); location.href=location.href;");
        
        return $xajax;
    }
    
    // For testing only.
    public function popupUpdate()
    {
        $xajax = new xajaxResponse;
        $xajax->script("alert('asdgasdgasdg');");
        
        return $xajax;
        
        //$xajax->script();
        //$xscript->script("  ");
        
        // ajax handler object
        //$xajax = new xajaxResponse;
        
        // business logic object
        //$logic = $this->getLogic();
        //$response = $logic->foo($data);
        
        //if($response instanceof Error)
        //{
            // onfail, display an alert box with the message "Error!"
            //$xajax->alert("Error!");
        //}
        //else 
        //{
            /* 
            onSuccess, alert "Success!". When the user clicks ok,
            redirect to /redirect/on/success.
            */
            //$xajax->alertRedirect("Success!","/redirect/on/success");
        //}
        //return $xajax;
        
    }

}

?>