<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class CitrixAPI {

	public $_organizerKey;
	public $_accessToken;

	public function __construct ($_accessToken = null, $_organizerKey = null) 
	{
		$this->_accessToken = $_accessToken;
		$this->_organizerKey = $_organizerKey;
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    get token
    */
	public function getOAuthToken ($_apiKey = null, $_callbackUrl = null, $authorize = null, $code = null) 
	{
		if ($authorize == 1) 
		{
			echo "<META HTTP-EQUIV=\"Refresh\" Content=\"0; URL=https://api.citrixonline.com/oauth/authorize?client_id=$_apiKey&redirect_uri=$_callbackUrl\">";    
			exit;
		}

		if ($code != null ) 
		{
			$url = 'https://api.citrixonline.com/oauth/access_token?grant_type=authorization_code&code='. $code .'&client_id='. $_apiKey;
			return $this->makeApiRequest($url);
		}
	}
/*
	public function getAttendeesByOrganizer () 
	{
		$url  = 'https://api.citrixonline.com/G2M/rest/organizers/'. $this->_organizerKey .'/attendees';
		$url .= '?startDate='. date('c');
		$url .= '?endDate='. date('c', strtotime("-7 Days"));

		return $this->makeApiRequest($url, 'GET', array(), $this->getJsonHeaders());
	}
*/

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    create Meeting
    */
	public function createMeeting($subject, $start, $end, $conferencecallinfo, $meetingtype) 
	{
		$url  = 'https://api.citrixonline.com/G2M/rest/meetings';
		$data = array(
			'subject'=>$subject,
			'starttime'=>$start,
			'endtime'=>$end, 
			'passwordrequired'=>'false', 
			'conferencecallinfo'=>$conferencecallinfo, 
			'timezonekey'=>'', 
			'meetingtype'=>$meetingtype
		);
		$dataJson = json_encode ($data);
		return $this->makeApiRequest($url, 'POST', $dataJson, $this->getJsonHeaders());
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    start Meeting
    */
	public function startMeeting($mid) 
	{
		$url  = "https://api.citrixonline.com/G2M/rest/meetings/$mid/start";
		return $this->makeApiRequest($url, 'GET', array(), $this->getJsonHeaders());
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    update Meeting
    */
	public function updateMeeting($mid, $subject, $start, $end, $conferenceCallInfo, $meetingType) 
	{
		$url  = "https://api.citrixonline.com/G2M/rest/meetings/$mid";
		$data = array(
			'subject'=>$subject,
			'starttime'=>$start,
			'endtime'=>$end, 
			'passwordrequired'=>'false', 
			'conferencecallinfo'=>$conferenceCallInfo, 
			'timezonekey'=>'', 
			'meetingtype'=>$meetingType
		);
		$dataJson = json_encode ($data);
		return $this->makeApiRequest($url, 'PUT', $dataJson, $this->getJsonHeaders());
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    get Futuer Meeting
    */
	public function getFutuerMeeting($start, $end) 
	{
		$url  = 'https://api.citrixonline.com/G2M/rest/meetings';
		$data = array(
			'scheduled'=>'true',
			'starttime'=>$start,
			'endtime'=>$end, 
		);
		$dataJson = json_encode ($data);
		return $this->makeApiRequest($url, 'GET', $dataJson, $this->getJsonHeaders());
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    get Meeting
    */
	public function getMeeting($m_id) 
	{
		$url  = "https://api.citrixonline.com/G2M/rest/meetings/$m_id";
		return $this->makeApiRequest($url, 'GET', array(), $this->getJsonHeaders());
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    delete Meeting
    */
	public function deleteMeeting($meeting_id) 
	{
		$url  = "https://api.citrixonline.com/G2M/rest/meetings/$meeting_id";
		return $this->apiDelRequest($url, $this->getJsonHeaders());
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    makeApiRequest
    */
	public function makeApiRequest ($url = null, $requestType = 'GET', $postData = array(), $headers = array()) 
	{
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	    if ($requestType == 'POST') 
	    {
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	    }
	    else if($requestType == 'PUT')
	    {
	    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	    	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	    }

	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    $data = curl_exec($ch);

	    $validResponseCodes = array(200, 201, 409);
	    $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 

	    if($requestType == 'PUT'){
	    	return $responseCode;
	    }
	    
	    if (curl_errno($ch)) 
	    {
	    	curl_close($ch);
	        return curl_error($ch);
	    }
	    elseif (!in_array($responseCode, $validResponseCodes)) 
	    {
	        if ($this->isJson($data)){
	            $data = json_decode($data);
	        }
	    }

	    curl_close($ch);
	    return $data;
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    apiDelRequest
    */
	public function apiDelRequest($url,$headers)
	{

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	    curl_setopt($ch, CURLOPT_URL,$url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

	    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    $result = curl_exec($ch);
	    
	    $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
	    curl_close($ch);

	    return $responseCode;
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    get Json Headers
    */
	public function getJsonHeaders () 
	{
		return array(
			"HTTP/1.1",
			"Content-type: application/json",
			"Accept: application/json",
			"Authorization: OAuth oauth_token=". $this->_accessToken
		);
	}

	/**
    * @author:  M. Hashim
    * @date:    Monday, October 21, 2013
    * @desc:    is Json 
    */
	public function isJson ($string) 
	{
	    $isJson = 0;
	    $decodedString = json_decode($string);

	    if (is_array($decodedString) || is_object($decodedString)) 
	    {
	        $isJson = 1;
	    }

	    return $isJson;
	}
}
