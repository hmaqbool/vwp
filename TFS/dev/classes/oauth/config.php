<?php

    use \fkooman\OAuth\Client;
    
    function getClientConfig()
    {
        if (isLocalhost())
        {
            // Create client config object. (TestClient1)
            $clientConfig = new Client\ClientConfig(
                array
                (
                    "authorize_endpoint" => "https://ws-api.onehub.com/oauth/authorize",
                    "token_endpoint" => "https://ws-api.onehub.com/oauth/token",
                    "client_id" => "7y5py9xjj0ae0gbti3hl2a8wvmydsjw",
                    "client_secret" => "ovtptijxukcpjp598cnjay0hp8tup4b",
                    "redirect_uri" => "http://localhost/folio3/projects/vwp/callback.php",
                    "credentials_in_request_body" => true,
                    "default_token_type" => "bearer",
                    "allow_null_expires_in" => true,
                )
            );
        }
        else
        {
            // Create client config object. (TestClient2)
            $clientConfig = new Client\ClientConfig(
                array
                (
                    "authorize_endpoint" => "https://ws-api.onehub.com/oauth/authorize",
                    "token_endpoint" => "https://ws-api.onehub.com/oauth/token",
                    "client_id" => "pf69kjzjmuwetcbl4bqdnlthvjkp3ob",
                    "client_secret" => "6t2xws0kzj3j2ylojmlbbzu5cu043sm",
                    //"redirect_uri" => "http://www.vwp.co/hashim/callback.php",
                    "redirect_uri" => "http://hashimadmin.sporefanz.com/oauth/admin/callback/query_",
                    "credentials_in_request_body" => true,
                    "default_token_type" => "bearer",
                    "allow_null_expires_in" => true,
                )
            );
        }

        return $clientConfig;
    }
    
    
    function getTokenStorage()
    {
        if (isLocalhost()) {
            $dsn = 'mysql:dbname=444787_hashim;host=72.3.204.187';
        }
        else {
            $dsn = 'mysql:dbname=444787_hashim;host=mysql51-031.wc2.dfw1.stabletransit.com';  
        }

        $user = '444787_hashim';
        $password = 'F0l10Three';

        try 
        {
            $db = new PDO($dsn, $user, $password);
        } catch (PDOException $e) 
        {
            echo 'Connection failed: ' . $e->getMessage();
        }

        // For database usage. (Temporarily commented out)
        $tokenStorage = new Client\PdoStorage($db);

        return $tokenStorage;
    }
    
    function getClientConfigId()
    {
        return "TestClient4";
    }

    function getScope()
    {
        return "authorizations";
        //return "read";
    }

    function getUserId()
    {
        return "info@venturehealth.com";
    }
    
    function isLocalhost() 
    {
        return ($_SERVER['HTTP_HOST'] == 'localhost') ? true : false;
    }

?>
