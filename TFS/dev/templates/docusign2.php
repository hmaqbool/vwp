<?
global $_TEMP,$_CONF,$template;

$script = ScriptLoader::getObj();
$link = LinkLoader::getObj();
ob_start();
?>
<?
$template->pushHeader(ob_get_clean());

$script->getJS();
$link->getJS();

$compress = "";
if($_SERVER["HTTP_ACCEPT_ENCODING"])
{
    $compress = ".jgz";
}

$page = new SiteTemplate;
$page->setOpt("feedback",false);

//$page->setOpt("css","/style.css");
//$page->setOpt("readyJS","ready12.js");

$page->setOpt("readyJS","ready.jquery.10.js");
$page->setOpt("compressJS",true);
$page->setOpt("jQuery",'1.10.2');
$page->setOpt("icon","favicon2.png");

ob_start();
?>

<div class="container">
    <div class="authbox">
        <span><?php echo $_SESSION["UserID"]; ?></span> 
        (<a href="index.php?logout">logout</a>)
    </div>
    <table class="tabs" cellspacing="0" cellpadding="0">
        <tr>
            <td class="current"><a href="http://hashimadmin.sporefanz.com/docusign2/admin/senddocument">Send Document</a></td>
            <td><a href="http://hashimadmin.sporefanz.com/docusign2/admin/sendTemplate">Send a Template</a></td>
            <td><a href="http://hashimadmin.sporefanz.com/docusign2/admin/embedDocuSign">Embed Docusign</a></td>
            <td><a href="http://hashimadmin.sporefanz.com/docusign2/admin/getStatusAndDocs">Get Status and Docs</a></td>
        </tr>
    </table>
    <?
    $template->pushHeader(ob_get_clean());
    $page->pushBody($_TEMP["main"]);
    echo $page;
    ?>
</div>