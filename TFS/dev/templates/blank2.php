<?php
global $_TEMP,$_CONF,$template;

ob_start();
?>
<link href="/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="http://wh.imranmedia.com/ext/helloadmin/style.css" rel="stylesheet" />

<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" media="all" type="text/css" href="https://rawgithub.com/trentrichardson/jQuery-Timepicker-Addon/master/src/jquery-ui-timepicker-addon.css" />
    
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://rawgithub.com/trentrichardson/jQuery-Timepicker-Addon/master/src/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="https://rawgithub.com/trentrichardson/jQuery-Timepicker-Addon/master/src/jquery-ui-sliderAccess.js"></script>

<style>
    #page{
        padding: 0; 
        margin: 0;
        height: auto;
        width: 100%
    }

    #container{
        width: 900px; 
        height: auto; 
        margin:0 auto;
    }

    #header {
        overflow: hidden;
        margin-top: 15px;
        width: 100%;
    }

    #logo {
        margin-top: 15px;
        width: 255px;
        float: left;
    }

    #logo a{
        text-decoration: none;
        color: #555;
    }

    #navigation a{
        text-decoration: none;
        color: #555;
        margin-left: 9px;
        font-weight: bold;
        font-size: 13px;
    }

    #navigation{
        float: right;
        width: 645px;
        margin-top: 50px;
    }

    #navigation a:hover{
        color: black;
    }
</style>
<?php
    
    $template->pushHeader(ob_get_clean());
    /*
    $template->pushReadyFunc('
        $("#menu").lavaLamp({
        fx: "easeOutBack",
        speed: 700
    });
    ');
    */
    $page = new SiteTemplate;
    $page->setOpt("feedback",false);
    $page->setOpt("removeCSS",true);
   // $page->setOpt("readyJS","ready11.2.js");
    $page->setOpt("icon","favicon3.png");    
    $page->setOpt("compressJS",true);
    
    // x x x x      START TEMPLATE     x x x x x x
    ob_start();
    ?>
<!-- Page Wrapper -->
<div id="page">
    <!-- Container -->
    <div id="container">
        <!-- Header -->
        <div id="header">
            <!-- Logo -->
            <div id="logo"><a href="http://hashimadmin.sporefanz.com/gotomeeting/admin/"><h1>GoToMeeting Api</h1></a></div>
            <!-- End Logo -->

            <!-- Navigation -->
            <div id="navigation">
                <a href="http://hashimadmin.sporefanz.com/gotomeeting/admin/">Home</a>
                <a href="http://hashimadmin.sporefanz.com/gotomeeting/admin/createMeeting/">Create</a>
                <a href="http://hashimadmin.sporefanz.com/gotomeeting/admin/startMeeting/">Start</a>
                <a href="http://hashimadmin.sporefanz.com/gotomeeting/admin/updateMeeting/">Update</a>
                <a href="http://hashimadmin.sporefanz.com/gotomeeting/admin/deleteMeeting/">Delete</a>
                <a href="http://hashimadmin.sporefanz.com/gotomeeting/admin/getFutureMeetings/">Future Meetings</a>
                <a href="http://hashimadmin.sporefanz.com/gotomeeting/admin/authorize_1">Update Token</a>
                <a href="http://hashimadmin.sporefanz.com/">Admin Panel</a>
            </div>
            <!-- Navigation -->

        </div>
        <!-- End Header -->

        <?
        if($_TEMP["showInner"])
        {
        ?>
        <!-- Page Header -->
        <div id="page-header">
            <div class="title"><h2><?=$_TEMP["containerTitle"]?></h2></div>
            <div class="divider-blue"></div>
            <div class="title"><h2><?=$_TEMP["containerPostTitle"]?></h2></div>
        </div>
        <!-- End Page Header -->
        <!-- Main Content -->
        <div id="main-body">        
            <div id="left-body">
                <?=$_TEMP["main"]?>
            </div>
            <div id="right-body">              
                <?
                echo $_TEMP["preSidebar"];
                if($_TEMP["showMenu"])
                {
                    if(!Helper::isEmptyString($_TEMP["sidebarTitle"]))
                    {
                ?>    
                    <h2><?=$_TEMP["sidebarTitle"]?></h2>
                <?
                    }
                ?>
                <ul class="bullet-styled">
                <?
                foreach($_TEMP["sidebarMenu"] as $k=>$v)
                {
                    if($k == $_TEMP["currMenu"])
                    {
                    ?>
                        <li style="display:list-item;float:none;">
                            <strong><?=$v?></strong>
                        </li>
                    <?
                    }
                    else
                    {
                        ?>
                        <li style="display:list-item;float:none;">
                            <a href="/<?=$k?>"><?=$v?></a>
                        </li>
                        <?
                    }
                }
                ?>
                </ul>
                <?          
                }
                echo $_TEMP["sidebarPost"];
                 echo $_TEMP["sidebarFoot"];
                 ?>
          </div>
        </div>
        <div id="main-body-bottom"></div>
        <!-- End Main Content -->
        <!-- Main Content -->
        <div class="content-wrapper">
        </div>
        <!-- End Main Content -->
        <?
        }
        else
        {
            echo $_TEMP['main'];
        ?>
        <br /><br />
        <?    
        }
        ?>        
        <!-- Footer -->
        <div id="footer">
        </div>
        <!-- End Footer -->    
    </div>
    <!-- End Container -->
</div>
<!-- End Page Wrapper -->
<?php 
// x x x x      END TEMPLATE     x x x x x x 
$page->pushBody(ob_get_clean());
echo $page;
?>