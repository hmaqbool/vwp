<?
global $_TEMP,$_CONF,$template;

$script = ScriptLoader::getObj();
$link = LinkLoader::getObj();
ob_start();
?>
<link href="/style.css" rel="stylesheet" type="text/css" />

<?
$template->pushHeader(ob_get_clean());

$script->getJS();
$link->getJS();

$compress = "";
if($_SERVER["HTTP_ACCEPT_ENCODING"])
{
    $compress = ".jgz";
}

$page = new SiteTemplate;
$page->setOpt("feedback",false);
$page->setOpt("css","/style.css");
//$page->setOpt("readyJS","ready12.js");
$page->setOpt("readyJS","ready.jquery.10.js");

$page->setOpt("compressJS",true);
$page->setOpt("jQuery",'1.10.2');
$page->setOpt("icon","favicon2.png");

ob_start();
?>
<?
$template->pushHeader(ob_get_clean());

$page->pushBody($_TEMP["main"]);
echo $page;
?>