<?
global $_TEMP,$_CONF,$template;

$script = ScriptLoader::getObj();
$link = LinkLoader::getObj();
ob_start();
?>
<link href="/style.css" rel="stylesheet" type="text/css" />
    
<?
$template->pushHeader(ob_get_clean());

$script->getJS();
$link->getJS();

$compress = "";
if($_SERVER["HTTP_ACCEPT_ENCODING"])
{
    $compress = ".jgz";
}

$page = new SiteTemplate;
$page->setOpt("feedback",false);
$page->setOpt("css","/style.css");
//$page->setOpt("readyJS","ready12.js");
$page->setOpt("readyJS","ready.jquery.10.js");

$page->setOpt("compressJS",true);
$page->setOpt("jQuery",'1.10.2');
$page->setOpt("icon","favicon2.png");

ob_start();
?>

<script>
    
    var Tab = {
        SEND_DOCUMENT : 'sendDocument',
        SEND_TEMPLATE : 'sendTemplate',
        EMBED_DOCUSIGN : 'embedDocuSign',
        GET_STATUS_AND_DOCS: 'getStatusAndDocs'
    };
    
    function getTabSelected(tabId)
    {
        $('#'+Tab.SEND_DOCUMENT).attr('class', '');
        $('#'+Tab.SEND_TEMPLATE).attr('class', '');
        $('#'+Tab.EMBED_DOCUSIGN).attr('class', '');
        $('#'+Tab.GET_STATUS_AND_DOCS).attr('class', '');
        
        $('#'+tabId).attr('class', 'current');
    }
    
</script>


<div class="container">
    <div class="authbox">
        <span><?php echo $_SESSION["UserID"]; ?></span> 
        (<a href="logout">logout</a>)
    </div>
    <table class="tabs" cellspacing="0" cellpadding="0">
        <tr>
            <td id="sendDocument" class="current"><a href="http://hashimadmin.sporefanz.com/docusign/admin/sendDocument">Send Document</a></td>
            <td id="sendTemplate"><a href="http://hashimadmin.sporefanz.com/docusign/admin/sendTemplate">Send a Template</a></td>
            <td id="embedDocuSign"><a href="http://hashimadmin.sporefanz.com/docusign/admin/embedDocuSign">Embed Docusign</a></td>
            <td id="getStatusAndDocs"><a href="http://hashimadmin.sporefanz.com/docusign/admin/getStatusAndDocs">Get Status and Docs</a></td>
        </tr>
    </table>
    <?
    $template->pushHeader(ob_get_clean());
    $page->pushBody($_TEMP["main"]);
    echo $page;
    ?>
</div>