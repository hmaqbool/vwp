<?
Init::processAjax();

Template::registerDisplay("DSPSoon","coming");

class DSPSoon
{
    public static function coming()
    {
        $html = new htmlBlock;
        
        $html
        	->push(Tag::div())
        		->addClass('largeAlert')
        		->push(Tag::h2())
        			->append('404')
        			->ascend()
        		->push(Tag::h3())
        			->append("Page Not Found")
        			->ascend()
        		->push(Tag::div())
        			->addCLass('divider striped')
        			->ascend()
        		->push(Tag::p())
        			->append('The page you requested could not be found. It has either been moved, removed or maybe it never even existed! Try searching for it, or <a href="/">return to the homepage.</a>')
        			->ascend()
        		->ascend()
        ;
        echo $html;
    }
}

?>