<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class LinkedInDisplay extends DashboardAppDisplay
{
   public static $urlVars = array();

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, November 26, 2013
    * @desc:    Landing page's view
    */
    public function homeView()
    {
        $this->linkedInApiCred();
        $script = $this->linkedInApiScript();
        
        $html = tag::div();
        $form = new basicForm;

		$form->newRow("First Name")
            	->pushIText("firstName")
                ->setID("firstName")
            ->ascend()

        	->newRow("Last Name")
	            ->pushIText("lastName")
	                ->setID("lastName")
            ->ascend()

        	->newRow("Email Address")
            	->pushIText("emailAddress")
                ->setID("emailAddress")
        	->ascend()

        	->newRow("Phone/Mobile")
            	->pushIText("phone")
                ->setID("phone")
        	->ascend()

        	->newRow()
                ->pushIButton("Reset")
                    ->setVal("Reset")
                    ->addAttr("onclick","location.reload();")
            	->ascend()
            	->pushIButton("create")
	                ->setVal('submit')
	                ->setID("create")
	                ->addAttr("onclick","alert('demo button')");

        $html
            ->setID("pageContainer")

            ->push(tag::div())
                ->setID("registerBtn")
                ->push(tag::p())
                	->push(tag::span())
                	    ->setID("registerMsg")
		            	->append("Register with linkedIn")
		            ->ascend()
		            ->push(tag::script())
		            	->setAttr("type","IN/Login")
		            ->ascend()
		        ->ascend()
	        ->ascend()

	        ->push(tag::div())
                ->setID("formContainer")
	            ->push(tag::h1())
	            	->append('Register')
	            ->ascend()
	            ->append($form)
	        ->ascend()

	        ->push(tag::script())
		    	->setAttr("type","text/javascript")
	        	->append($script)
	        ->ascend()
        ->ascend()
        ;
        return $html;
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, November 26, 2013
    * @desc:    Login Api credentials
    */
    public function linkedInApiCred()
    {
        global $template;
        $template->pushHeader("
            <script type=\"text/javascript\" src=\"http://platform.linkedin.com/in.js\">
    			api_key: 750qkqflrdycf8
    			authorize: false
    			onLoad: onLinkedInLoad
            </script>"
        );
    }

    /**
    * @author:  M. Hashim
    * @date:    Tuesday, November 26, 2013
    * @desc:    Login Api credentials
    */
    public function linkedInApiScript()
    {
        return'
				function onLinkedInLoad() 
				{
					IN.Event.on(IN, "auth", function() {onLinkedInLogin();});
					IN.Event.on(IN, "logout", function() {onLinkedInLogout();});
				}

				function onLinkedInLogout() {
					setLoginBadge(false);
				}

				function onLinkedInLogin() 
				{
					IN.API.Profile("me")
				  	.fields(["id", "firstName", "lastName", "pictureUrl", "publicProfileUrl","emailAddress","phoneNumbers","mainAddress"])
				  	.result(function(result) {
				  		setLoginBadge(result.values[0]);
					})
					.error(function(err) {
				  		alert(err);
					});
				}

				function setLoginBadge(profile) 
				{
					if (!profile) {
				  		profHTML = "<p>You are not logged in</p>";
					}
					else {
				  		$("#firstName").val(profile.firstName);
				  		$("#lastName").val(profile.lastName);
				  		$("#emailAddress").val(profile.emailAddress);
				  		$("#phone").val(profile.phoneNumbers.values[0].phoneNumber);
				  		$("#registerMsg").hide();

					}
				}
            ';
    }







}

?>