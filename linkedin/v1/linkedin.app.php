<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class LinkedInApp extends GenericApp
{
    protected  $class_logic = "LinkedInLogic";
    protected  $class_display = "LinkedInDisplay";
    protected  $class_ajax = "LinkedInAjax";
    protected  $class_db = "LinkedInDB";
    protected  $class_css = "LinkedInCSS";
    protected  $class_page = "LinkedInPage";

    protected static $appInstance;

    public static $concern = "linkedin";
    public static $name = "LinkedIn";
    public static $icon = "http://global.imranmedia.com/images/agg-icons/gig/affiliate_sales.png";

    public function __construct($db,$path,$vars=array())
    {
        parent::__construct($db,$path,$vars);
    }
    public static function getObj($db,$path,$arr=array())
    {
        if (!isset(self::$appInstance))
        {
            $c = __CLASS__;
            self::$appInstance = new $c($db,$path,$arr);
        }
        return self::$appInstance;
    }
    public function __varInit($arr=array())
    {
        return parent::__varInit( array_merge(
            array(
            /*
            "collections" => array('Press'=>"PR Type"),
                            "types" => array(
                            "Press"=>array("header"=>"Press Release Collections","type"=>"PR Collection","types"=>"PR Collections"),
                        ),
                            "prefs" =>array(
                    "metaData"=>array('contact'=>"Press Releases Contact Person", 'about'=>'Company Information'),
                ),
            */
            ),$arr) );
    }
    public function getCrons()
    {
        //$this->crons[] = array("frequency"=>86400,"app"=>"Hashim","callback"=>"logicMethod","firstrun"=>0);
        return $this->crons;
    }
}

?>