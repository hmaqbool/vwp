<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class LinkedInPage extends DashboardAppPage
{
	/**
    * @author:  M. Hashim
    * @date:    Tuesday, November 26, 2013
    * @desc:    Landing page of application
    */
    public function admin()
    {
        $sc = SiteConfig::getObj();
        $sc->template("linkedInLogin.php");
        $logic = $this->getLogic();

        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }

        $this->regDisplay("homeView");
    }
    
}

?>