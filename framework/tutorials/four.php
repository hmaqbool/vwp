<head>
    <title>Lesson 4 - Page Class In Depth - Framework</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <h1>Lesson 4 - Page Class In Depth</h1>
    <section>
        <p>
            The available actions to page class methods are truly powerful. We will cover
            them in depth in this lesson.
        </p>
    </section>
    <h2>Path Setup</h2>
    <section>
        <p>
            The base path of the app is defined in the site config. This allows the same 
            app to be easily bound to different paths on different sites.
            The names of the page methods defines the relative paths to the app path. For
            example, for the app "Foo" bound to path "/bar", the method 
            "<strong>some_method</strong>" will
            resolve to "<strong>/bar/some/method</strong>". if you have another method 
            with two consecutive 
            underscores, as in "<strong>some__xml</strong>", it will resolve to 
            "<strong>/bar/some.xml</strong>".
        </p>
    </section>
    <h2>Variables</h2>
    <section>
        <p>
            The format for url variables is as follows: <strong>/some/path/key_value</strong>. 
            This is a slight departure from traditional RESTful urls. The reason for our 
            approach is that it
            allows us to more efficiently distinguish the path from the vars. No recursion required!
            The tradeoff is that keys can't have underscores in them, but values can. For instance,
            <strong>/some/path/key_foo_bar</strong>. This would resolve to a variable 
            "<strong>key</strong>" with value "<strong>foo_bar</strong>".
        </p>
        <p>
            There are two reserved keys, "<strong>id</strong>" and "<strong>page</strong>". 
            They have corresponding method calls:
            <strong>$this-&gt;getID();</strong> and <strong>$this-&gt;getPage();</strong>.
        </p>
        <p>
            For all other variables, you have two options: <strong>$this-&gt;getVar($key);</strong>
            and <br /><strong>$this-&gt;getNumericVar($key,0);</strong> The latter returns the requested 
            key, and if the
            returned value is non-numeric, it will return the value stated in the second paramter.
            The second paramter defaults to 0.
        </p>
    </section>
    <h2>Ajax Methods</h2>
    <section>
        <p>
        We use Xajax as our ajax controller. This provides a level of security to ajax calls, because functionality
        can be registered or unregistered based on the user, and dedicated urls for ajax functions become unneccesary.
        </p>
        <p> 
        There are two formats for registering ajax functions. The first registers a function name, which should match
        the name of an ajax function in your "Ajax" class.
        </p>
        <h3>regAjax() Sample</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_regajax.example",true))?>
        </pre>
        <p>
        The other form is called "regStdAjax()". This is for standard form processing, one of the primary uses of ajax.
        Using this function allows the developer to omit creating corresponding functions in the "Ajax" class.
        There are several configuration options for "regStdAjax()", which will be described next.
        </p>
        <h3>regStdAjax() Sample</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_regstdajax.example",true))?>
        </pre>
    </section>
    <h2>Business Logic</h2>
    <section>
        <p>
            Another important aspect of the controller class (page class) is access to the business logic layer.
        </p>
        <h3>Business Logic Sample</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_pagelogic.example",true))?>
        </pre>
    </section>
    <div id="controls">
        <div style="float: left;"><a href="three.php">Previous</a></div>
        <div style="float: right;"><a href="five.php">Next</a></div>
        <div style="clear: both;"></div>
    </div>
</div>
</body>