<head>
    <title>Lesson 3 - Framework</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <h1>Lesson 3 - App Overview</h1>
    <section>
        <p>
        Our apps consist of 7 files. The files are part of an MVC design pattern, with some
        minor but important variations. The most important difference is the addition of an App class.
        This class acts as the central communication hub for classes within the app, as well as other
        app to app communication.
        </p>
        <p>
        The seven classes are: app, ajax, css, db, display, logic, page.
        </p>
    </section>
    <h2>App Naming &amp; Versions</h2>
    <section>        
        <p>
        The naming system for apps is fairly intricate. Say you want to name your app "Foo". The classes
        should be named <strong>FooApp, FooAjax, FooCSS, FooDB, FooDisplay, FooLogic, FooPage</strong>, 
        and the corresponding files should be named <strong>foo.app.php, foo.ajax.php, foo.css.php, 
        foo.db.php, foo.display.php, foo.logic.php, foo.page.php</strong>. 
        </p>
        <p>
        The files themselves should be in a version numbered folder in a folder named "foo".
        For example, the <strong>FooApp</strong> class file should be located at 
        <strong>foo/v1/foo.app.php</strong>. Note the "v" preceding the version number.
        </p>
    </section>
    <h2>The App Class</h2>
    <section>        
        <h3>Sample App Class</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_appclass.example",true))?>
        </pre>
    </section>
    <h2>The Model</h2>
    <section>
        <p>
        There are two classes that make the model: the DB class and the Logic Class.
        The DB class is the actual php model of the tables in the class. They are
        stored in an array format as shown below:
        </p>
        <h3>Sample DB Class</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_db.example",true))?>
        </pre>
        <p>
        The logic class holds the business logic of the app. Most of the basic operations are 
        created by the app generator (covered later). Complete documentation of available logic
        methods will be covered in another lesson.
        </p>
        <h3>Sample Logic Class</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_logic.example",true))?>
        </pre>
    </section>
    <h2>Controller</h2>
    <section>
        <p>
        Though the design patter is referred to as MVC, we'll cover the controller first 
        since it invokes the model and then registers the desired view. There are two controllers,
        the Page class, and the Ajax class. The Page class is the primary controller, and serves
        three main tasks: register the allowed ajax functions, perform the desired business logic
        and select the proper View.   
        </p>
        <h3>Sample Page Class</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_page.example",true))?>
        </pre>
        <p>
        There are a lot of details that go into the page methods, which will be covered in the 
        next lesson.
        </p>
        <p>        
        The ajax class is a secondary controller, specifically for ajax requests. For most standard
        use cases, the standard ajax function that can be called in the page class is sufficient to
        define an ajax interaction, and no actual function writing is required. For more complex
        ajax functionality, an ajax function can be defined in this class. Here is an example ajax
        method: 
        </p>
        <h3>Sample Ajax Class</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_ajax.example",true))?>
        </pre>
    </section>
    <h2>The View</h2>
    <section>
        <p>
        The primary View class is the Display class. It is extremely versatile. Each method can
        echo out html, return an html string, or an object that has a declared __toString() function.
        We developed our own family of templating classes that use the "chainable" approach utilized
        in the site config.
        </p>
        <h3>Sample Display Class</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_display.example",true))?>
        </pre>
        <p>
        By itself in this simple example, writing html tags in php may seem like overkill, but it
        provides a number of advantages, which will be enumerated in the display class lesson. 
        </p>
        <p>
        The CSS class is very simple. It allows the developer to add custom css specific to the app.
        A virtual file is created at <strong>/path/to/app/app.css</strong>. It is only loaded on app
        pages. 
        </p>
        <h3>Sample CSS Class</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_logic.example",true))?>
        </pre>
    </section>
    <h2>Summary</h2>
    <section>        
        <p>
        While this is not enough information to actually implement an app, hopefully it will give you
        an idea of the approach that we take. Subsequent lessons will cover each topic in more depth.
        </p>
    </section>
    <div id="controls">
        <div style="float: left;"><a href="two.php">Previous</a></div>
        <div style="float: right;"><a href="four.php">Next</a></div>
        <div style="clear: both;"></div>
    </div>
</div>
</body>