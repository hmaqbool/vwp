<head>
    <title>Lesson 7 - Forms &amp; Ajax - Framework</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <div align="right"><a href="index.php">Back to TOC</a></div>
    <h1>Lesson 7 - Understanding Ajax</h1>
    <section>
        <p>
            Our ajax system uses xajax to control both server and client-side 
            asynchronous requests. xajax exposes a number of functions that allow
            data to be pushed to the backend, and content to be returned to the
            frontend.
        </p>
    </section>
    <h2>Ajax Function</h2>
    <section>
        <p>
            The following example lists the most common xajax functions to call on the
            server-side.
        </p>
        <h3>Xajax Server-Side Sample</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_xajax.example",true))?>
        </pre>
    </section>
    <h2>Form Generation</h2>
    <section>
        <p>
            The form object is tightly coupled with the xajax system. Let's start with the
            display.
        </p>
        <h3>Form Display Object</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_formdisplay.example",true))?>
        </pre>
    </section>
    <h2>From processing</h2>
    <section>
        <p>
            The easiest way to handle form processing is with standard ajax. That way you don't
            have to write an ajax function to handle the form response. See the previous lessons
            for directions.
             
        </p>
        <h3>Form Logic Function</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_formlogic.example",true))?>
        </pre>
    </section>
    <div id="controls">
        <div style="float: left;"><a href="five.php">Previous</a></div>
        <div style="float: right;"></div>
        <div style="clear: both;"></div>
    </div>
</div>
</body>