<head>
    <title>Lesson 1 - Framework</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <h1>Lesson 1 - Overview of the System</h1>
    <section>
        <p>
            Our websites and web applications run on our custom MVC Framework. 
            It is a multi-tenant framework that allows for easy, config-driven website and 
            application development, setup and management. The primary imperative of the framework is to
            make iterative development as easy as possible.
        </p>
    </section>
    <h2>The Config File</h2>
    <section>
    <pre>
        <?=str_replace("<br />","",highlight_file("_config.example",true))?>
    </pre>
        <p>
        We don't expect you to understand all of this yet, but hopefully much of it is self-explanatory.
        We've found through years of developing sites and apps that the layers of config files employed by
        certain frameworks can slow down future development cycles. It's time consuming to hunt and peck for
        the location where a certain setting is defined weeks, months or years from the most recent development
        cycle. By having a single, unified config file for each site, we hope to avoid this. 
        </p>
    </section>
    <h2>"Chainable" Style</h2>
    <section>
        <p>
        You hopefully noticed the jQuery-style chaining of calls on the SiteConfig object. We love that style
        of programming and think it provides the best level of readability. Each function call returns the
        SiteConfig object. We use this style wherever possible, especially in the display system.
        </p>
    </section>
    <div id="controls">
        <div style="float: left;"></div>
        <div style="float: right;"><a href="two.php">Next</a></div>
        <div style="clear: both;"></div>
    </div>
</div>
</body>