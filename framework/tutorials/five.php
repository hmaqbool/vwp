<head>
    <title>Lesson 5 - Logic Class In Depth - Framework</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <h1>Lesson 5 - Logic Class In Depth</h1>
    <section>
        <p>
            The logic class is where you implement the business logic of your app.
            It exposes a number of method calls to handle the most common data
            acqusition needs. Also, most of these calls are customizable. And if none
            of them suit your needs, there is a system for handling fully custom
            SQL queries as well.
        </p>
    </section>
    <h2>Inserts</h2>
    <section>
        <h3>Sample Insert Calls</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_inserts.example",true))?>
        </pre>
        <p>
            There are a couple of important things to keep in mind. <strong>First, you DO NOT need to
            clean extra fields off of the array.</strong> All of the insert functions will do that for you.
            Just send the array in as-is, and as long as the keys match the fields in the table,
            they will be part of the insert. This will come in handy when you want to make multiple
            inserts to different tables using the same array data.
        </p>
        <p>
            <strong>$this->getCurrUser();</strong> This gets the currently logged in user from the 
            Session app.
        </p>
    </section>
    <h2>Updates</h2>
    <section>
        <p>
            Updates are fairly simmilar to inserts, but with a "set" array as well.
        </p>
        <pre>
            <?=str_replace("<br />","",highlight_file("_updates.example",true))?>
        </pre>
    </section>
    <h2>Selects Overview</h2>
    <section>
        <h3>Select, From, Where, Order, Limit &amp; Caching.</h3>
        <p>
            A standard select call has five parts, the select, from, where, order and limit statements.
            To this, we add a caching element, which will be covered later. 
        </p>
        <pre>
            <?=str_replace("<br />","",highlight_file("_select.example",true))?>
        </pre>
    </section>
    <h2>Get Calls</h2>
    <section>
        <p>
        Here are the available get calls:
        </p>
        <pre>
            <?=str_replace("<br />","",highlight_file("_get.example",true))?>
        </pre>
    </section>
    <h2>Caching</h2>
    <section>
        <p>
        The cache value in select calls sets the Memcache timeout for storing the returned
        data. 
        </p>        
    </section>
    <h2>Error Checking</h2>
    <section></section>
    <div id="controls">
        <div style="float: left;"><a href="four.php">Previous</a></div>
        <div style="float: right;"><a href="six.php">Next</a></div>
        <div style="clear: both;"></div>
    </div>
</div>
</body>