<head>
    <title>Lesson 6 - Display Class In Depth - Framework</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <div align="right"><a href="index.php">Back to TOC</a></div>
    <h1>Lesson 6 - Displays In Depth</h1>
    <section>
        <p>
            The display class offers quite a bit of flexibility to the developer in
            creating MVC views. For the admin backend, there are a number of helper
            functions to quickly create usable admin panels.
        </p>
    </section>
    <h2>Chainables</h2>
    <section>
        <p>
            The chainable display system provides the developer a means to create a 
            view without alternating between html and php markup.
        </p>
        <h3>Chainable Sample</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_displaychain.example",true))?>
        </pre>
    </section>
    <h2>Admin Manage Layout</h2>
    <section>
        <p>
            The manage layout is a powerful tool for creating aggregate view interfaces
            in the admin panel. Based on the chainable style for displays, it enables the dev
            to create tables of data, add quick action functions and other content to the 
            client interface in a simple manner.
        </p>
        <h3>Manage Layout</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_adminmanage.example",true))?>
        </pre>
    </section>
    <h2>Admin Add Layout</h2>
    <section>
        <p>
            Similar to the Manage Layout, the Add Layout creates a wrapper interface to a
            form object to display necessary admin-related information to the user.
        </p>
        <h3>Add Layout</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_adminadd.example",true))?>
        </pre>
    </section>
    <div id="controls">
        <div style="float: left;"><a href="five.php">Previous</a></div>
        <div style="float: right;"></div>
        <div style="clear: both;"></div>
    </div>
</div>
</body>