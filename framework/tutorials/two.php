<head>
    <title>Lesson 2 - Framework</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div id="main">
    <h1>Lesson 2 - The Site Folder</h1>
    <section>
        <p>
            The "Site Folder", as the name implies, is where all of the site files are housed.
            There are three main subfolders: classes, logic &amp; templates.
        </p>
    </section>
    <h2>Classes Folder</h2>
    <section>
        <p>
        If you have any site-specific classes, this is the folder to put them in. You shouldn't 
        place apps here; those belong in your dev folder (covered in Lesson 3). Typical classes that you
        would expect to find in this section include the Layouts and Blocks classes for the CMS App.
        </p>
        <p>
        Just adding the class to this folder doesn't make it accessible though; You need to register 
        it in the site's config file. 
        </p>
        <pre>
            <?=str_replace("<br />","",highlight_file("_addclasses.example",true))?>
        </pre>
    </section>
    <h2>Logic Folder</h2>
    <section>
        <p>
        The Logic folder houses any static pages that you would like to add to your site.  
        There is one required file, _404.php. It is the catchall _404 file for all bad page requests.
        All other pages are loaded from the registered-apps page classes or from the CMS table.   
        </p>
        <h3>Sample _404.php Page</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_404.example",true))?>
        </pre>
    </section>
    <h2>Template Folder</h2>
    <section>
        <p>
        The template folder stores all of the template files for the site. The primary template file is
        set in the config file:   
        </p>
        <h3>Template Configuration</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_configTemplate.example",true))?>
        </pre>
        <p>
        If you need to change the template used for a particular app page, all you need to do 
        call the above line in your app page class method and reference the template you would
        like to load. 
        </p>
        <h3>Sample Template File</h3>
        <pre>
            <?=str_replace("<br />","",highlight_file("_template.example",true))?>
        </pre>
        <p>
        The important pieces parts of this file to keep in mind are:
        </p>
        <strong>$page = new SiteTemplate;</strong>
        <p>
        The SiteTemplate should be invoked in each template you plan to use for your site.
        It loads all of the client-side files (jQuery,xajax,etc.). 
        </p>
        <strong>$template->pushHeader($string);</strong>
        <p>
        pushHeader will load $string into the &lt;head&gt;&lt;/head&gt; section of the html document.
        </p>
        <strong>$template->pushReadyFunc();</strong>
        <p>
        pushReadyFunc will load any jQuery calls you'd like to make into a pre-defined 
        $(document).ready(function(){}); 
        in the &lt;head&gt;&lt;/head&gt; section of the html document.
        </p>
        <strong>$_TEMP['main'];</strong>
        <p>
        This is where the output of any of the registered apps, the 404 page or any other static pages
        you created will go. Just invoke $_TEMP["main"] anywhere in your template to make it work. 
        </p>
        <strong>$page->pushBody($string);</strong>
        <p>
        pushBody is where the template design is placed in the &lt;body&gt;&lt;/body&gt; tags. 
        </p>
    </section>
    <div id="controls">
        <div style="float: left;"><a href="one.php">Previous</a></div>
        <div style="float: right;"><a href="three.php">Next</a></div>
        <div style="clear: both;"></div>
    </div>
</div>
</body>