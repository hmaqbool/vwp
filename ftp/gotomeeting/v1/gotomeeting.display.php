<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingDisplay extends DashboardAppDisplay
{
	public static $urlVars = array();

/*
    public function admin($data,$page,$pages)
    {
        $html = new htmlBlock;
        $html
            ->push(new basicH1)
                ->append("GoToMeeting Management")
                ->ascend()
            ->push(new basicDiv)
                ->push("<p> Please press the following button to get authentication token from GoToMeeting service: </p>")
            ;
        
        return $html;
    }
*/

    public function test($data,$page,$pages)
    {
        $html = new htmlBlock;
        $html
            ->push(new basicH1)
                ->append("GoToMeeting Api")
                ->ascend()
            ->push(new basicDiv)
                ->push($data['access_token'])
            ->push(new basicDiv)
                ->push($data['organizer_key'])
            ;
        
        return $html;
    }
    
}

?>