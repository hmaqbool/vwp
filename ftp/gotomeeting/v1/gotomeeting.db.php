<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class GoToMeetingDB 
{
    public static $Contact = array(
		"tableName"=>"aw_contact",
		"schema"=>array(
				"submissionID" =>array("type"=>"int(10) unsigned","null"=>"NO","autoIncrement"=>"auto_increment","field"=>"int","form"=>"text",),
				"fName" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"lName" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"name" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"addr1" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"addr2" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"city" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"state" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"country" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"email" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"phone" =>array("type"=>"varchar(255)","null"=>"NO","field"=>"int","form"=>"text",),
				"isDel" =>array("type"=>"tinyint(1)","null"=>"NO","field"=>"tinyint","form"=>"radio",)
		),
		"minfields"=>array(),
		"uniquefields"=>array(),
        "singular"=>"Contact",
        "primary"=>"submissionID",
        "keys"=>"",
	);
	
	public static $Token = array(
		"tableName"=>"tokens",
		"schema"=>array(
				"id" =>array("type"=>"int(11)","null"=>"NO","autoIncrement"=>"auto_increment",),
				"fname" =>array("type"=>"varchar(255)","null"=>"NO",),
				"lname" =>array("type"=>"varchar(255)","null"=>"NO",),
				"email" =>array("type"=>"varchar(255)", "null"=>"NO",),
				"access_token" =>array("type"=>"varchar(255)","null"=>"NO",),
				"organizer_key" =>array("type"=>"varchar(255)","null"=>"NO",),
				"account_key" =>array("type"=>"varchar(255)","null"=>"NO",),
				"account_type" =>array("type"=>"varchar(255)","null"=>"NO",),
				"issue_time" =>array("type"=>"varchar(255)","null"=>"NO",),
				"expires_in" =>array("type"=>"varchar(255)","null"=>"NO",)
		),
		"minfields"=>array(),
		"uniquefields"=>array(),
		"singular"=>"Token",
		"primary"=>"id",
		"keys"=>"",
	);

}

?>