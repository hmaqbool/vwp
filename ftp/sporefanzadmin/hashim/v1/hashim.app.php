<?php

/**
 * @author Imran Media, Inc.
 * @copyright 2011
 */

class HashimApp extends GenericApp
{
	protected  $class_logic = "HashimLogic";
	protected  $class_display = "HashimDisplay";
	protected  $class_ajax = "HashimAjax";
	protected  $class_db = "HashimDB";
	protected  $class_css = "HashimCSS";
	protected  $class_page = "HashimPage";

	protected static $appInstance;
	
    public static $concern = "hashim";
    public static $name = "Hashim Contact App";
    public static $icon = "http://global.imranmedia.com/images/agg-icons/gig/affiliate_sales.png";

	public function __construct($db,$path,$vars=array())
	{
		parent::__construct($db,$path,$vars);
	}
	public static function getObj($db,$path,$arr=array())
	{
        if (!isset(self::$appInstance))
        {
            $c = __CLASS__;
            self::$appInstance = new $c($db,$path,$arr);
        }
        return self::$appInstance;
	}
	public function __varInit($arr=array())
	{
        return parent::__varInit( array_merge(
        	array(
                /*
                "collections" => array('Press'=>"PR Type"),
				"types" => array(
                                "Press"=>array("header"=>"Press Release Collections","type"=>"PR Collection","types"=>"PR Collections"),
                            ),
				"prefs" =>array(
                    	"metaData"=>array('contact'=>"Press Releases Contact Person", 'about'=>'Company Information'),
                    ),
                */
		),$arr) );
	}
	public function getCrons()
    {
        //$this->crons[] = array("frequency"=>86400,"app"=>"Hashim","callback"=>"logicMethod","firstrun"=>0);
        return $this->crons;
    }
}

?>