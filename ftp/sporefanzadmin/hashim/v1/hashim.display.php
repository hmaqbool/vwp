<?php

/**
 * @author Imran Media, Inc.
 * @copyright 2011
 */

class HashimDisplay extends DashboardAppDisplay
{
	public static $urlVars = array();


    public function admin($data,$page,$pages)
    {
        $obj = $this->getManageObj();
        $obj
            ->regData($data,"data")
            ->setTitle("Manage Contact")
            ->setTopAdd("admin/add","Add Contact")
            ->pushTable("adminTable","th", "tr",array('td1 nameCol','td2 actionCol'))
                ->setHeader("Contact Table")
                ->pushHeadRow("Name","Actions")
                ->useData("data")
                ->doRowsWithObject("adminRow")
                ->ascend()
            ->pushMain($this->getPagination($page,$pages))
        ;
        return $obj;
    }
    public function adminRow($v,$table,$obj)
    {
        $table->pushRow($v["fName"].' '.$v['lName'], $this->adminRowActions($v));
    }
    public function adminRowActions($v)
    {
        $html = $this->getRowActionsObj($v["submissionID"]);   
        $html
            ->addView("admin/view/" ,"View Contact")
            ->addEdit("admin/edit/","Edit Contact")
            ->addAjaxNoConfirm("deleteContact","basicDel","Delete Contact");
            
        return $html;
    }
    public function adminAdd($data=array())
    {
        $mf = $this->getAddFormObj();
        $mf
            ->setHeader("Contact")
            ->setBackLink("admin/","Back To Manage Contact")
            ->setAjaxFunc('addContact')
            ->setData($data)
            ->addHiddenPrimary("submissionID");
        
        $mf
            ->newRow("fName")
                        ->appendIText("fName")
            ->newRow("lName")
			->appendIText("lName")
            ->newRow("name")
			->appendIText("name")
            ->newRow("addr1")
			->appendIText("addr1")
            ->newRow("addr2")
			->appendIText("addr2")
            ->newRow("city")
			->appendIText("city")
            ->newRow("state")
			->appendIText("state")
            ->newRow("country")
			->appendIText("country")
            ->newRow("email")
			->appendIText("email")
            ->newRow("phone")
			->appendIText("phone");
        
        $mf
            ->newRow("Submit")
            ->pushISubmit();
        
        return $mf;
    }
    public function adminView($arr)
    {
        $html = new htmlBlock;
        $html
            ->push(new basicH1)
                ->append("View Contact")
                ->ascend()
            ->push(new divLink)
                ->setLink($this->getPathX() ."admin/","Back to Manage Contact")
                ->ascend()
            ->push(new divLink)
                ->setLink($this->getPathX()."admin/edit/id_" .$arr["submissionID"],"Edit Record");
        
        foreach($arr as $k=>$v)
        {
            $html
                ->push(new basicDiv)
                    ->push("<strong>" .$k ."</strong>: " .$v);
        }
        
        return $html;
    }
    
}

?>