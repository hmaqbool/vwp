<?php

/**
 * @author Imran Media, Inc.
 * @copyright 2011
 */

class HashimPage extends DashboardAppPage
{

    public function admin()
    {
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $this->regStdAjax("deleteContact","deleteContact","Deleted Completed.","Success.");
        $this->doAjax();
        $arr = $logic->getAllContact();
        $this->regDisplay("admin",$arr);
    }
    public function admin_add()
    {
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $this->regStdAjax("addContact","addContact","Contact successfully added","Success.",
                            array("path"=>$this->getPathX()."admin"));
        $this->doAjax();
        $this->regDisplay("adminAdd");
    }
    public function admin_edit()
    {
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $id = $this->getID();
        $arr = $logic->getContactByID($id);
        if($id==0 || count($arr)==0)
        {
            header("location:".$this->getPathX()."admin");
        }
        
        $this->regStdAjax("addContact","updateContact","Contact successfully edited","Success.",
                            array("path"=>$this->getPathX()."admin"));
        $this->doAjax();
        $this->regDisplay("adminAdd",$arr);
    }
    public function admin_view()
    {
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $id = $this->getID();
        $arr = $logic->getContactByID($id);
        if($id==0 || count($arr)==0)
        {
            header("location:".$this->getPathX()."admin");
        }
        
        $this->doAjax();
        $this->regDisplay("adminView",$arr);
    }
    
}

?>