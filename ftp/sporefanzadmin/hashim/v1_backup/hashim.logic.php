<?php

/**
 * @author Imran Media, Inc.
 * @copyright 2011
 */

class HashimLogic extends DashboardAppLogic
{

	// Contact CRUD Methods
    public function addContact($arr)
    {
        return $this->_addTable("Contact",$arr);
    }
    public function updateContact($arr)
    {
        return $this->_updateTable("Contact",$arr);
    }
    public function deleteContact($arr)
    {
        return $this->_deleteContact("Contact",$arr);
    }
    public function getContactByID($id)
    {
        return $this->_getTableByID("Contact",$id);
    }
    public function getAllContact()
    {
        return $this->_getAllTable("Contact");
    }
    public function getPagedContact($page,$length=10)
    {
        return $this->_getPagedTable("Contact",$page,$length);
    }
    public function getIndexedContact()
    {
        return $this->_getIndexedTable("Contact");
    }
}

?>