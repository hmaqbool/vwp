<?
/*
$sc
    ->sitepath("sites/hashim/dev")
    ->template("blank.php")
    ->addDB("HOST","DB","USER","Pass")
    ->addApp("Session",4,'rohan')->registerApp("_session","session")
;
*/
global $_CONF, $_TEMP, $_LANG, $classArr;

$sc = SiteConfig::getObj();

$sc
    ->sitePath("icf")
    ->template("blank.php")
    ->siteTitle('Hashim Dev Site')
    //->s3Bucket("iminc-icf")
    //->mailDomain("venturehealth.com")
    //->setCONF("AWSMailDomain", 'info@venturehealth.com')
    //->setCONF("rss_title", "Healthy Returns")
    //->setCONF("siteURLFull", "/blog/")
    ->addClasses(
            array(
                'DashboardAppLogic'=>'rohan/dbextends/v4/dbapplogic.class.php',
            	'DashboardAppPage'=>'rohan/dbextends/v4/dbapppage.class.php',
            	'DashboardAppDisplay'=>'rohan/dbextends/v4/dbappdisplay.class.php',

                //'CompleteMenu'=>'rohan/classes/dropdownmenu.php',
                        
                "HashimBlocks" 	=> "cms/hashim.blocks.php",
                "HashimLayouts" 	=> "cms/hashim.layouts.php",
                
                "CMSBlocks" => "rohan/newcms/v9/newcms.blocks.php",
                "CMSLayouts" => "rohan/newcms/v9/newcms.layout.php",
            )
    )
    ->addDB("mysql51-031.wc2.dfw1.stabletransit.com","444787_hashim","444787_hashim","F0l10Three")
    
    ->addDashGroup('admin','Site Admin Tools')
    ->addDashGroup('utils','Utilities')
    ->regApp("Dashboard",6,"rohan","_")->dashGroup("admin")
    
    ->regApp("SiteTypes",2,"rohan","_types")->dashGroup("utils")
    ->regApp("Cron",2,"","_cron")->dashGroup("utils")
    ->regApp("Scheduler",2,"rohan","_schedule")->dashGroup("utils")
    ->regApp("Mail",1,"rohan","_mail")->dashGroup("utils")
    
;

class AppConfigs extends DefaultAppConfigs
{
    public static function NewCMS()
    {
        return array
        (
        	"blocksClass" => "ICFBlocks",
    		"layoutClass" => "ICFLayouts",
            'menuDispDepth'=> 0,
        );
    }
}

?>