<?
global $_CONF, $_TEMP, $_LANG, $classArr;

$sc = SiteConfig::getObj();
$sc
	->sitePath("sites/hashim/dev")
    //->dashboardMode()
    ->forceLogin()
    ->setCONF("defaultLogin",Vars::getCONF("rootPath") ."sites/demo/vwpdemo/admin/templates/login.php")
    ->setCONF("sDisplayBlocksPath",$_CONF["rootPath"] ."sites/demo/vwpdemo/admin/templates/")
    ->setCONF("sLogicPath",$_CONF["rootPath"] ."sites/demo/vwpdemo/admin/logic/")
    ->setCONF('sTemplatesPath', $_CONF["rootPath"] ."sites/demo/vwpdemo/admin/templates/")
    ->setCONF("logoLoc",'http://a.wethrive.com/ext/im/vwpnew/images/logo-5.gif')
    ->setCONF('localUploadDir', "/mnt/stor1-wc2-dfw1/444787/rohan.wethrive.com/web/content/upload/")
    ->setCONF("critYearOffset", 3)
    
    ->devSite()
    
    ->template("hello.php")
    ->CDN("http://cdn.incubelabs.com/")
    ->s3Bucket("iminc-goneg")
    ->mailDomain("imranmedia.com")
    ->setCONF("AWSMailDomain", 'talat@imranmedia.com')
    ->addClasses(
        array(
            'DashboardAppLogic'=>'rohan/dbextends/v4/dbapplogic.class.php',
            'DashboardAppPage'=>'rohan/dbextends/v4/dbapppage.class.php',
            'DashboardAppDisplay'=>'rohan/dbextends/v4/dbappdisplay.class.php',

            "ICLBlocks" 	=> "cms/icl.blocks.php",
            "ICLLayouts" 	=> "cms/icl.layouts.php",

            "CMSBlocks" => "rohan/newcms/v9/newcms.blocks.php",
            "CMSLayouts" => "rohan/newcms/v9/newcms.layout.php",

            "RowActions"=>"rohan/chainableclasses/layouts.chainable.php",
            "ManageLayout"=>"rohan/chainableclasses/layouts.chainable.php",
            "manageTable"=>"rohan/chainableclasses/layouts.chainable.php",
            "newPseudoTable"=>"rohan/chainableclasses/layouts.chainable.php",


            'DealsCommonAjax'=>'rohan/icf/dealscommon/v1/dealscommon.ajax.php',
            'DealsCommonCSS'=>'rohan/icf/dealscommon/v1/dealscommon.css.php',
            'DealsCommonDisplay'=>'rohan/icf/dealscommon/v1/dealscommon.display.php',
            'DealsCommonPage'=>'rohan/icf/dealscommon/v1/dealscommon.page.php',

            "ManageAdminForm"=>"rohan/chainableclasses/add.manage.php",

            'SiteDelegate'=>'site.delegate.php',

            'CompleteMenu'=>'rohan/classes/dropdownmenu.php',
            )
    )
    ->addDB("mysql51-031.wc2.dfw1.stabletransit.com","444787_hashim","444787_hashim","F0l10Three")
    
    ->addS3Bucket("iminc-goneg","http://d1iylknx45zy01.cloudfront.net/")
    
    ->addDashGroup('admin','Site Admin Tools')
    ->addDashGroup('utils','Utilities')
	->addDashGroup('test','Test123')
    
    ->regApp("Dashboard",6,"rohan","")->dashGroup("admin")
    ->regApp("Hashim",1,"hashim","hashim")->dashGroup("admin")
    ->regApp("Session",5,"")->dashGroup("utils")
    ->regApp("SiteTypes",2,"rohan","types")->dashGroup("utils")
    ->regApp("Cron",2,"","cron")->dashGroup("utils")
    ->regApp("Scheduler",2,"rohan","schedule")->dashGroup("utils")
    ->regApp("Mail",1,"rohan","mail")->dashGroup("utils")
    
;

class AppConfigs extends DefaultAppConfigs
{
    public static function NewCMS()
    {
        return array
        (
        	"blocksClass" => "ICFBlocks",
    		"layoutClass" => "ICFLayouts",
        );
    }
}

?>