<?
global $_TEMP,$_CONF,$template;

$script = ScriptLoader::getObj();
$link = LinkLoader::getObj();

$script->getJS();
$link->getJS();

$compress = "";
if($_SERVER["HTTP_ACCEPT_ENCODING"])
{
    $compress = ".jgz";
}

$page = new SiteTemplate;
$page->setOpt("feedback",false);
$page->setOpt("css","/style.css");
$page->setOpt("readyJS","ready12.js");
$page->setOpt("compressJS",true);
$page->setOpt("jQuery",'1.5.2');
$page->setOpt("icon","favicon2.png");

ob_start();
?>
<?
$template->pushHeader(ob_get_clean());

$page->pushBody($_TEMP["main"]);
echo $page;
?>