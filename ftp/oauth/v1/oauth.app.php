<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class OAuthApp extends GenericApp
{
    protected  $class_logic = "OAuthLogic";
    protected  $class_display = "OAuthDisplay";
    protected  $class_ajax = "OAuthAjax";
    protected  $class_db = "OAuthDB";
    protected  $class_css = "OAuthCSS";
    protected  $class_page = "OAuthPage";

    protected static $appInstance;

    public static $concern = "oauth";
    public static $name = "OAuth Management";
    public static $icon = "http://global.imranmedia.com/images/agg-icons/gig/affiliate_sales.png";

    public function __construct($db,$path,$vars=array())
    {
        parent::__construct($db,$path,$vars);
    }
    public static function getObj($db,$path,$arr=array())
    {
        if (!isset(self::$appInstance))
        {
            $c = __CLASS__;
            self::$appInstance = new $c($db,$path,$arr);
        }
        return self::$appInstance;
    }
    public function __varInit($arr=array())
    {
        return parent::__varInit( array_merge(
            array(
            /*
            "collections" => array('Press'=>"PR Type"),
                            "types" => array(
                            "Press"=>array("header"=>"Press Release Collections","type"=>"PR Collection","types"=>"PR Collections"),
                        ),
                            "prefs" =>array(
                    "metaData"=>array('contact'=>"Press Releases Contact Person", 'about'=>'Company Information'),
                ),
            */
            ),$arr) );
    }
    public function getCrons()
    {
        //$this->crons[] = array("frequency"=>86400,"app"=>"Hashim","callback"=>"logicMethod","firstrun"=>0);
        return $this->crons;
    }
}

?>