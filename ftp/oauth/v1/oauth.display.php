<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class OAuthDisplay extends DashboardAppDisplay
{
	public static $urlVars = array();


    public function admin($data,$page,$pages)
    {
        $html = new htmlBlock;
        $html
            ->push(new basicH1)
                ->append("OAuth Management")
                ->ascend()
            ->push(new basicDiv)
                //->push("<p> Please press the following button to get authentication token from oAuth service: </p>")
                //->push("<div>")
                //->push("<input type='button' value='Authentication with oAuth Service' onclick=\"popupwindow('authenticate.php', 'oAuth Authentication', 800, 500);\" />")
                //->push("</div>")
                ->push("<br />")
                ->push(new divLink)
                    ->setLink($this->getPathX() ."admin/workspaces","Workspace Management")
                ->push("</div>")
                ->push(new divLink)
                    ->setLink($this->getPathX() ."admin/events","Event Management")
                ->push("</div>")
                ->push(new divLink)
                    ->setLink($this->getPathX() ."admin/rolemaps","Role Management")
                ->push("</div>")
                ->push(new divLink)
                    ->setLink($this->getPathX() ."admin/users","User Management")
                ->push("</div>")
                ->push(new divLink)
                    ->setLink($this->getPathX() ."admin/invitations","Invitation Management")
                ->push("</div>")
            ;
        
        return $html;
    }
    
    //public function workspace($data,$page,$pages)
    public function workspace($arrWorkspaces)
    {
        $html = new htmlBlock;
        $html
            ->push(new basicH1)
                ->append("Workspace Management")
                ->ascend()
            ->push(new basicDiv)
                ->push("<table>")
                    ->push("<tr>")
                        ->push("<td>New Workspace: </td>")
                        ->push("<td><input type='textbox' id='newWorkspace' value='' /> </td>")
                        ->push("<td align='right'><input type='button' id='btnCreate' value='Create' /></td>")
                    ->push("</tr>")
                ->push("</table>")
            ->push("</div>")
                
            ->push('<div style="font-size: 14px; width: 700px; font-style: italic; margin-bottom: 3px;">Note: There are only 2 workspaces are allowed to create. if you want to create a new workspace then delete any existing workspace and then create a new one. </div>')
            ->push('<div style="border: 1px solid lightgray; padding: 20px; width: 700px;  margin-bottom: 50px;">')
                ->push('<div class="wrap" style="width: 100%; height: 200px;">')
                    ->push('<div style="font-size: 14px; font-style: italic; margin-bottom: 3px;"> Select any workspace to view its childs.</div>')
                    ->push('<table id="table" cellpadding="0" cellspacing="0">')
                    ;
        
                    foreach ($arrWorkspaces['items'] as $workspace) 
                    {
                        $html
                            ->push('<tr>')
                                ->push('<td style="width: 400px;"><a href="workspaceFilesAndFolders/workspaceId_'. $workspace['workspace']['id'] .'/rootFolderId_'. $workspace['workspace']['root_folder_id'] .'">'. $workspace['workspace']['name'] .'</a></td>')
                                ->push('<td style="width: 80px; text-align: center;"><a class="delete" href="deleteWorkspace/id_'.$workspace['workspace']['id'] .'">Delete</a></td>')
                            ->push('</tr>')
                                ;
                    }

                    $html
                        ->push("</table>")
                ->push("</div>")
            ->push("</div>")
            ;
                    
        return $html;
    }
    
    public function workspaceFilesAndFolders($arrWorkspaces)
    {
        $html = new htmlBlock;
        $html
            ->push(
                "<div style='width: 100%; height: 100%;'>
                    <div class='wrap' style='width: 100%; height: 90%;'>

                        <h1>Folders & Files Management (OAuth)</h1>
                        <img id='imgLoading' src='../../../resources/images/loading2.gif' style='display: none' />

                        <div style='border: 1px solid lightgray; padding: 20px; width: 700px; margin-bottom: 10px;'>
                            <table>
                                <tr>
                                    <td>New Folder: </td>
                                    <td><input type='textbox' id='newFolder' value='' /> </td>
                                    <td align='right'><input type='button' id='btnCreate' value='Create' /></td>
                                </tr>
                            </table>
                        </div>

                        <div style='border: 1px solid lightgray; padding: 20px; width: 700px; margin-bottom: 10px;'>
                            <form action='fileUpload.php' method='post' enctype='multipart/form-data'>
                                <table>
                                    <tr>
                                        <td>Browse for a file to upload:</td>
                                        <td>
                                            <input type='file' id='file' name='file' size='100'/>
                                            <input type='hidden' name='folderId' id='folderId' />
                                            <input type='hidden' name='workspaceId' value='".$_GET['workspaceId'] ."' />
                                            <input type='hidden' name='rootFolderId' value='". $_GET['rootFolderId'] ."' />
                                        </td>
                                        <td align='right'><input type='submit' name='submit' value='Upload' /></td>
                                    </tr>
                                </table>
                            </form>
                        </div>

                        <div style='border: 1px solid lightgray; padding: 20px; width: 700px;'>
                            <!-- // Ancestor IDs -->
                            <p>Folders & Files List:</p>
                            <table id='folderTable' cellpadding='0' cellspacing='0'>");
            
                            foreach ($arrFolders['items'] as $folders)
                            {
                                $html->push("<tr>");
                                
                                if (isset($folders['folder'])) 
                                {
                                    $html->push("
                                        <td style='width: 400px;'><a href='index.php?id=" . $folders['folder']['id'] . "&workspaceId=" . $_GET['workspaceId'] . "&rootFolderId=" . $_GET['rootFolderId'] ."'>" . $folders['folder']['filename'] . "</a></td>
                                        <td style='width: 80px; text-align: center;'><a class='update' href='folderUpdate.php?id=" . $folders['folder']['id'] . "'>Update</a></td>
                                        <td style='width: 80px; text-align: center;'><a title='folder' class='delete' href='folderDelete.php?id=" . $folders['folder']['id'] . "'>Delete</a></td>");
                                    
                                }
                                else if (isset($folders['file'])) 
                                {
                                    $html->push("
                                        <td style='width: 400px;'><a href='fileDownload.php?id=" . $folders['file']['id'] . "&workspaceId=" . $_GET['workspaceId'] . "'>" . $folders['file']['filename'] . "</a></td>
                                        <td style='width: 80px; text-align: center;'><a class='update' href='fileUpdate.php?id=" . $folders['file']['id'] . "'>Update</a></td>
                                        <td style='width: 80px; text-align: center;'><a title='file' class='delete' href='fileDelete.php?id=" . $folders['file']['id'] . "'>Delete</a></td>");
                                }
                                
                                $html->push("</tr>");
                            }
                            
                            $html->push("</table>
                                    </div>
                                </div>
                                <div style='width: 100%; height: 8%; text-align: center;'>
                                        <input type='button' onclick='javascript:window.close();' value='Close' />
                                </div>
                            </div>");
                    
        return $html;
    }
    
    public function adminRow($v,$table,$obj)
    {
        $table->pushRow($v["fName"].' '.$v['lName'], $this->adminRowActions($v));
    }
    public function adminRowActions($v)
    {
        $html = $this->getRowActionsObj($v["submissionID"]);   
        $html
            ->addView("admin/view/" ,"View Contact")
            ->addEdit("admin/edit/","Edit Contact")
            ->addAjaxNoConfirm("deleteContact","basicDel","Delete Contact");
            
        return $html;
    }
    public function adminAdd($data=array())
    {
        $mf = $this->getAddFormObj();
        $mf
            ->setHeader("Contact")
            ->setBackLink("admin/","Back To Manage Contact")
            ->setAjaxFunc('addContact')
            ->setData($data)
            ->addHiddenPrimary("submissionID");
        
        $mf
            ->newRow("fName")
                        ->appendIText("fName")
            ->newRow("lName")
			->appendIText("lName")
            ->newRow("name")
			->appendIText("name")
            ->newRow("addr1")
			->appendIText("addr1")
            ->newRow("addr2")
			->appendIText("addr2")
            ->newRow("city")
			->appendIText("city")
            ->newRow("state")
			->appendIText("state")
            ->newRow("country")
			->appendIText("country")
            ->newRow("email")
			->appendIText("email")
            ->newRow("phone")
			->appendIText("phone");
        
        $mf
            ->newRow("Submit")
            ->pushISubmit();
        
        return $mf;
    }
    public function adminView($arr)
    {
        $html = new htmlBlock;
        $html
            ->push(new basicH1)
                ->append("View Contact")
                ->ascend()
            ->push(new divLink)
                ->setLink($this->getPathX() ."admin/","Back to Manage Contact")
                ->ascend()
            ->push(new divLink)
                ->setLink($this->getPathX()."admin/edit/id_" .$arr["submissionID"],"Edit Record");
        
        foreach($arr as $k=>$v)
        {
            $html
                ->push(new basicDiv)
                    ->push("<strong>" .$k ."</strong>: " .$v);
        }
        
        return $html;
    }
    
}

?>