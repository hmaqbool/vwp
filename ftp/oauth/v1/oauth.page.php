<?php

/**
 * @author Folio3 Private Ltd.
 * @copyright 2013
 */

class OAuthPage extends DashboardAppPage
{

    public function admin()
    {
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $this->regStdAjax("deleteContact","deleteContact","Deleted Completed.","Success.");
        $this->doAjax();
        //$arr = $logic->getAllContact();
        $this->regDisplay("admin",$arr);
    }
    
    public function admin_workspaces()
    {
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $this->regStdAjax("deleteContact","deleteContact","Deleted Completed.","Success.");
        $this->doAjax();
        
        $objCommon = Common::getObj();
        $client = $objCommon->getHttpClient();

        $apiUri = "https://ws-api.onehub.com/workspaces";
        $response = $client->get($apiUri)->send();

        $arrWorkspaces = $response->json();
        
        //px($arrWorkspaces);
        
        //$arr = $logic->getAllContact();
        
        /*
        $arrWorkspaces['items'][] = array(
            'workspace' => array('id'=>1, 'root_folder_id'=>2, 'name'=>'workspace1')
        );
        $arrWorkspaces['items'][] = array(
            'workspace' => array('id'=>1, 'root_folder_id'=>2, 'name'=>'workspace2')
        );
        $arrWorkspaces['items'][] = array(
            'workspace' => array('id'=>1, 'root_folder_id'=>2, 'name'=>'workspace3')
        );
         * 
         */
        
        $this->regDisplay("workspace", $arrWorkspaces);
    }
    
    public function admin_deleteWorkspace()
    {        
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        //$this->regStdAjax("deleteContact","deleteContact","Deleted Completed.","Success.");
        //$this->doAjax();
        
        if (empty($this->getId())) {
            return false;
        }
        
        $objCommon = Common::getObj();
        
        // TODO: If client object already exist then get that one.
        $client = $objCommon->getHttpClient();

        $apiUri = "https://ws-api.onehub.com/workspaces/{$this->getId()}";
        $request = $client->delete($apiUri);
        $request->getParams()->set('redirect.disable', true);
        $request->send();
        
        // TODO: Display message to user either success of failure.
    }
    
    public function admin_workspaceFilesAndFolders()
    {      
        error_reporting(E_ALL);
        ex("Asdgasdg");
        
        
        
        /*
        $logic = $this->getLogic();
        
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        
        echo "Asdgasgdasdg";
         * 
         */

        /*
        
        if (empty($this->getVar('workspaceId'))) {
            echo "Error missing parameter workspaceId";
            return;
        }

        if (empty($this->getVar('rootFolderId'))) {
            echo "Error missing parameter Root Folder Id";
            return;
        }
        
        // Override default folder id if exist in url.
        if (empty($this->getId)) {
            $apiUri = "https://ws-api.onehub.com/workspaces/{$this->getVar('workspaceId')}/folders/{$this->getId()}.json";
        }
        else {
            $apiUri = "https://ws-api.onehub.com/workspaces/{$this->getVar('workspaceId')}/files.json";
        }
        
        $objCommon = Common::getObj();
        // TODO: If client object already exist then get that one.
        $client = $objCommon->getHttpClient();
        
        $response = $client->get($apiUri)->send();
        //print_r($response->json()); exit;

        $arrFolders = $response->json();
        
        px($arrFolders);

        // TODO: Display message to user either success of failure.
        $this->regDisplay("workspaceFilesAndFolders",$arr);
         * 
         */
        
        
        $this->regDisplay("workspaceFilesAndFolders",$arr);
    }
    
    public function admin_add()
    {
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $this->regStdAjax("addContact","addContact","Contact successfully added","Success.",
                            array("path"=>$this->getPathX()."admin"));
        $this->doAjax();
        $this->regDisplay("adminAdd");
    }
    
    public function admin_edit()
    {
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $id = $this->getID();
        $arr = $logic->getContactByID($id);
        if($id==0 || count($arr)==0)
        {
            header("location:".$this->getPathX()."admin");
        }
        
        $this->regStdAjax("addContact","updateContact","Contact successfully edited","Success.",
                            array("path"=>$this->getPathX()."admin"));
        $this->doAjax();
        $this->regDisplay("adminAdd",$arr);
    }
    
    public function admin_view()
    {
        $logic = $this->getLogic();
        if(!$logic->isAdmin())
        {
            $this->doAjax();
            $this->regDisplay("access_denied");
        }
        $id = $this->getID();
        $arr = $logic->getContactByID($id);
        if($id==0 || count($arr)==0)
        {
            header("location:".$this->getPathX()."admin");
        }
        
        $this->doAjax();
        $this->regDisplay("adminView",$arr);
    }
    
}

?>