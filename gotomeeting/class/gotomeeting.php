<?php
class CitrixAPI {

	public $_organizerKey;
	public $_accessToken;

	public function __construct ($_accessToken = null, $_organizerKey = null) {
		$this->_accessToken = $_accessToken;
		$this->_organizerKey = $_organizerKey;
	}

	public function getOAuthToken ($_apiKey = null, $_callbackUrl = null) {
		if (isset($_GET['authorize']) && (int)$_GET['authorize'] == 1) {
			include 'redirect.php';
		}

		if (isset($_GET['code'])) {
			$url = 'https://api.citrixonline.com/oauth/access_token?grant_type=authorization_code&code='. $_GET['code'] .'&client_id='. $_apiKey;
			return $this->makeApiRequest($url);
		}
	}

	/**
	 * @name getAttendeesByOrganizer
	 * @desc GoToMeeting API
	 */

	public function getAttendeesByOrganizer () {
		$url  = 'https://api.citrixonline.com/G2M/rest/organizers/'. $this->_organizerKey .'/attendees';
		$url .= '?startDate='. date('c');
		$url .= '?endDate='. date('c', strtotime("-7 Days"));

		return $this->makeApiRequest($url, 'GET', array(), $this->getJsonHeaders());
	}


	/*  created by Adil Abbasi */
	public function CreateMeeting($subject, $start, $end, $conferencecallinfo, $meetingtype) {
		$url  = 'https://api.citrixonline.com/G2M/rest/meetings';
		$data = array(
			'subject'=>$subject,
			'starttime'=>$start,
			'endtime'=>$end, 
			'passwordrequired'=>'false', 
			'conferencecallinfo'=>$conferencecallinfo, 
			'timezonekey'=>'', 
			'meetingtype'=>$meetingtype
		);
		$data_json = json_encode ($data);

		return $this->makeApiRequest($url, 'POST', $data_json, $this->getJsonHeaders());
	}

	/*  created by Adil Abbasi */
	public function StartMeeting($m_id) {

		$url  = "https://api.citrixonline.com/G2M/rest/meetings/$m_id/start";
		return $this->makeApiRequest($url, 'GET', array(), $this->getJsonHeaders());
	}



	/*  created by Adil Abbasi */
	public function UpdateMeeting($mid, $subject, $start, $end, $conferencecallinfo, $meetingtype) {
		$url  = "https://api.citrixonline.com/G2M/rest/meetings/$mid";
		$data = array(
			'subject'=>$subject,
			'starttime'=>$start,
			'endtime'=>$end, 
			'passwordrequired'=>'false', 
			'conferencecallinfo'=>$conferencecallinfo, 
			'timezonekey'=>'', 
			'meetingtype'=>$meetingtype
		);
		$data_json = json_encode ($data);


		return $this->makeApiRequest($url, 'PUT', $data_json, $this->getJsonHeaders());
	}









	/*  created by Adil Abbasi */
	public function GetFutuerMeeting($start, $end) {

		$url  = 'https://api.citrixonline.com/G2M/rest/meetings';
		$data = array(
			'scheduled'=>'true',
			'starttime'=>$start,
			'endtime'=>$end, 
		);
		$data_json = json_encode ($data);


		return $this->makeApiRequest($url, 'GET', $data_json, $this->getJsonHeaders());
	}

	/*  created by Adil Abbasi */
	public function GetMeeting($m_id) {

		$url  = "https://api.citrixonline.com/G2M/rest/meetings/$m_id";
		return $this->makeApiRequest($url, 'GET', array(), $this->getJsonHeaders());
	}






	/*  created by Adil Abbasi */
	public function DeleteMeeting($meeting_id) {

		$url  = "https://api.citrixonline.com/G2M/rest/meetings/$meeting_id";
		//var_dump($url); die();
		return $this->curl_del($url, $this->getJsonHeaders());
	}

	/**
	 * @param String $url
	 * @param String $requestType
	 * @param Array $postData
	 * @param Array $headers
	 */

	public function makeApiRequest ($url = null, $requestType = 'GET', $postData = array(), $headers = array()) {

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	    if ($requestType == 'POST') {
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	    }else if($requestType == 'PUT'){
	    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	    	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	    }

	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    $data = curl_exec($ch);

	    $validResponseCodes = array(200, 201, 409);
	    $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 

	    if($requestType == 'PUT'){
	    	return $responseCode;
	    }
	    
	    if (curl_errno($ch)) {
	    	curl_close($ch);
	        return curl_error($ch);
	    }
	    elseif (!in_array($responseCode, $validResponseCodes)) {
	        if ($this->isJson($data)) {
	            $data = json_decode($data);
	        }
	    }

	    curl_close($ch);
	    return $data;
	}

	public function curl_del($url,$headers)
	{

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	    curl_setopt($ch, CURLOPT_URL,$url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

	    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);

	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    $result = curl_exec($ch);
	    
	    $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
	    curl_close($ch);

	    return $responseCode;
	}

	public function getJsonHeaders () {
		return array(
			"HTTP/1.1",
			"Content-type: application/json",
			"Accept: application/json",
			"Authorization: OAuth oauth_token=". $this->_accessToken
		);
	}

	public function isJson ($string) {
	    $isJson = 0;
	    $decodedString = json_decode($string);

	    if (is_array($decodedString) || is_object($decodedString)) {
	        $isJson = 1;
	    }

	    return $isJson;
	}
}
