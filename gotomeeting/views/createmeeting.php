<?php 

error_reporting(E_ALL);
ini_set('display_errors', 'On');


foreach (glob("../class/*.php") as $filename){
    include $filename;
}

/* including header html */
include '../header.php';
$msg ='';


if ($_SERVER['REQUEST_METHOD'] === 'POST') {    

    //$key  = '3b24077831e9cd626424aa4e7872da8d';
   // $url  = 'http://meeting.dev/';
    
    $db = DBSingleton::Instance();
    $user = $db->GetDbToken();

    $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);

    $subject = $_POST['subject'];
    $start_datetime = new DateTime($_POST['start']);
    $start = $start_datetime->format('c');
    $end_datetime = new DateTime($_POST['end']);
    $end = $end_datetime->format('c');
    $conferencecallinfo = $_POST['conference'];
    $meetingtype = $_POST['meetingtype'];


    if(isset($user['access_token']) ){
        $result = $citrix->CreateMeeting($subject, $start, $end, $conferencecallinfo, $meetingtype);
        $data = json_decode($result);
    } else{
        $msg = 'Please update token first';
    }
    if($data)
        $msg = 'Congratulations !!! meeting has been created';
    else{
        $msg = 'Something went wrong please try again (My be file with same name already exist)';
        die();
    }

    /* including footer html */
    include '../footer.php';


    echo '<div style=" width: 900px; auto; margin:0 auto; "><div style=" width: 900px; margin-top: 70px; height: auto; color: #343434; ">';
    foreach ($data as $arr) {
        foreach ($arr as $key =>$info) {
            echo '<div style="width: 100%;overflow: hidden;"><span style="margin-bottom: 20px; width: 130px; float: left;">'.$key.'</span><span style="margin-left: 100; float: left;">'.$info.'</span></div>';
        }
    }
    echo '</div></div>';

    if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
        $redir_url = 'http://yii.dev/views/create.php';
    }else{
        $redir_url = 'http://www.vwp.co/hashim/gotomeeting/views/create.php';
    }

    echo '<div  style=" width: 900px; height:auto; margin:0 auto;  overflow: hidden; ">
    <hr/>
    <p>To search again <a style="text-decoration: none; color: green;" href="'.$redir_url.'">click</a> here.</p>
    </div>';
}


?>
