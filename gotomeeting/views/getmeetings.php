<!DOCTYPE html>
<html>
  <head>
    <title>Go To Meeting</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" media="all" type="text/css" href="css/jquery-ui-timepicker-addon.css" />
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>

	<style>

	body{
		width: 100%; 
		height: auto;  
		border: none; 
		background-color: #EDE9D8; 
		padding: 0; margin:0;
	}

	#header_strip{
		width: 100%; 
		height: 66px; 
		overflow: hidden; 
		background-color: #343434;
	}

	#logo_continer{
		width: 900px; 
		height: auto; 
		margin:0 auto; 
		overflow: hidden;
	}

	#logo{
		width: 300px; 
		height: 100%; 
		overflow: hidden; 
		float: left; 
		color: white;
	}

	#update_btn{
		width: 130px; 
		height: 40 px; 
		overflow: hidden; 
		float: right; 
		color: white; 
		padding-top: 35px;
	}

	.form-group{
		width: 500px;
		padding-top: 50px;
	}

	#ui-datepicker-div, .ui-datepicker{ font-size: 85%; }

	</style>

	<script type="text/javascript">
		$(function(){
				/* $('#start').datetimepicker();*/
				$('#end').datetimepicker();
			});
	</script>

</head>


<body >
	<!-- Header -->
    <div id = "header_strip">
        <div id = "logo_continer">
            <div id = "logo">
                <h1>GoToMeeting Api</h1>
            </div>
            <div id = "update_btn">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://yii.dev/index.php?authorize=1">Update Token</a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://www.vwp.co/hashim/gotomeeting/index.php?authorize=1">Update Token</a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 130px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://yii.dev/views/getfuturemeetings.php">Future Meetings</a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://www.vwp.co/hashim/gotomeeting/views/getfuturemeetings.php">Future Meetings</a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/views/getmeetings.php">Delete </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/views/getmeetings.php">Delete </a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/views/meetingsupdate.php">Update </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/views/meetingsupdate.php">Update </a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/views/create.php">Create </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/views/create.php">Create </a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/">Home </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/index.php">Home </a>';
				    }
            	?>
            </div>

        </div>
    </div>
	<!-- Header -->

	<div style=" width: 900px; auto; margin:0 auto; ">
    <div style=" width: 900px; height: auto; text-align: center; color: #343434; ">
        <p><?php $msg='';echo $msg; ?></p>
    </div>

    <div style=" width: 900px; height: auto; color: #343434; margin-top: 60px; margin-left: 150px; ">
    	<h1 style="margin-bottom: -10px;">Search For Delete</h1>
    	<form role="form" method="POST" action="delmeetings.php">

		  <div class="form-group">
		    <span style="width: 150px; margin-top: 8px; float: left;"><label>Start Date Time</label></span>
		    <span style="width: 300px; float: left;">
		    	<input type="text" name="start" id="start_time" class="form-control" placeholder="Start Date Time" disabled value="<?php echo date("m/d/Y H:i"); ?>" /></span>
		  </div>

		  <div class="form-group">
		    <span style="width: 150px; margin-top: 8px; float: left;"><label>End Date Time</label></span>
		    <span style="width: 300px; float: left;">
		    	<input type="text" name="end" id="end" class="form-control" placeholder="End Date Time" /></span>
		  </div>

		  <div class="form-group" style="padding-top: 60px;">
		    <span style="width: 150px; margin-top: 8px; float: left;"></span>
		    <span style="float: left;">
		    	<input type="button" class="btn btn-default" onclick="location.reload();" value="Reset" />
		    </span>
		    <span style=" float: left; margin-left: 10px;">
		    	<input type="submit" class="btn btn-primary" value="Search" />
		    </span>
		  </div>
		  <input type="hidden" name="start" value="<?php echo date("m/d/Y H:i"); ?>" />
    	</form>
    </div>
</div>
</body>