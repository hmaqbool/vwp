<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

foreach (glob("../class/*.php") as $filename){
    include $filename;
}

/* including header html */
    include '../header.php';
    $msg ='';

/* including footer html */
    include '../footer.php';


if ($_SERVER['REQUEST_METHOD'] === 'POST') { 
	$mid = $_POST['meeting_id'];

	$db = DBSingleton::Instance();
    $user = $db->GetDbToken();

    $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);

    if(isset($user['access_token']) ){
        $result = $citrix->StartMeeting($mid);
        $data = json_decode($result);
    }

    if($data){
        echo '<div style=" width: 900px; auto; margin:0 auto; "><div style=" width: 900px; margin-top: 70px; height: auto; color: #343434; ">';
        foreach ($data as $arr) {
            echo '<div style="width: 100%;overflow: hidden;">
                    In order to start meeting <a style="color: green; text-decoration: none;"href="'.$arr.'">click here</a>.
                    </div>';
        }
        echo '</div></div>';
    }
    else{
        echo '<div style=" width: 900px; auto; margin:0 auto; ">
            <div style=" width: 900px; margin-top: 70px; height: auto; color: #343434;">
                Please try again later ! something went wrong.
            </div>
        </div>';

    }
}

?>