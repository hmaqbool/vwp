<?php 


    foreach (glob("../class/*.php") as $filename){
        include $filename;
    }

    $id = $_POST['title'];

    $db = DBSingleton::Instance();
    $user = $db->GetDbToken();

    $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);

    if(isset($user['access_token']) ){
        $result = $citrix->DeleteMeeting($id);
        if($result == 204)
            echo 'Success: Meeting has been deleted';
    } else{
        echo 'Error: Please update token first';
    }

?>