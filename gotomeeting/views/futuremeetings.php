<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script language ="javascript" type = "text/javascript" >
    function submitForm(id)
    {
       $("#meeting_id").val(id);
       $("#startform").submit();
    }
</script>
<?php 

error_reporting(E_ALL);
ini_set('display_errors', 'On');


foreach (glob("../class/*.php") as $filename){
    include $filename;
}

/* including header html */
include '../header.php';
$msg = '';


if ($_SERVER['REQUEST_METHOD'] === 'POST') {    

    //$key  = '3b24077831e9cd626424aa4e7872da8d';
   // $url  = 'http://meeting.dev/';
    
    $db = DBSingleton::Instance();
    $user = $db->GetDbToken();

    $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);

    $start_datetime = new DateTime($_POST['start']);
    $start = $start_datetime->format('c');

    $end_datetime = new DateTime($_POST['end']);
    $end = $end_datetime->format('c');



    if(isset($user['access_token']) ){
        $result = $citrix->GetFutuerMeeting($start,$end);
        $data = json_decode($result);
    } else{
        $msg = 'Please update token first';
    }
    if($data)
        $msg = '';

    /* including footer html */
    include '../footer.php';

//    var_dump($data);
    $searchs = array(0=>array());
    $index = 0;
    
    foreach ($data as $arr) {
        foreach ($arr as $key =>$info) {
            if($key == 'startTime' && strtotime($info) <= strtotime($_POST['start']) ){
                break;
            }

            if( $key == 'startTime' && strtotime($info) > strtotime($_POST['end']) ){
                break;
            }

            if($key == 'startTime'){
                $time = strtotime($info);
                $Started_at = date('Y-m-d H:i',$time);
                $searchs[$index][$key] = $Started_at;
                //echo '<div style="width: 100%;overflow: hidden;"><span style="margin-bottom: 20px; width: 130px; float: left;">'.$key.'</span><span style="margin-left: 100; float: left;">'.$Started_at.'</span></div>';
            }
            else if($key == 'endTime'){
                $time = strtotime($info);
                $Ended_at = date('Y-m-d H:i',$time);
                $searchs[$index][$key] = $Ended_at;
                //echo '<div style="width: 100%;overflow: hidden;"><span style="margin-bottom: 20px; width: 130px; float: left;">'.$key.'</span><span style="margin-left: 100; float: left;">'.$Ended_at.'</span></div>';
            }
            else{
                $searchs[$index][$key] = $info;
                //echo '<div style="width: 100%;overflow: hidden;"><span style="margin-bottom: 20px; width: 130px; float: left;">'.$key.'</span><span style="margin-left: 100; float: left;">'.$info.'</span></div>';
            }
        }
        $index++;
    }

    echo '<form id ="startform" method="POST" action="startmeeting.php">
            <input type="hidden" name="meeting_id" id="meeting_id" />
            <div style=" width: 900px; height:auto; margin:0 auto; border: 1px solid; overflow: hidden; ">
                <div style=" float: left; width: 250px; height:30px; padding-top: 15px; text-align: center; color: white; background-color:#343434; text-decoration: bold;  border: 1px; solid; ">
                    Subject
                </div>

                <div style=" float: left; width: 110px; height:30px; padding-top: 15px; text-align: center; color: white; background-color:#343434; text-decoration: bold;  border: 1px; solid; ">
                    Action
                </div>

                <div style=" float: left; width: 110px; height:30px; padding-top: 15px; text-align: center; color: white; background-color:#343434; text-decoration: bold;  border: 1px; solid; ">
                    Type
                </div>

                <div style=" float: left; width: 110px; height:30px; padding-top: 15px; text-align: center; color: white; background-color:#343434; text-decoration: bold;  border: 1px; solid; ">
                    Start
                </div>

                <div style=" float: left; width: 110px; height:30px; padding-top: 15px; text-align: center; color: white; background-color:#343434; text-decoration: bold;  border: 1px; solid; ">
                    End
                </div>

                <div style=" float: left; width: 210px; height:30px; padding-top: 15px; text-align: center; color: white; background-color:#343434; text-decoration: bold;  border: 1px; solid; ">
                    Conference Info
                </div>
            </div>';

    foreach ($searchs as $search) {
        if(isset($search['startTime'])){
            //var_dump($search);
            echo '<div style=" width: 900px; height:auto; margin:0 auto; border: 1px solid; border-top: none; overflow: hidden; ">
                <div style=" float: left; width: 249px; height: 49px; border-right: 1px solid; padding-top: 15px; text-align: center; color: #343434;">
                '.$search['subject'].'
                </div>

                <div style=" float: left; width: 109px; height: 49px; border-right: 1px solid; padding-top: 15px; text-align: center; color: #343434;">
                    <a href="#" style="color: green; text-decoration: none;"onclick="submitForm(\''.$search['uniqueMeetingId'].'\')" >Start Meeting</a>
                </div>

                <div style=" float: left; width: 109px; height: 49px; border-right: 1px solid; padding-top: 15px; text-align: center; color: #343434;">
                '.$search['meetingType'].'  
                </div>

                <div style=" float: left; width: 109px; height: 49px; border-right: 1px solid; padding-top: 15px; text-align: center; color: #343434;">
                '.$search['startTime'].'
                </div>

                <div style=" float: left; width: 109px; height: 49px; border-right: 1px solid; padding-top: 15px; text-align: center; color: #343434;">
                '.$search['endTime'].'
                </div>

                <div style=" float: left; width: 210px; height: 49px; padding-top: 15px; text-align: center; color: #343434;">
                '.$search['conferenceCallInfo'].'
                </div></div>';
        }
    }
    echo '</form>';
    
}


?>
