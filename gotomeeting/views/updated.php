<?php
foreach (glob("../class/*.php") as $filename){
    include $filename;
}

/* including header html */
include '../header.php';
$msg = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {    


    $db = DBSingleton::Instance();
    $user = $db->GetDbToken();

    $citrix = new CitrixAPI($user['access_token'],$user['organizer_key']);

    $mid = $_POST['meeting_id'];
    $subject = $_POST['subject'];
    $start_datetime = new DateTime($_POST['start']);
    $start = $start_datetime->format('c');
    $end_datetime = new DateTime($_POST['end']);
    $end = $end_datetime->format('c');
    $conferencecallinfo = $_POST['conference'];
    $meetingtype = $_POST['meetingtype'];


    if(isset($user['access_token']) ){
        $result = $citrix->UpdateMeeting($mid, $subject, $start, $end, $conferencecallinfo, $meetingtype);
    } else{
        $msg = 'Please update token first';
    }
    if($result == 204)
        $msg = 'Congratulations !!! meeting has been updated';

    /* including footer html */
    include '../footer.php';

    if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
        $redir_url = 'http://yii.dev/views/meetingsupdate.php';
    }else{
        $redir_url = 'http://www.vwp.co/hashim/gotomeeting/views/meetingsupdate.php';
    }

    echo '<div  style=" width: 900px; height:auto; margin:0 auto;  overflow: hidden; ">
    <hr/>
    <p>To search again <a style="text-decoration: none; color: green;" href="'.$redir_url.'">click</a> here.</p>
    </div>';
}
?>