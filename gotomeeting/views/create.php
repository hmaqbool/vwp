<!DOCTYPE html>
<html>
  <head>
    <title>Go To Meeting</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" media="all" type="text/css" href="css/jquery-ui-timepicker-addon.css" />
	
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="js/jquery-ui-sliderAccess.js"></script>

	<style>

	body{
		width: 100%; 
		height: auto;  
		border: none; 
		background-color: #EDE9D8; 
		padding: 0; margin:0;
	}

	#header_strip{
		width: 100%; 
		height: 66px; 
		overflow: hidden; 
		background-color: #343434;
	}

	#logo_continer{
		width: 900px; 
		height: auto; 
		margin:0 auto; 
		overflow: hidden;
	}

	#logo{
		width: 300px; 
		height: 100%; 
		overflow: hidden; 
		float: left; 
		color: white;
	}

	#update_btn{
		width: 130px; 
		height: 40 px; 
		overflow: hidden; 
		float: right; 
		color: white; 
		padding-top: 35px;
	}

	.form-group{
		width: 500px;
		padding-top: 50px;
	}

	#ui-datepicker-div, .ui-datepicker{ font-size: 85%; }

	</style>

	<script type="text/javascript">
		$(function(){
				$('#start').datetimepicker();
				$('#end').datetimepicker();
			});

		function CreateForm(){

			var ind = 1;
			var error = '';

			if($('#subject').val() == ''){
				error += ind+". Subject cant be blank. \n";
				ind++;
			}
			if($('#start').val() == ''){
				error += ind+'. Subject cant be blank. \n';
				ind++;
			}
			if($('#end').val() == ''){
				error += ind+'. Subject cant be blank. \n';
				ind++;
			}
			if($('#meetingtype').val() == ''){
				error += ind+'. Subject cant be blank. \n';
				ind++;
			}
			if($('#conference').val() == ''){
				error += ind+'. Subject cant be blank. \n';
				ind++;
			}

			var start = $('#start').val();
			var end = $('#end').val();

			start = start.replace("/", "");
			start = start.replace("/", "");
			start = start.replace(" ", "");
			start = start.replace(":", "");

			end = end.replace("/", "");
			end = end.replace("/", "");
			end = end.replace(" ", "");
			end = end.replace(":", "");

			var startint = parseInt(start);
			var endint = parseInt(end);

			if (startint >= endint) {
				error += ind+'. Start date time shoud be greater than end time.\n';
				ind++;
			}
			if(error == '')
				$("#CreateForm").submit();
			else
      			alert(error);
    	}
	</script>



</head>


<body >
	<!-- Header -->
    <div id = "header_strip">
        <div id = "logo_continer">
            <div id = "logo">
                <h1>GoToMeeting Api</h1>
            </div>
            <div id = "update_btn">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://yii.dev/index.php?authorize=1">Update Token</a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://www.vwp.co/hashim/gotomeeting/index.php?authorize=1">Update Token</a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 130px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://yii.dev/views/getfuturemeetings.php">Future Meetings</a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://www.vwp.co/hashim/gotomeeting/views/getfuturemeetings.php">Future Meetings</a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/views/getmeetings.php">Delete </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/views/getmeetings.php">Delete </a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/views/meetingsupdate.php">Update </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/views/meetingsupdate.php">Update </a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/views/create.php">Create </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/views/create.php">Create </a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/">Home </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/index.php">Home </a>';
				    }
            	?>
            </div>

        </div>
    </div>
	<!-- Header -->

	<div style=" width: 900px; auto; margin:0 auto; ">
    <div style=" width: 900px; height: auto; text-align: center; color: #343434; ">
        <p><?php $msg='';echo $msg; ?></p>
    </div>

    <div style=" width: 900px; height: auto; color: #343434; margin-top: 60px; margin-left: 150px; ">
    	<h1 style="margin-bottom: -10px;"> Create Meeting</h1>
    	<form id ="CreateForm" role="form" method="POST" action="createmeeting.php">
		  <div class="form-group">
		    <span style="width: 150px; margin-top: 8px; float: left;"><label>Subject</label></span>
		    <span style="width: 300px; float: left;"><input type="text" name="subject" id="subject" class="form-control"  placeholder="Subject"></span>
		  </div>

		  <div class="form-group">
		    <span style="width: 150px; margin-top: 8px; float: left;"><label>Start Date Time</label></span>
		    <span style="width: 300px; float: left;">
		    	<input type="text" name="start" id="start" class="form-control" placeholder="Start Date Time" /></span>
		  </div>

		  <div class="form-group">
		    <span style="width: 150px; margin-top: 8px; float: left;"><label>End Date Time</label></span>
		    <span style="width: 300px; float: left;">
		    	<input type="text" name="end" id="end" class="form-control" placeholder="End Date Time" /></span>
		  </div>


		  <div class="form-group">
		    <span style="width: 150px; margin-top: 8px; float: left;"><label>Conference Type</label></span>
		    <span style="width: 300px; float: left;">
		    	<select name="conference" id="conference" class="form-control">
				  <option>PSTN</option>
				  <option>Free</option>
				  <option>Hybrid</option>
				  <option>Private</option>
				  <option>VoIP</option>
				</select>
			</span>
		  </div>

		  <div class="form-group">
		    <span style="width: 150px; margin-top: 8px; float: left;"><label>Meeting Type</label></span>
		    <span style="width: 300px; float: left;">
		    	<select name='meetingtype' id="meetingtype" class="form-control">
				  <option>Immediate</option>
				  <option>Scheduled</option>
				  <option>Recurring</option>
				</select>
			</span>
		  </div>


		  <div class="form-group" style="padding-top: 60px;">
		    <span style="width: 150px; margin-top: 8px; float: left;"></span>
		    <span style="float: left;">
		    	<input type="button" class="btn btn-default" onclick="location.reload();" value="Reset" />
		    </span>
		    <span style=" float: left; margin-left: 10px;">
		    	<input type="button" onclick="CreateForm()" class="btn btn-primary" value="Submit" />
		    </span>
		  </div>

    	</form>
    </div>
</div>
</body>