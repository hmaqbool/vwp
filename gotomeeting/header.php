<link rel="stylesheet" type="text/css" href="views/css/bootstrap.min.css">
<style>

body{
	width: 100%; 
	height: auto;  
	border: none; 
	background-color: #EDE9D8; 
	padding: 0; margin:0;
}

#header_strip{
	width: 100%; 
	height: 66px; 
	overflow: hidden; 
	background-color: #343434;
}

#logo_continer{
	width: 900px; 
	height: auto; 
	margin:0 auto; 
	overflow: hidden;
}

#logo{
	width: 300px; 
	height: 100%; 
	overflow: hidden; 
	float: left; 
	color: white;
}

#update_btn{
	width: 130px; 
	height: 40 px; 
	overflow: hidden; 
	float: right; 
	color: white; 
	padding-top: 35px;
}

</style>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script language ="javascript" type = "text/javascript" >
    function tokenForm(){
       $("#tokenform").submit();
    }
</script>

<body >

	<form id="tokenform" method="GET" action="index.php" >
    	<input type="hidden" name="authorize" value="1" />
	</form>

	<!-- Header -->
    <div id = "header_strip">
        <div id = "logo_continer">
            <div id = "logo">
                <h1>GoToMeeting Api</h1>
            </div>
            <div id = "update_btn">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://yii.dev/index.php?authorize=1">Update Token</a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="#" onclick="tokenForm()">Update Token</a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 130px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://yii.dev/views/getfuturemeetings.php">Future Meetings</a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px;" href="http://www.vwp.co/hashim/gotomeeting/views/getfuturemeetings.php">Future Meetings</a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/views/getmeetings.php">Delete </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/views/getmeetings.php">Delete </a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/views/meetingsupdate.php">Update </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/views/meetingsupdate.php">Update </a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/views/create.php">Create </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/views/create.php">Create </a>';
				    }
            	?>
            </div>

            <div id = "update_btn" style="width: 80px;">
            	<?php
            		if ($_SERVER['HTTP_HOST'] == 'yii.dev') {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://yii.dev/">Home </a>';
				    } else {
				        echo '<a style="color: white; text-decoration: none; padding-left: 20px; width: 80px;" href="http://www.vwp.co/hashim/gotomeeting/index.php">Home </a>';
				    }
            	?>
            </div>

        </div>
    </div>
	<!-- Header -->
