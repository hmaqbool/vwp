<?php
/*
 * Created By: Muhammad Hashim
 * Date: Tuesday, August 20, 2013
 * Desc: Client configuration send to oAuth server for authorization and in response oAuth service send a code back to callback.php file (Redirect Uri).
 * 		 Callback.php handle the oAuth response and request back for token string, and on successfully getting token, redirect page again to authenticate.php file.
 * 		 authenticate.php list all workspaces by using the token string.
 */

include ( dirname(__FILE__) . '/lib/common.php' );
$accessToken = getAccessToken();

/*
 * for testing only.
if ($_GET['test'] == 1)
{
    print_r($accessToken);
    exit;
}
*/

// if token not found or expired.
if (false === $accessToken) 
{
    /* no valid access token available, go to authorization server */
    header("HTTP/1.1 302 Found");
    header("Location: " . getAuthorizeUri());
    exit;
}

try
{
    $apiUri = "https://ws-api.onehub.com/workspaces";
    $client = getHttpClient($accessToken);
    
    // header("Content-Type: application/json");
    $response = $client->get($apiUri)->send();
}
catch (Exception\BearerErrorResponseException $e)
{
    if ("invalid_token" === $e->getBearerReason())
    {
        // the token we used was invalid, possibly revoked, we throw it away
        $api->deleteAccessToken($context);
        $api->deleteRefreshToken($context);
        
        /* no valid access token available, go to authorization server */
        header("HTTP/1.1 302 Found");
        header("Location: " . $api->getAuthorizeUri($context));
        exit;
    }
    
    throw $e;
}
catch (\Exception $e)
{
    die(sprintf('ERROR: %s', $e->getMessage()));
}
?>

<style>
    .wrap
    {
        /* white-space: pre-wrap; */ /* css-3 */
        white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
        white-space: -pre-wrap; /* Opera 4-6 */
        white-space: -o-pre-wrap; /* Opera 7 */
        word-wrap: break-word; /* Internet Explorer 5.5+ */
    }
</style>

<div style="width: 100%; height: 100%;">
    <div class="wrap" style="width: 100%; height: 90%;"> <?php echo $response->getBody(); ?> </div>
    <div style="width: 100%; height: 8%; text-align: center;">
        <input type="button" onclick="javascript:window.close();" value="Close" />
    </div>
</div>