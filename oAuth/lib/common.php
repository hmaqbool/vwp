<?php
/*
* Created By: Muhammad Hashim
* Date: Tuesday, August 20, 2013
* Desc: List / Create / Update / Delete folders
*/

use \fkooman\OAuth\Client;
use \Guzzle\Http;
use \fkooman\Guzzle\Plugin\BearerAuth;
use \fkooman\Guzzle\Plugin\BearerAuth\Exception;

require_once dirname(__FILE__) . '/config.php';
require_once 'vendor/autoload.php';

class Common
{
    protected static $appInstance;
    public static function getObj()
    {
        if (!isset(self::$appInstance)) 
        {
            $c = __CLASS__;
            self::$appInstance = new $c();
        }
        
        return self::$appInstance;
    }
    
    public function getHttpClient($accessToken = '')
    {
        if ($accessToken == '') {
            $accessToken = self::getAccessToken();
        }

        $client = new Http\Client();
        $bearerAuth = new BearerAuth\BearerAuth($accessToken->getAccessToken());
        $client->addSubscriber($bearerAuth);

        return $client;
    }

    public function getAccessToken()
    {
        // Iniatialized oauth api object.
        $api = new Client\Api(getClientConfigId(), getClientConfig(), getTokenStorage(), new Http\Client());

        // iniatialized context object client authorizations scope.
        $context = new Client\Context(getUserId(), new Client\Scope(getScope()));

        // call api to access token with the client context object.
        $accessToken = $api->getAccessToken($context);

        return $accessToken;
    }

    public function getAuthorizeUri()
    {
        $api = new Client\Api(getClientConfigId(), getClientConfig(), getTokenStorage(), new Http\Client());
        $context = new Client\Context(getUserId(), new Client\Scope(getScope()));

        return $api->getAuthorizeUri($context);
    }

    public function deleteAccessToken()
    {
        $api = new Client\Api(getClientConfigId(), getClientConfig(), getTokenStorage(), new Http\Client());
        $context = new Client\Context(getUserId(), new Client\Scope(getScope()));
        $api->deleteAccessToken($context);
    }

    public function deleteRefreshToken()
    {
        $api = new Client\Api(getClientConfigId(), getClientConfig(), getTokenStorage(), new Http\Client());
        $context = new Client\Context(getUserId(), new Client\Scope(getScope()));
        $api->deleteRefreshToken($context);
    }

    public function getCallback()
    {
        return new Client\Callback(getClientConfigId(), getClientConfig(), getTokenStorage(), new Http\Client());
    }
    
}

?>
