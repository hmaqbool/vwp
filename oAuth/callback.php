<?php
include ( dirname(__FILE__) . '/lib/common.php' );

try 
{
    $cb = getCallback();
    $cb->handleCallback($_GET);

    header("HTTP/1.1 302 Found");
    
    if (isLocalhost()) {
        // For testing only
        //header("Location: http://localhost/folio3/projects/vwp/authenticate.php?test=1");
        header("Location: http://localhost/folio3/projects/vwp/authenticate.php");
    }else{
        header("Location: http://www.vwp.co/hashim/authenticate.php");
    }
    
    exit;
} catch (Client\AuthorizeException $e) {
    // this exception is thrown by Callback when the OAuth server returns a
    // specific error message for the client, e.g.: the user did not authorize
    // the request
    die(sprintf("ERROR: %s, DESCRIPTION: %s", $e->getMessage(), $e->getDescription()));
} catch (\Exception $e) {
    // other error, these should never occur in the normal flow
    die(sprintf("ERROR: %s", $e->getMessage()));
}