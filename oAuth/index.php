<script>
	function popupwindow(url, title, w, h) {
	  var left = (screen.width/2)-(w/2);
	  var top = (screen.height/2)-(h/2);
	  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
	} 
</script>

<div style="width: 100%; text-aligh: center;">
    <p> Please press the following button to get authentication token from oAuth service: </p>
    <input type="button" value="Authentication with oAuth Service" onclick="popupwindow('authenticate.php', 'oAuth Authentication', 800, 500);" />
</div>

<br />
<br />

<div>
    <p> Successfully accessed token from oAuth service. Please select anyone of the options below:  </p>
    
    <a onclick="javascript:popupwindow('views/workspaces/index.php', 'Workspace Management', 1000, 650);" href="#" >Workspaces Management</a> <br />
    <a onclick="javascript:popupwindow('views/events/index.php', 'Events Management', 1000, 600);" href="#" >Events Management</a> <br />
    <a onclick="javascript:popupwindow('views/rolemaps/index.php', 'Rolemaps Management', 1000, 600);" href="#" >Rolemaps Management</a> <br />
    <a onclick="javascript:popupwindow('views/users/index.php', 'Users Management', 1000, 600);" href="#" >Users Management</a> <br />
    <a onclick="javascript:popupwindow('views/invitations/index.php', 'Invitation Management', 1000, 600);" href="#" >Invitations Management</a> <br />
</div>