<?php
/*
 * Created By: Adil Abbasi
 * Date: Tuesday, September 03, 2013
 * Desc: Invitations
 */


// Default id set to root folder.
$id = 248324455;

// Override default folder id if present in url.
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}
include ( dirname(__FILE__) . '/../../lib/common.php' );

$objCommon = Common::getObj();
$client = $objCommon->getHttpClient();

$client->getEventDispatcher()->addListener('request.error', function(\Guzzle\Common\Event $event) {

    if ($event['response']->getStatusCode() == 422) {
        echo 'Unable to Send emial either email incorrect or already assigned.';
        exit;
    }
    else if($event['response']->getStatusCode() == 201) {
        echo 'Assigned to folder.';
    }
});

$apiUri = "https://ws-api.onehub.com/workspaces/390845/folders/$id.json";
$response = $client->get($apiUri)->send();
$arrFolders = $response->json();

if (isset($_POST['email']) && isset($_POST['file'])){

        $folder = $_POST['file'];
        $apiUri = "https://ws-api.onehub.com/folders/".$folder."/invitations.json";
        $request = $client->post($apiUri);
        $request->addPostFields(array(
            'invitation' => array('emails' => $_POST["email"]),
            )
        );      
    $response = $request->send();
}
?>
<style>
  .header{
    width: 250px;
    padding-left: 20px;
    padding-top:5px;
    height: 30px;
    float: left; 
    border-right: 1px solid;
  }
</style>
<script src="../../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" language="javascript">
    function Delformsubmit(email,fileid) { 

        if($(email).val() == ''){
            alert('Please provide valid email address');
        }else{
            $('input[name=email]').val($(email).val());
            $('input[name=file]').val(fileid);
            $('form#delform').submit();
        }
        
    }
</script>

<div style="width: 100%; height: 100%; overflow: scroll; ">
    <form id="delform" method="POST" action="index.php">
        <div style=" width: 900px; border: 1px solid; height: auto; margin:0 auto; ">
            <div style=" background-color: gray; width: 900px; border-bottom: 1px solid; height: 30px; margin:0 auto; overflow:hidden; ">
                <span class="header" >File Name</span>
                <span  class="header" >Email Address</span>
                <span  class="header" style=" width: 200px;border-right: none;">Action</span>

            </div>
            
            <div style="width: 900px;   margin:0 auto; overflow:hidden; ">
                <?php 
                    $count = 0;
                    foreach($arrFolders['items'] as $folder){
                        if($folder['folder']['id'] != null){
                            echo '<div style="border-bottom: solid 1px; overflow:hidden">';
                            echo '<span class="header" >'.$folder['folder']['filename'].'</span>';
                            echo '<span class="header" ><input id="emailid'.$count.'" type="text" name="emailaddr"></span>';
                            echo '<a href="#" class="header" style=" color: blue; text-decoration: none; width: 200px;border-right: none;" onclick="Delformsubmit(\'#emailid'.$count.'\','.$folder['folder']['id'].');">Invite</a>';
                            echo '</div>';
                        }
                        $count++;
                    }
                ?>
            </div>
        </div>
        <input type="hidden" name="email">
        <input type="hidden" name="file">
    </form>
</div>    