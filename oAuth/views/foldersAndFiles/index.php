<?php
/*
 * Created By: Muhammad Hashim
 * Date: Tuesday, August 20, 2013
 * Desc: List / Create / Update / Delete folders
 */

if (!isset($_GET['workspaceId'])) {
    echo "Error missing parameter workspaceId";
    return;
}

if (!isset($_GET['rootFolderId'])) {
    echo "Error missing parameter Root Folder Id";
    return;
}

// Override default folder id if present in url.
if (isset($_GET['id'])) {
    $apiUri = "https://ws-api.onehub.com/workspaces/{$_GET['workspaceId']}/folders/{$_GET['id']}.json";
}
else
{
    $apiUri = "https://ws-api.onehub.com/workspaces/{$_GET['workspaceId']}/files.json";
}

include ( dirname(__FILE__) . '/../../lib/common.php' );

$objCommon = Common::getObj();
$client = $objCommon->getHttpClient();

$response = $client->get($apiUri)->send();

//print_r($response->json()); exit;

$arrFolders = $response->json();
?>

<style>
    .wrap
    {
            /* white-space: pre-wrap; */ /* css-3 */
            white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
            white-space: -pre-wrap; /* Opera 4-6 */
            white-space: -o-pre-wrap; /* Opera 7 */
            word-wrap: break-word; /* Internet Explorer 5.5+ */
    }

    #folderTable { border: 1px solid lightgray; width: 500px; }
    #folderTable td { border: 1px solid lightgray; }
	
</style>

<link rel="stylesheet" href="../../../resources/css/colorbox.css" />

<div style="width: 100%; height: 100%;">
    <div class="wrap" style="width: 100%; height: 90%;">
        
        <h1>Folders & Files Management (OAuth)</h1>
        <img id="imgLoading" src="../../../resources/images/loading2.gif" style="display: none" />
        
        <div style="border: 1px solid lightgray; padding: 20px; width: 700px; margin-bottom: 10px;">
            <table>
                <tr>
                    <td>New Folder: </td>
                    <td><input type="textbox" id="newFolder" value="" /> </td>
                    <td align="right"><input type="button" id="btnCreate" value="Create" /></td>
                </tr>
            </table>
        </div>
        
        <div style="border: 1px solid lightgray; padding: 20px; width: 700px; margin-bottom: 10px;">
            <form action="fileUpload.php" method="post" enctype="multipart/form-data">
                <table>
                    <tr>
                        <td>Browse for a file to upload:</td>
                        <td>
                            <input type="file" id="file" name="file" size="100"/>
                            <input type="hidden" name="folderId" id="folderId" />
                            <input type="hidden" name="workspaceId" value="<?php echo $_GET['workspaceId']; ?>" />
                            <input type="hidden" name="rootFolderId" value="<?php echo $_GET['rootFolderId']; ?>" />
                        </td>
                        <td align="right"><input type="submit" name="submit" value="Upload" /></td>
                    </tr>
                </table>
            </form>
        </div>
        
        <div style="border: 1px solid lightgray; padding: 20px; width: 700px;">
            <!-- // Ancestor IDs -->
            <p>Folders & Files List:</p>
            <table id="folderTable" cellpadding="0" cellspacing="0">
            <?php foreach ($arrFolders['items'] as $folders) {?>
                <tr>
                    <?php if (isset($folders['folder'])) { ?>
                        <td style="width: 400px;"><a href="index.php?id=<?php echo $folders['folder']['id'];?>&workspaceId=<?php echo $_GET['workspaceId'];?>&rootFolderId=<?php echo $_GET['rootFolderId'];?>"><?php echo $folders['folder']['filename'];?></a></td>
                        <td style="width: 80px; text-align: center;"><a class="update" href="folderUpdate.php?id=<?php echo $folders['folder']['id']?>">Update</a></td>
                        <td style="width: 80px; text-align: center;"><a title="folder" class="delete" href="folderDelete.php?id=<?php echo $folders['folder']['id'];?>">Delete</a></td>
                    <?php } else if (isset($folders['file'])) { ?>
                        <td style="width: 400px;"><a href="fileDownload.php?id=<?php echo $folders['file']['id'];?>&workspaceId=<?php echo $_GET['workspaceId'];?>"><?php echo $folders['file']['filename'];?></a></td>
                        <td style="width: 80px; text-align: center;"><a class="update" href="fileUpdate.php?id=<?php echo $folders['file']['id']?>">Update</a></td>
                        <td style="width: 80px; text-align: center;"><a title="file" class="delete" href="fileDelete.php?id=<?php echo $folders['file']['id'];?>">Delete</a></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </table>
        </div>
    </div>
    <div style="width: 100%; height: 8%; text-align: center;">
            <input type="button" onclick="javascript:window.close();" value="Close" />
    </div>
</div>



<!-- Popup: Update -->
<div style="width: 300px; height: 100px; padding-top: 20px; padding-left: 40px; display: none;">
    <div id='updateContent' style='padding:10px; background:#fff;'>
        <table cellspacing="3">
            <tr>
                <td>Name: </td>
                <td><input id="folderName" type="text" value="" /></td>
            </tr>
            <tr>
                <td></td>
                <td align="right">
                    <input id="btnUpdate" type="button" value="Update" />
                </td>
            </tr>
        </table>
        <div id="updateMessage" style="display: none;">Please Wait..</div>
    </div>
</div>




<script src="../../../resources/js/jquery-1.10.2.min.js"></script>
<script src="../../../resources/js/jquery.colorbox.js"></script>

<script>
    
    var currentFolderId = <?php echo (isset($_GET['id'])) ? $_GET['id'] : 0; ?>;
    var workspaceId = <?php echo $_GET['workspaceId']; ?>;
    
    $(document).ready(function()
    {
        $('#folderId').val(currentFolderId);
        
        $('.delete').click(function()
        {
            if ($(this).attr('title') == 'folder')
            {
                if (!confirm('Are you sure you want to delete this folder?. If yes then all of its sub-folders and files will also be deleted.')) {
                    return false;
                }
            }
            else
            {
                if (!confirm('Are you sure you want to delete this file?.')) {
                    return false;
                }
            }
            
            $('#imgLoading').show();
            
            $.ajax({
                type: 'get',
                url: $(this).attr('href'),
                success: function(msg) {
                    if (msg == "Success") {
                        location.href=location.href;
                    }
                    else {
                        $('#imgLoading').hide();
                        alert("Error: " + msg);
                    }
                },
                error: function(err) {
                    $('#imgLoading').hide();
                    alert("Error: " + err);
                }
            });
            
            return false;
        });
        
        
        $('.update').click(function()
        {
            var url = $(this).attr('href');
            
            $.colorbox ({
                href: '#updateContent',
                inline: true,
                width: '300px',
                height: '170px'
            });
            
            $("#btnUpdate").click(function() 
            {
                if ($('#folderName').val().trim() == '')
                {
                    alert('Please provide any name');
                    return;
                }
                
                $(this).attr("disabled", true);
                $('#updateMessage').show();
                
                $.ajax({
                    type: 'get',
                    url:  url + "&name=" + $('#folderName').val(),
                    success: function(msg) {
                        $('#imgLoading').show();
                        location.href=location.href; parent.jQuery.colorbox.close();
                    },
                    error: function(err) {
                        $('#imgLoading').hide();
                        alert(err);
                    }
                });
            });
            
            return false;
            
        });
        
        
        $('#btnCreate').click(function(){
        
            if ($('#newFolder').val().trim() == '') 
            {
                alert('Please provide new folder name.');
                return;
            }
        
            $('#imgLoading').show();
        
            $.ajax({
               type: 'get',
               url: 'folderCreate.php?id=' + currentFolderId + '&name=' + $('#newFolder').val() + '&workspaceId=' + workspaceId,
               success: function(msg) {
                   $('#imgLoading').show();
                   location.href=location.href;
                   //alert(msg);
               },
               error: function(err) {
                   $('#imgLoading').hide();
                   alert(err);
               }
            });
        
        });
        
    });
    
</script>