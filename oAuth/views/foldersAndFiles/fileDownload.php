<?php

try
{
    if (!isset($_GET['id'])){
        echo "Error: missing parameter id";
        return;
    }
    
    if (!isset($_GET['workspaceId'])) {
        echo "Error missing parameter Workspace Id";
        return;
    }
    
    include ( dirname(__FILE__) . '/../../lib/common.php' );
    
    $apiUri = "https://ws-api.onehub.com/workspaces/{$_GET['workspaceId']}/files/{$_GET['id']}/download";

    $objCommon = Common::getObj();
    $client = $objCommon->getHttpClient();
    
    $request = $client->get($apiUri);
    $response = $request->send();
    
    header("Content-Type: {$response->getContentType()}");
    header("Content-Transfer-Encoding: {$response->getTransferEncoding()}");
    header("Cache-Control: {$response->getCacheControl()}",false); 
    header("Content-Disposition:  {$response->getContentDisposition()}" ); 
    header("Content-Length:  {$response->getContentLength()}" ); 
    
    echo $response->getBody();
}
catch (Guzzle\Http\Exception\ClientErrorResponseException $e)
{
    //echo "failure";
    die(sprintf("ERROR: %s, DESCRIPTION: %s", $e->getMessage(), $e->getDescription()));
}