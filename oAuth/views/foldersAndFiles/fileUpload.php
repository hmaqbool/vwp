<?php

if (!isset($_POST['folderId']))
{
    echo "Error missing parameter Folder Id";
    return;
}

if (!isset($_POST['workspaceId']))
{
    echo "Error missing parameter Workspace Id";
    return;
}

if (!isset($_POST['rootFolderId']))
{
    echo "Error missing parameter Root Folder Id";
    return;
}


if ($_FILES["file"]["error"] > 0) {
    echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
}
else
{
    //echo "Upload: " . $_FILES["file"]["name"] . "<br>";
    //echo "Type: " . $_FILES["file"]["type"] . "<br>";
    //echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
    //echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
    
    try
    {
        $path = dirname(__FILE__) . '/../../uploads/';
        
        move_uploaded_file($_FILES["file"]["tmp_name"], $path . $_FILES["file"]["name"]); 
        
        include ( dirname(__FILE__) . '/../../lib/common.php' );
        
        if (!empty($_POST['folderId'])) {
            $apiUri = "https://ws-api.onehub.com/workspaces/{$_POST['workspaceId']}/folders/{$_POST['folderId']}/files";
        }
        else {
            $apiUri = "https://ws-api.onehub.com/workspaces/{$_POST['workspaceId']}/folders/{$_POST['rootFolderId']}/files";
        }
        
        //echo $apiUri; exit;
        
        //$filePath = '@' . dirname(__FILE__) . '/../../uploads/BK4.xlsx';
        $filePath = "@$path{$_FILES["file"]["name"]}";

        $objCommon = Common::getObj();
        $client = $objCommon->getHttpClient();
    
        $request = $client->post($apiUri, null, array(
            'file_field'   => $filePath
        ));

        $response = $request->send();
        
        echo "Redirecting back, Please wait...";
        echo "<script>location.href='". $_SERVER['HTTP_REFERER'] ."';</script>";
    }
    catch (Guzzle\Http\Exception\ClientErrorResponseException $e)
    {
        echo "failure";
        //echo $e;
        //die(sprintf("ERROR: %s, DESCRIPTION: %s", $e->getMessage(), $e->getDescription()));
    } 
}