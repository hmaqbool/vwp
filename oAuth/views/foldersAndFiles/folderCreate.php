<?php

//if (!isset($_GET['folderId']) || !isset($_GET['name']))
if (!isset($_GET['name'])) {
    echo "Error missing parameter name";
    return;
}

if (!isset($_GET['workspaceId'])) {
    echo "Error missing parameter Workspace Id";
    return;
}

include ( dirname(__FILE__) . '/../../lib/common.php' );

if (empty($_GET['id'])) {
    $apiUri = "https://ws-api.onehub.com//workspaces/{$_GET['workspaceId']}/folders.json";
}
else {
    $apiUri = "https://ws-api.onehub.com//workspaces/{$_GET['workspaceId']}/folders/{$_GET['id']}/folders.json";
}

try
{
    $objCommon = Common::getObj();
    $client = $objCommon->getHttpClient();
    
    $request = $client->post($apiUri);
    $request->addPostFields(array(
        'folder' => array('filename' => $_GET['name']),
    ));
    
    $response = $request->send();
    
    echo $response->getBody();
}
catch (Guzzle\Http\Exception\ClientErrorResponseException $e)
{
    echo $e;
    //die(sprintf("ERROR: %s, DESCRIPTION: %s", $e->getMessage(), $e->getDescription()));
}
?>