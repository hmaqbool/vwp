<?php

if (!isset($_GET['id']) || !isset($_GET['name']))
{
    echo "error";
    return;
}

include ( dirname(__FILE__) . '/../../lib/common.php' );
$apiUri = "https://ws-api.onehub.com/folders/{$_GET['id']}";

try
{
    $objCommon = Common::getObj();
    $client = $objCommon->getHttpClient();
    
    $p["folder"] = array("filename" => $_GET['name']);
    $response = $client->put($apiUri)->addPostFields($p)->send();
    $arrResponse = $response->json();
    
    if ( count($arrResponse['folder']['errors']) <= 0) {
        echo "success";
    }
}
catch (Guzzle\Http\Exception\ClientErrorResponseException $e)
{
    echo $e;
    //die(sprintf("ERROR: %s, DESCRIPTION: %s", $e->getMessage(), $e->getDescription()));
}
?>