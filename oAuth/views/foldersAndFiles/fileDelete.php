<?php
/*
 * Created By: Muhammad Hashim
 * Date: Tuesday, August 20, 2013
 * Desc: List / Create / Update / Delete folders
 * Doc: http://guzzlephp.org/tour/http.html
 */
// Override default folder id if present in url.
if (!isset($_GET['id'])) {
    echo "Error: missing parameter folder id";
}

try
{
    include ( dirname(__FILE__) . '/../../lib/common.php' );
    
    $objCommon = Common::getObj();
    $client = $objCommon->getHttpClient();

    $apiUri = "https://ws-api.onehub.com/files/{$_GET['id']}";
    $request = $client->delete($apiUri);
    $request->getParams()->set('redirect.disable', true);
    $request->send();

    echo "Success";
    return;
}
catch (Exception $e)
{
    echo $e->getMessage();
}

?>
