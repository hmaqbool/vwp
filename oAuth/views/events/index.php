<?php
/*
 * Created By: Adil Abbasi
 * Date: Wednessday, September 04, 2013
 * Desc: Activity
 */

include ( dirname(__FILE__) . '/../../lib/common.php' );

//header("Content-Type: application/json");
$objCommon = Common::getObj();
$client = $objCommon->getHttpClient();

$apiUri = "https://ws-api.onehub.com/activity.json";
$response = $client->get($apiUri)->send();
$arrActivity = $response->json();
//var_dump($arrActivity['items']);  die();

?>
<style>
  .header{
    width: 140px;
    padding-left: 20px;
    padding-top:5px;
    height: 30px;
    float: left; 
    border-right: 1px solid;
  }
</style>

<div style="width: 100%; height: 100%; margin-bottom: 10px; overflow: scroll;">
	<div style=" width: 900px; border: 1px solid; height: auto; margin:0 auto;">
        <div style=" background-color: gray; width: 900px; border-bottom: 1px solid; height: 30px; margin:0 auto; overflow:hidden; ">
            <span class="header" style=" width: 210px;">File Name</span>
            <span  class="header">File Type</span>
            <span  class="header">User Name</span>
            <span  class="header">Updated At</span>
            <span class="header" style="border-right: none; ">Action</span>
        </div>
		<?php
			foreach($arrActivity['items'] as $activity){
				if (!strpos($activity['event']['detail'],'the role for') !== false) {
					$pieces = explode(">", $activity['event']['detail']);
					$name = explode("<", $pieces[1]); // $name[0]
					$action = explode("<", $pieces[3]); // $action[0]
					$filename = explode("<", $pieces[6]); // $filename[0]
					$filetype = explode("<", $pieces[7]); // $filetype[0]
					if($filetype[0] == null){
						$filetype[0] = 'Folder';
					}
					if (strpos($action[0],'changed') !== false) {
		    			$action[0] = 'Renamed';
					}
					$udated_at = new DateTime($activity['event']['updated_at']);
					$udated_at =  date_format($udated_at, 'd M, Y H:i');
					echo '<div style=" background-color: lightgray; width: 900px; border-bottom: 1px solid; height: 30px; margin:0 auto; overflow:hidden; ">';
		            echo '<span class="header" style=" width: 210px;">'.$filename[0].'</span>';
		            echo '<span  class="header">'.$filetype[0].'</span>';
		            echo '<span  class="header">'.$name[0].'</span>';
		            echo '<span  class="header">'.$udated_at.'</span>';
		            echo '<span class="header" style="border-right: none;">'.$action[0].'</span>';
		        	echo '</div>';
		        }
			}
		?>
	</div>
</div>