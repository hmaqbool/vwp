<?php

if (!isset($_GET['name'])){
    echo "Error missing parameter workspace name";
    exit;
}

include ( dirname(__FILE__) . '/../../lib/common.php' );
$apiUri = "https://ws-api.onehub.com/workspaces";

try
{
    $objCommon = Common::getObj();
    $client = $objCommon->getHttpClient();
    
    $response = $client->post($apiUri)->addPostFields(array(
                            'workspace' => array('name' => $_GET['name']),
                        ))->send();
    
    //echo "Workspace successfully created";
}
catch (Guzzle\Http\Exception\ClientErrorResponseException $e)
{
    //echo "There are some errors in processing your request";
    //die(sprintf("ERROR: %s, DESCRIPTION: %s", $e->getMessage(), $e->getDescription()));
}
?>