<?php
/*
 * Created By: Muhammad Hashim
 * Date: Tuesday, August 20, 2013
 * Desc: List / Create / Delete Workspaces
 */

include ( dirname(__FILE__) . '/../../lib/common.php' );

$objCommon = Common::getObj();
$client = $objCommon->getHttpClient();

//header("Content-Type: application/json");
//$client = getHttpClient();
$apiUri = "https://ws-api.onehub.com/workspaces";
$response = $client->get($apiUri)->send();

//print_r($response->json());

$arrWorkspaces = $response->json();
?>

<style>
    .wrap
    {
            /* white-space: pre-wrap; */ /* css-3 */
            white-space: -moz-pre-wrap; /* Mozilla, since 1999 */
            white-space: -pre-wrap; /* Opera 4-6 */
            white-space: -o-pre-wrap; /* Opera 7 */
            word-wrap: break-word; /* Internet Explorer 5.5+ */
    }
        
    #table { border: 1px solid lightgray; width: 500px; }
    #table td { border: 1px solid lightgray; }
</style>

<script src="../../../resources/js/jquery-1.10.2.min.js"></script>

<script>

    $(document).ready(function(){
        
        $('.delete').click(function()
        {
            if (!confirm('Are you sure you want to delete this workspace?. If yes then all of its sub-folders and files will also be deleted.')) {
                return false;
            }
            
            $('#imgLoading').show();
            
            $.ajax({
                type: 'get',
                url: $(this).attr('href'),
                success: function(msg) {
                    if (msg == "Success") {
                        location.href=location.href;
                    }
                    else {
                        $('#imgLoading').hide();
                        alert("Error: " + msg);
                    }
                },
                error: function(err) {
                    $('#imgLoading').hide();
                    alert("Error: " + err);
                }
            });
            
            return false;
        });
        
       
       $('#btnCreate').click(function(){
           
          $('#imgLoading').show();
          
          $.ajax({
             type: 'get',
             url: 'create.php?name=' + $('#newWorkspace').val(),
             success: function(msg) {
                 $('#imgLoading').hide();
                 location.href = location.href;
             },
             error: function(err) {
                 $('#imgLoading').hide();
             }             
              
          });
           
       });
        
    });

</script>


<h1>Workspace Management (OAuth)</h1>
<img id="imgLoading" src="../../../resources/images/loading2.gif" style="display: none" />

<div style="border: 1px solid lightgray; padding: 20px; width: 700px; margin-bottom: 20px;">
    <table>
        <tr>
            <td>New Workspace: </td>
            <td><input type="textbox" id="newWorkspace" value="" /> </td>
            <td align="right"><input type="button" id="btnCreate" value="Create" /></td>
        </tr>
    </table>
</div>

<div style="font-size: 14px; width: 700px; font-style: italic; margin-bottom: 3px;">Note: There are only 2 workspaces are allowed to create. if you want to create a new workspace then delete any existing workspace and then create a new one. </div>
<div style="border: 1px solid lightgray; padding: 20px; width: 700px;  margin-bottom: 50px;">
    <div class="wrap" style="width: 100%; height: 200px;">
        <div style="font-size: 14px; font-style: italic; margin-bottom: 3px;"> Select any workspace to view its childs.  </div>
        <table id="table" cellpadding="0" cellspacing="0">
        <?php foreach ($arrWorkspaces['items'] as $workspace) { ?>
            <tr>
                <td style="width: 400px;"><a href="../foldersAndFiles/index.php?workspaceId=<?php echo $workspace['workspace']['id'];?>&rootFolderId=<?php echo $workspace['workspace']['root_folder_id'];?>"><?php echo $workspace['workspace']['name'];?></a></td>
                <td style="width: 80px; text-align: center;"><a class="delete" href="delete.php?id=<?php echo $workspace['workspace']['id'];?>">Delete</a></td>
            </tr>
        <?php } ?>
        </table>
        
    </div>
</div>

<div style="width: 100%; height: 8%; text-align: center;">
        <input type="button" onclick="javascript:window.close();" value="Close" />
</div>