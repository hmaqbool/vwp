<?php
/*
 * Created By: Adil Abbasi
 * Date: Tuesday, September 03, 2013
 * Desc:  Roles Deletion
 */

/* Default id set to root folder. */
$id = 248324455;

include ( dirname(__FILE__) . '/../../lib/common.php' );

$objCommon = Common::getObj();
$client = $objCommon->getHttpClient();

/* Deleting rolemap after getting parameters from url. */
if (isset($_POST['userid'])) {
    $apiUri = "https://ws-api.onehub.com/rolemaps/".$_POST['userid'];
    $request = $client->delete($apiUri);
    $request->addPostFields(array(
        $_POST['filetype'] => array('filename' => $_POST['filename']),
    ));
    $request->getParams()->set('redirect.disable', true);
    $request->send();
}

/* Collecting data related to folders. */
$apiUri = "https://ws-api.onehub.com/workspaces/390845/folders/$id.json";
$response = $client->get($apiUri)->send();
$arrFolders = $response->json();
$all_folders = 0;
$directory = array('rolemap_id'=>array(),'role_name'=>array(),'user_id'=>array(),'filename'=>array(),'filetype'=>array());

/* Iterating folders and collectiong rolemaps of each folder. */
while ($all_folders < count($arrFolders['items'])){
    if(isset($arrFolders['items'][$all_folders]['folder'])){
        $fold_id[$all_folders] = $arrFolders['items'][$all_folders]['folder']['id'];
        $apiUri = "https://ws-api.onehub.com/folders/$fold_id[$all_folders]/rolemaps";
        
    }
    else if(isset($arrFolders['items'][$all_folders]['file'])) {
        $fold_id[$all_folders] = $arrFolders['items'][$all_folders]['file']['id'];
        $apiUri = "https://ws-api.onehub.com/files/$fold_id[$all_folders]/rolemaps";
        
    }
    else{
        break;
    }
    $response = $client->get($apiUri)->send();
    $Folders_rolemap = $response->json();

    $rolemap_index = 0;
    while($rolemap_index < count($Folders_rolemap['items'])){

        if(isset($arrFolders['items'][$all_folders]['folder']['filename'])){
            $name = $arrFolders['items'][$all_folders]['folder']['filename'];
            $directory[$all_folders]['filetype'][$rolemap_index] = 'folder';
        }else{
            $name = $arrFolders['items'][$all_folders]['file']['filename'];
            $directory[$all_folders]['filetype'][$rolemap_index] = 'file';
        }
        $directory[$all_folders]['filename'][$rolemap_index] = $name;
        $directory[$all_folders]['rolemap_id'][$rolemap_index] = $Folders_rolemap['items'][$rolemap_index]['rolemap']['id'];
        $directory[$all_folders]['role_name'][$rolemap_index] = $Folders_rolemap['items'][$rolemap_index]['rolemap']['role_name'];
        $directory[$all_folders]['user_id'][$rolemap_index] = $Folders_rolemap['items'][$rolemap_index]['rolemap']['user_id'];
        $rolemap_index++;
    }
    $all_folders++;
}

?>
<style>
  .header{
    width: 180px;
    padding-left: 20px;
    padding-top:5px;
    height: 30px;
    float: left; 
    border-right: 1px solid;
  }
</style>
<script src="../../js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" language="javascript">
    function formsubmition (uid,file,type) { 
        $('input[name=userid]').val(uid);
        $('input[name=filename]').val(file);
        $('input[name=filetype]').val(type);
        $('form#userid').submit();
    }
</script>
<div style="width: 100%; height: 100%; overflow: scroll;">
    <div style=" width: 900px; border: 1px solid; height: auto; margin:0 auto; ">
        <div style=" background-color: gray; width: 900px; border-bottom: 1px solid; height: 30px; margin:0 auto; overflow:hidden; ">
            <span class="header" style=" width: 250px;">File Name</span>
            <span  class="header">Role Name</span>
            <span  class="header">User Name</span>
            <span class="header" style="border-right: none; width: 180px;">Action</span>
        </div>
        <div style="width: 900px;   margin:0 auto; overflow:hidden; ">

          <?php 
            $records = 0;
            while ( $records < $all_folders){

                $rolemap_index = 0;
                while($rolemap_index < count($directory[$records]['role_name'])){

                    $apiUri = "https://ws-api.onehub.com/user_profiles/".$directory[$records]['user_id'][$rolemap_index].".json";
                    $response = $client->get($apiUri)->send();
                    $userdata = $response->json();
                    $name = $userdata['user']['first_name'].' '.$userdata['user']['last_name'];

                    if($directory[$records]['role_name'][$rolemap_index] == 'Administrator' || $directory[$records]['role_name'] == 'administrator'){
                        $text = '<a href="#" style="text-decoration: none; padding-left: 50px;" ></a>';
                    }else{
                        $text = '<a href="#" style="text-decoration: none; color:blue" onclick="formsubmition('.$directory[$records]['rolemap_id'][$rolemap_index].',\''.$directory[$records]['filename'][$rolemap_index].'\',\''.$directory[$records]['filetype'][$rolemap_index].'\')">Delete</a>';
                    }
                    echo '<div style="border-bottom: 1px solid; overflow: hidden;">';
                    echo '<span class="header" style=" width: 250px;">'.$directory[$records]['filename'][$rolemap_index].'</span>';
                    echo '<span class="header" >'.$directory[$records]['role_name'][$rolemap_index].'</span>';
                    echo '<span class="header" >'.$name.'</span>';
                    echo '<span class="header" style="border-right: none; width: 180px; ">'.$text.'</span>';
                    echo '</div>';
                    $rolemap_index++;
                }
                $records++;
            }
        ?>
     </div>
    </div>
    <form id="userid" method="POST" action="index.php">
        <input type="hidden" name="userid">
        <input type="hidden" name="filename">
        <input type="hidden" name="filetype">
    </form>
</div>  
